package com.rsl.alfredodiaz.utils;

import android.app.Application;

/**
 * Created by vinay on 16-11-2016.
 */

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        LocaleHelper.onCreate(this, "en");
    }

//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
//    }
}
