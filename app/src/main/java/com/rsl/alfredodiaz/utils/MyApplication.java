package com.rsl.alfredodiaz.utils;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Created by admin on 10/14/2015.
 */
public class MyApplication extends MultiDexApplication {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
       // LocaleHelper.onAttach(base, "en");
        LocaleHelper.onCreate(this, "en");
        MultiDex.install(this);
    }

//    @Override
//    public void onCreate() {
//        super.onCreate();
//        LocaleHelper.onCreate(this, "en");
//    }
}
