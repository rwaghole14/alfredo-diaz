package com.rsl.alfredodiaz.utils;

import java.util.ArrayList;
import java.util.HashMap;

public class Constants {
    public static boolean activity_past = false;
    public static boolean activity_future = false;
    public static boolean activity_news = false;
    public static boolean activity_message = false;
    public static boolean activity_media = false;
    public static boolean activity_event = false;
    public static boolean activity_pcmc = false;
    public static boolean activity_emergency = false;
    public static boolean activity_importantnum= false;
    public static boolean activity_esrvices= false;
    public static boolean activity_onlinefrom= false;
    public static boolean activity_impwed= false;
    public static boolean activity_job= false;
    public static boolean activity_tender= false;



    public static ArrayList<HashMap<String, String>> getImage_data() {
        return image_data;
    }

    public static void setImage_data(ArrayList<HashMap<String, String>> image_data) {
        Constants.image_data = image_data;
    }

//    public static boolean isWorkActivityOpen = false;

    public static ArrayList<HashMap<String, String>> image_data = new ArrayList<>();


}
