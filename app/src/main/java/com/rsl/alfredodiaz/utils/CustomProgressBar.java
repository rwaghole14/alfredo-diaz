package com.rsl.alfredodiaz.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/**
 * Created by rsl on 7/3/18.
 */

public class CustomProgressBar extends ProgressBar {

    private String mText;
    private Paint mTextPaint;

    public CustomProgressBar(Context context) {
        super(context);
        mText = "0/100";
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
    }

    public CustomProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mText = "0/100";
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mText = "0/100";
        mTextPaint = new Paint();
        mTextPaint.setColor(Color.BLACK);
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect bounds = new Rect();
        mTextPaint.getTextBounds(mText, 0, mText.length(), bounds);
        int x = getWidth() / 2 - bounds.centerX();
        int y = getHeight() / 2 - bounds.centerY();
        canvas.drawText(mText, x, y, mTextPaint);
    }

    public synchronized void setText(String text) {
        this.mText = text;
        drawableStateChanged();
    }

    public void setTextColor(int color) {
        mTextPaint.setColor(color);
        drawableStateChanged();
    }
}
