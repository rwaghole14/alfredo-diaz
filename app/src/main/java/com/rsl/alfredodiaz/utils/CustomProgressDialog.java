package com.rsl.alfredodiaz.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

import com.rsl.alfredodiaz.R;


/**
 * Created by admin on 2/8/2017.
 */

public class CustomProgressDialog  extends ProgressDialog {

    public CustomProgressDialog(Context context) {
        super(context);
    }

    public static ProgressDialog ctor(Context context) {
        CustomProgressDialog dialog = new CustomProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }



    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_custome_progres_dialog);
        ImageView la = (ImageView) findViewById(R.id.animation);
        //  la.setBackgroundResource(R.drawable.animation_drwable);
        //  animation = (AnimationDrawable) la.getBackground();
    }

}
