package com.rsl.alfredodiaz;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rsl.alfredodiaz.activity.EServices;
import com.rsl.alfredodiaz.activity.EmergencyServicesActivity;
import com.rsl.alfredodiaz.activity.EventsActivity;
import com.rsl.alfredodiaz.activity.FutureWorkActivity;
import com.rsl.alfredodiaz.activity.ImpLinksActivity;
import com.rsl.alfredodiaz.activity.ImportantNumbersActivity;
import com.rsl.alfredodiaz.activity.JobOpprtunityActivity;
import com.rsl.alfredodiaz.activity.MainActivity;
import com.rsl.alfredodiaz.activity.MassageActivity;
import com.rsl.alfredodiaz.activity.MediaActivity;
import com.rsl.alfredodiaz.activity.NewsAndUpdatesActivity;
import com.rsl.alfredodiaz.activity.PastWorkActivity;
import com.rsl.alfredodiaz.activity.PcmcSchemesActivity;
import com.rsl.alfredodiaz.activity.TenderActivity;
import com.rsl.alfredodiaz.activity.UsefulFormsActivity;
import com.rsl.alfredodiaz.model.EmergencyServices;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;


/**
 * Created by admin on 8/30/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.notification_icon);
        Log.i("onMessage/////", "Received message");
        Intent notificationIntent = null;
        String msg = "You Have Message";
        String type = "status";
        String message_display;

        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.e(TAG, "From: " + remoteMessage.getFrom());
      //  Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
       // Log.e(TAG, "Notification Message data: " + remoteMessage.getData());
    //    Log.e(TAG, "Notification Message Tag: " + remoteMessage.getNotification().getTag());
        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData());
            Log.e(TAG, "JSONObject: " + jsonObject.toString());
            sendNotification(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private void sendNotification(JSONObject jo) {
        Intent notificationIntent = null;
        int random_number = (int) Math.round(Math.random() * 999999);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.notification_icon);
        Log.i("onMessage/////", "Received message");
        try {
            String action = jo.get("action").toString();
            if (action != null) {
                if (action.toLowerCase().equals("work")) {
                    Log.e("offer...............", jo.get("type").toString() + "" + "" + jo.get("msg").toString());

                    //sendNotification(this,getString(R.string.app_name),jo.get("msg").toString(),random_number);
                    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                    if (jo.get("type").toString().equals("Past work") && Constants.activity_past) {
                        notificationIntent = new Intent(this, PastWorkActivity.class);
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        notificationIntent.putExtra("type", "Past work");
                        startActivity(notificationIntent);

                    } else if (jo.get("type").toString().equals("Future work") && Constants.activity_future) {
                        notificationIntent = new Intent(this, FutureWorkActivity.class);
                        notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        notificationIntent.putExtra("type", "Future work");
                        startActivity(notificationIntent);
                    } else {
                        if (jo.get("type").toString().equals("Past work")) {
                            notificationIntent = new Intent(this, PastWorkActivity.class);
                            notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        } else if (jo.get("type").toString().equals("Future work")) {
                            notificationIntent = new Intent(this, FutureWorkActivity.class);
                            notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TOP);
                        }
                        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, notificationIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT| PendingIntent.FLAG_ONE_SHOT);
                        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                                .setContentTitle(getString(R.string.app_name))
                                .setContentText(jo.get("msg").toString())
                                .setLargeIcon(largeIcon)
                                .setContentIntent(pendingIntent)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri);
                        NotificationManager notificationManager =(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(random_number /* ID of notification */, notificationBuilder.build());
                    }

                } else if (action.toLowerCase().equals("newsupdate")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, NewsAndUpdatesActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_news) {
                        startActivity(notificationIntent);
                    }
                }else if (action.toLowerCase().equals("message")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, MassageActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_message) {
                        startActivity(notificationIntent);
                    }
                }else if (action.toLowerCase().equals("video")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, MediaActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_media) {
                        startActivity(notificationIntent);
                    }
                } else if (action.toLowerCase().equals("image")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, MediaActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_media) {
                        startActivity(notificationIntent);
                    }
                }
                else if (action.toLowerCase().equals("scheme")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, PcmcSchemesActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_pcmc) {
                        startActivity(notificationIntent);
                    }
                }else if (action.toLowerCase().equals("event")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, EventsActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_event) {
                        startActivity(notificationIntent);
                    }
                }/*else if (action.toLowerCase().equals("emergencyservices")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, EmergencyServicesActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_emergency) {
                        startActivity(notificationIntent);
                    }
                } else if (action.toLowerCase().equals("importantnumber")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, ImportantNumbersActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_importantnum) {
                        startActivity(notificationIntent);
                    }
                }*/else if (action.toLowerCase().equals("eservices")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, EServices.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_esrvices) {
                        startActivity(notificationIntent);
                    }
                }else if (action.toLowerCase().equals("online forms")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, UsefulFormsActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_onlinefrom) {
                        startActivity(notificationIntent);
                    }
                } /*else if (action.toLowerCase().equals("impweb")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, ImpLinksActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_impwed) {
                        startActivity(notificationIntent);
                    }
                }*/else if (action.toLowerCase().equals("job")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, JobOpprtunityActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_job) {
                        startActivity(notificationIntent);
                    }
                }else if (action.toLowerCase().equals("tenderupdate")) {
                    Log.v("msggh...............", jo.get("msg").toString());
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, TenderActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(jo.get("msg").toString())
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                    if (Constants.activity_tender) {
                        startActivity(notificationIntent);
                    }
                }else {
                    String message_display = jo.get("msg").toString();
                    Log.v("msggh...............", "" + message_display);
                    Uri alarmSound = RingtoneManager
                            .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    notificationIntent = new Intent(this, MainActivity.class);
                    notificationIntent.setFlags(FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent intent1 = PendingIntent.getActivity(this, 0,
                            notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(message_display)
                            .setLargeIcon(largeIcon)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(intent1)
                            .setAutoCancel(true)
                            .setSound(alarmSound);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(random_number, notification.build());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(Context context, String title, String message, int countNotification) {
        Intent intent = new Intent(context, FutureWorkActivity.class);
        intent.putExtra("type", "Future work");

        intent.addFlags(FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(countNotification /* ID of notification */, notificationBuilder.build());
    }

}