package com.rsl.alfredodiaz.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import com.gtomato.android.ui.transformer.CoverFlowViewTransformer;
import com.gtomato.android.ui.widget.CarouselView;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.adapter.RecyclerCoverFlowAdapter;
import com.rsl.alfredodiaz.model.GameEntity;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.MultipartUtility;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

//    @BindView(R.id.list)
//    RecyclerCoverFlow mRecyclerCoverFlow;
//

    @BindView(R.id.carousel)
    CarouselView mScrollView;

    private RecyclerCoverFlowAdapter mAdapter;

//    private FeatureCoverFlow mCoverFlow;
//    private CoverFlowAdapter mAdapter;

    private ArrayList<GameEntity> mData = new ArrayList<>(14);
    private Context mContext;

    String mobile_number;
    String month_name;
    ImageView btn_about_me, btn_about_work, btn_support;
    String fcm_id = "";

    SharedPreferences sp;
    ConnectionDetector internet;
    static final int send_data = 1;
    TextView btn_aboutme, btn_aboutwork, btn_support1, btn_viewAll;

    TextView txt_aboutme, txt_aboutwork, txt_support;

    //  Animation animAlpha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        );
        ButterKnife.bind(this);
        mContext = MainActivity.this;
        Toolbar mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            insertDummyContactWrapper();
        }
        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        internet = new ConnectionDetector(MainActivity.this);
        initViews();

        fcm_id = FirebaseInstanceId.getInstance().getToken();
        Log.e("device_token", "===" + fcm_id);
        Log.e("sendDeviceId_flag", "====" + sp.getBoolean("sendDeviceId", false));

        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(2 * 1000);
                    // After 5 seconds redirect to another intent
                    if (!sp.getBoolean("sendDeviceId", false)) {
                        sendDeviceId();
                    }

                } catch (Exception e) {

                }
            }
        };
        background.start();

//        mAdapter = new CoverFlowAdapter(this);
        mAdapter = new RecyclerCoverFlowAdapter(this);
        CoverFlowViewTransformer transformer = new CoverFlowViewTransformer();
        transformer.setYProjection(90f);
        mScrollView.setTransformer(transformer);
        mScrollView.setAdapter(mAdapter);
        mScrollView.setInfinite(true);
        mScrollView.setEnableFling(false);

        mScrollView.setOnItemClickListener(new CarouselView.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView.Adapter adapter, View view, int position, int adapterPosition) {
                Intent clickIntent = null;
                switch (adapterPosition) {
                    case 0:
                        clickIntent = new Intent(MainActivity.this, IntroductionActivity.class);
                        clickIntent.putExtra("TAG", "Introduction");
                        break;

                    case 1:
                        clickIntent = new Intent(MainActivity.this, EmergencyServicesActivity.class);
                        break;

                    case 2:
                        clickIntent = new Intent(MainActivity.this, ImportantNumbersActivity.class);
                        break;

                    case 3:
                        clickIntent = new Intent(MainActivity.this, EServices.class);
                        break;

                    case 4:
                        clickIntent = new Intent(MainActivity.this, NewsAndUpdatesActivity.class);
                        break;

                    case 5:
                        clickIntent = new Intent(MainActivity.this, EventsActivity.class);
                        break;

                    case 6:
                        clickIntent = new Intent(MainActivity.this, VoterListActivity.class);
                        clickIntent.putExtra("link", "http://103.23.150.139/marathi/");
                        clickIntent.putExtra("title", "" + getString(R.string.str_voter_list));
                        break;

                    case 7:
                        clickIntent = new Intent(MainActivity.this, ComplaintActivity.class);
                        break;

                    case 8:
                        clickIntent = new Intent(MainActivity.this, ContactUsActivity.class);
                        break;

                    case 9:
                        clickIntent = new Intent(MainActivity.this, UsefulFormsActivity.class);
                        break;

                    case 10:
                        clickIntent = new Intent(MainActivity.this, ImpLinksActivity.class);
                        break;

                    case 11:
                        clickIntent = new Intent(MainActivity.this, PcmcSchemesActivity.class);
                        break;

                    case 12:
                        clickIntent = new Intent(MainActivity.this, JobOpprtunityActivity.class);
                        break;

                    case 13:
                        clickIntent = new Intent(MainActivity.this, MediaActivity.class);
                        break;

                    case 14:
                        clickIntent = new Intent(MainActivity.this, TenderActivity.class);
                        break;
                    case 15:
                        clickIntent = new Intent(MainActivity.this, TransparencyLaw.class);
                        break;

                }
               try {
                   startActivity(clickIntent);
               }catch (Exception e){
                    e.printStackTrace();
               }
            }
        });

        loadLocale();

//        mScrollView.setItemTransformer(new ScaleTransformer.Builder()
//                .setMaxScale(1.05f)
//                .setMinScale(0.8f)
//                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
//                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
//                .build());
//        mScrollView.setSlideOnFling(true);
//        mScrollView.setSlideOnFlingThreshold(2100);

//        InfiniteScrollAdapter wrapper = InfiniteScrollAdapter.wrap(mAdapter);
//        mScrollView.setAdapter(wrapper);

//        mRecyclerCoverFlow.setAdapter(mAdapter);
//        int velocityX = 1;
//        velocityX *= 0.1;
//        mRecyclerCoverFlow.fling(velocityX,1);
//        mRecyclerCoverFlow.setLayoutManager(mLayoutManager);
//        mRecyclerCoverFlow.canScrollHorizontally()
//        mCoverFlow.setAdapter(mAdapter);

//        mCoverFlow.scroll();
//        mCoverFlow.setLayoutAnimation();


        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (tm != null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mobile_number = tm.getLine1Number();
        }
        Log.e("number", "----" + mobile_number);
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month = month_date.format(cal.getTime());
        month_name = month.substring(0, 3);
        Log.e("month_name", "----" + month_name);
    }

    private void initViews() {
        btn_support = (ImageView) findViewById(R.id.img_support);
        btn_about_work = (ImageView) findViewById(R.id.about_work);
        btn_about_me = (ImageView) findViewById(R.id.about_me);
        btn_aboutme = (TextView) findViewById(R.id.btn_aboutme);
        btn_aboutwork = (TextView) findViewById(R.id.btn_aboutwork);
        btn_support1 = (TextView) findViewById(R.id.btn_support1);
        btn_viewAll = (TextView) findViewById(R.id.btn_viewAll);
        txt_aboutme = (TextView) findViewById(R.id.txt_aboutme);
        txt_aboutwork = (TextView) findViewById(R.id.txt_aboutwork);
        txt_support = (TextView) findViewById(R.id.txt_support);

        //setclick listener
        // btn_support.setOnClickListener(this);
        btn_aboutwork.setOnClickListener(this);
        btn_aboutme.setOnClickListener(this);
        btn_support1.setOnClickListener(this);
        btn_viewAll.setOnClickListener(this);
        // animAlpha = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);

        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/OpenSans-Light.ttf");
        txt_aboutme.setTypeface(face);
        txt_aboutwork.setTypeface(face);
        txt_support.setTypeface(face);
        // btn_viewAll.setTypeface(face);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private Locale myLocale;

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.apply();
    }

    public void loadLocale() {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "en");
        changeLang(language);
    }

    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // myLocale = new Locale(LocaleHelper.getLanguage(getApplicationContext()));
        if (myLocale != null) {
            Log.e("onConfigurationChanged", "====onConfigurationChanged" + myLocale.getDisplayLanguage());
            newConfig.locale = myLocale;
            Locale.setDefault(myLocale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    private void updateTexts() {
        mAdapter.clear();
        mAdapter.add(new GameEntity(R.drawable.browser, R.string.str_home));
        mAdapter.add(new GameEntity(R.drawable.emergency_service, R.string.str_emergency_service));
        mAdapter.add(new GameEntity(R.drawable.important_number, R.string.str_imp_numbers));
        mAdapter.add(new GameEntity(R.drawable.eservices, R.string.str_e_services));
        mAdapter.add(new GameEntity(R.drawable.news, R.string.str_news_update));
        mAdapter.add(new GameEntity(R.drawable.event, R.string.str_events));
        mAdapter.add(new GameEntity(R.drawable.voterlist, R.string.str_voter_list));
        mAdapter.add(new GameEntity(R.drawable.complaints, R.string.str_complaints));
        mAdapter.add(new GameEntity(R.drawable.contactus, R.string.str_contact_us));
        mAdapter.add(new GameEntity(R.drawable.online_froms, R.string.str_online_forms));
        mAdapter.add(new GameEntity(R.drawable.imp_web, R.string.str_imp_webs));
        mAdapter.add(new GameEntity(R.drawable.schemes, R.string.str_pcmc_scheme));
        mAdapter.add(new GameEntity(R.drawable.job_opportunites, R.string.str_jobs));
        mAdapter.add(new GameEntity(R.drawable.gallery, R.string.str_media));
        mAdapter.add(new GameEntity(R.drawable.browser, R.string.string_tender_header));
        mAdapter.add(new GameEntity(R.drawable.gavel, R.string.str_transparency));
//        Log.e("mAdapter","=="+mAdapter.getCount());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.btn_aboutme: {
                Thread background = new Thread() {
                    public void run() {
                        try {
                            // Thread will sleep for 5 seconds
                            sleep(1 * 1000);
                            // After 5 seconds redirect to another intent
                            Intent i = new Intent(MainActivity.this, AboutMeActivtiy.class);
                            startActivity(i);
                            // Remove activity

                        } catch (Exception e) {

                        }
                    }
                };
                background.start();
                break;
            }
            case R.id.btn_aboutwork: {

                Thread background = new Thread() {
                    public void run() {
                        try {
                            // Thread will sleep for 5 seconds
                            sleep(1 * 1000);
                            // After 5 seconds redirect to another intent
                            Intent i = new Intent(MainActivity.this, AllWorkActivity.class);
                            startActivity(i);
                            // Remove activity

                        } catch (Exception e) {

                        }
                    }
                };
                background.start();

                break;
            }
            case R.id.btn_support1: {

                new getSupport().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mobile_number, month_name, sp.getString("my_lang", "english"));

                break;
            }
            case R.id.btn_viewAll: {
                Intent i = new Intent(MainActivity.this, ViewAllActivity.class);
                startActivity(i);
                break;
            }
        }
    }

    private void sendDeviceId() {
        final RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        String requestURL = getString(R.string.web_path) + "get_device_token.php";
        Log.e("check web_path", requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                JSONObject jobj;
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            Log.d("response", "" + result);
                            jobj = new JSONObject(result);
                            if (jobj.getString("result").equals("success")) {
                                sp.edit().putBoolean("sendDeviceId", true).apply();
                            } else {
                                sp.edit().putBoolean("sendDeviceId", false).apply();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                fcm_id = FirebaseInstanceId.getInstance().getToken();
                params.put("device_token", fcm_id);
                Log.e("device_token_params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }


    private class getSupport extends AsyncTask<String, Void, String> {

        String cr_date = "";
        final ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            Log.w("upload", "startd....");
            //dialog.setMessage(getString(R.string.str_registering));
            dialog.setCancelable(false);
            dialog.show();
        }


        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.w("upload", "uploading.....");
            StringBuffer result = new StringBuffer();
            String upLoadServerUri = getString(R.string.web_path) + "support.php";
            Log.e("URL", "=" + upLoadServerUri.replaceAll(" ", "%20"));
            try {
                String charset = "UTF-8";
                MultipartUtility multipart = new MultipartUtility(upLoadServerUri, charset);
                multipart.addHeaderField("User-Agent", "SanjayWable");
                multipart.addHeaderField("Test-Header", "Header-Value");
                multipart.addFormField("mobile_number", params[0]);
                multipart.addFormField("month", "" + params[1]);
                multipart.addFormField("language", "" + params[2]);
                multipart.addFormField("repost", "false");
                Log.e("params[0]", "--" + params[0]);
                Log.e("params[1]", "--" + params[1]);
                Log.e("params[2]", "--" + params[2]);
                //multipart.addFormField("device_token", "" + FirebaseInstanceId.getInstance().getToken());


                List<String> response = multipart.finish();
                Log.e("multipart", response.toString());

                System.out.println("SERVER_REPLIED:");
                for (String line : response) {
                    System.out.println("" + line);
                    result.append(line);
                }

            } catch (Exception e) {
                e.printStackTrace();
                dialog.dismiss();
                // something went wrong. connection with the server error
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String response) {
            // TODO Auto-generated method stub
            Log.w("onPostExecute", "onPostExecute==========" + response);
            JSONObject jObj;
            if (response != null) {
                Log.e("complaint_registration", response);
                if (!response.startsWith("null")) {
                    try {
                        jObj = new JSONObject(response);
                        if (jObj.length() > 0) {
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                Toast.makeText(MainActivity.this, jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                Log.e("msg", "---" + jObj.getString("msg"));
                                Intent i = new Intent(MainActivity.this, SupportGraphActivity.class);
                                i.putExtra("msg", jObj.getString("msg"));
                                startActivity(i);

                                /// finish();
                            } else if (jObj.getString("result").equals("failed")) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                Toast.makeText(MainActivity.this, jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                Log.e("msg", "+++" + jObj.getString("msg"));
                                Intent i = new Intent(MainActivity.this, SupportGraphActivity.class);
                                i.putExtra("msg", jObj.getString("msg"));
                                startActivity(i);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (dialog.isShowing())
                            dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
            } else {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        }
    }

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.READ_SMS))
            permissionsNeeded.add("Contact");

        if (!addPermission(permissionsList, android.Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Contact");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }

            return;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkPermission(permission)) {
            permissionsList.add(permission);
            return false;
        }
        return true;
    }

    private boolean checkPermission(String permission) {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
        if (result != PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                } else {
                    //Permission Denied
                    // Toast.makeText(RegistrationActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT);
//                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String lang = "en";
        switch (item.getItemId()) {
            case R.id.action_english:
                //Toast.makeText(this, "You have selected English Menu", Toast.LENGTH_SHORT).show();
                lang = "en";
                sp.edit().putString("my_lang", "english").apply();
                changeLang(lang);
                finish();
                startActivity(new Intent(this, MainActivity.class));
                break;
//            case R.id.action_marathi:
//                lang = "mr";
//                sp.edit().putString("my_lang", "marathi").apply();
//                // Toast.makeText(this, "You have selected Marathi Menu", Toast.LENGTH_SHORT).show();
//                changeLang(lang);
//                finish();
//                startActivity(new Intent(this, MainActivity.class));
//                break;
            case R.id.action_shear:
                //   Toast.makeText(this, "You have selected shear Menu", Toast.LENGTH_SHORT).show();
                if (internet.isConnectingToInternet()) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    //add a subject
                    sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                            getResources().getString(R.string.app_name));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.rsl.alfredodiaz&hl=en");
                    sendIntent.setType("text/plain");
                    startActivityForResult(sendIntent, send_data);
                } else {
                    internet.showAlertDialog(MainActivity.this,
                            "No Internet Connection",
                            "You don't have internet connection.", false);
                }
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
}
