package com.rsl.alfredodiaz.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.MultipartUtility;
import com.rsl.alfredodiaz.utils.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ComplaintActivity extends AppCompatActivity {
    EditText edt_name, edt_mobile, edt_email, edt_subject, edt_description;
    ImageView img_attchment;
    Button btn_submit;
    ConnectionDetector cd;
    SharedPreferences sp;
    private String userChoosenTask;
    String encoded_image = "";
    String filePath = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    public static final int REQUEST_CAMERA_LOWER=1;
    public static final int REQUEST_CROP_PICTURE=3;
    public static final int CAMERA_CROP=0;
    private int REQUEST_CAMERA = 2;
    Uri photoURI;
    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }


        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_complaints));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cd = new ConnectionDetector(getApplicationContext());
        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        img_attchment = (ImageView) findViewById(R.id.img_attchment);
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_description = (EditText) findViewById(R.id.edt_description);
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);

        edt_email = (EditText) findViewById(R.id.edt_Email);
        edt_subject = (EditText) findViewById(R.id.edt_subject);
        btn_submit = (Button) findViewById(R.id.btn_submit);
/*
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        Log.e("file_path", "=" + file.getAbsolutePath());
        if (file.exists()) {
            file.delete();
        }*/
        filePath = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/" + TEMP_PHOTO_FILE;
        img_attchment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        edt_name.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_name.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        edt_mobile.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_mobile.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        edt_email.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_email.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        edt_subject.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_subject.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        edt_description.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_description.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              if (!cd.isConnectingToInternet()) {
                                                  cd.showAlertDialog(ComplaintActivity.this,
                                                          "No Internet Connection",
                                                          "You don't have internet connection.", false);
                                              } else {
                                                  InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                  imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                                  String name = edt_name.getText().toString().trim();
                                                  String emailText = edt_email.getText().toString().trim();
                                                  String phoneText = edt_mobile.getText().toString().trim();
                                                  String subjectText = edt_subject.getText().toString().trim();
                                                  String complaintText = edt_description.getText().toString().trim();
                                                  if (Is_Valid_first_Name(edt_name)) {
                                                      if (!phoneText.equals("")) {
                                                          if ((phoneText.length() == 10)) {
                                                              if (emailText.length() > 0) {
                                                                  if (isValidEmail(emailText)) {
                                                                      if ((subjectText.length() > 0)) {
                                                                          if (edt_mobile.getText().toString().trim().equals("0000000000") || edt_mobile.getText().toString().trim().equals("00000000000") || edt_mobile.getText().toString().trim().equals("000000000000") || edt_mobile.getText().toString().trim().equals("0000000000000") || edt_mobile.getText().toString().trim().equals("00000000000000") || edt_mobile.getText().toString().trim().equals("000000000000000")) {
                                                                              edt_mobile.setError(getResources().getString(R.string.enter_valid_mobile));
                                                                              edt_mobile.requestFocus();
                                                                          } else {
                                                                              if (edt_description.getText().toString().trim().length() > 0) {
                                                                                  Log.e("encoded_image", "+" + encoded_image);
                                                                                  new postVideo().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, name, phoneText, emailText, subjectText, complaintText);

//                                                                                  complaint(name, phoneText, emailText, subjectText, complaintText);
                                                                              } else {
                                                                                  edt_description.setError(getResources().getString(R.string.enter_text));
                                                                                  edt_description.requestFocus();
                                                                              }
                                                                          }
                                                                      } else {
                                                                          edt_subject.setError(getResources().getString(R.string.enter_subject_text));
                                                                          edt_subject.requestFocus();
                                                                      }
                                                                  } else {
                                                                      edt_email.setError(getString(R.string.str_email_error));
                                                                      edt_email.requestFocus();
                                                                  }
                                                              } else {
                                                                  edt_email.setError(getString(R.string.str_email_error));
                                                                  edt_email.requestFocus();
                                                              }
                                                          } else {
                                                              edt_mobile.setError(getResources().getString(R.string.enter_digits));
                                                              edt_mobile.requestFocus();

                                                          }
                                                      } else {
                                                          edt_mobile.setError(getResources().getString(R.string.enter_mobile));
                                                          edt_mobile.requestFocus();
                                                      }

                                                  } else {

                                                  }
                                              }//onClick
                                          }
                                      }

        );
    }//onCreate


  /*  /////old code/////

    private String userChoosenTask = "";
    String encoded_image = "";
    private static final int PICK_FROM_CAMERA = 1;
    private static final int CROP_FROM_CAMERA = 2;
    private static final int PICK_FROM_FILE = 3;
    private static final int CROP_FROM_FILE = 4;

    private void selectImage() {
        final CharSequence[] items = getResources().getStringArray(R.array.array_select_photo_options);
        AlertDialog.Builder builder = new AlertDialog.Builder(ComplaintActivity.this);
        builder.setTitle(getString(R.string.str_add_photo_title));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(ComplaintActivity.this);
                if (item == 0) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                } else if (item == 1) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    static boolean active = false;
    @Override
    protected void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File croppedImageFile = new File(getFilesDir(), "test.jpg");
        if (resultCode == RESULT_OK) {
            try {
                switch (requestCode) {
                    case PICK_FROM_FILE: {
                        if (data != null) {
                            String url = data.getData().toString();
                            if (url.startsWith("content://com.google.android.apps.photos.content")) {
                                try {
                                    InputStream is = null;
                                    is = getContentResolver().openInputStream(Uri.parse(url));
                                    Bitmap bitmap = BitmapFactory.decodeStream(is);
                                    if (bitmap != null) {
                                        img_attchment.setImageBitmap(bitmap);
                                        encoded_image = encodeTobase64(bitmap);
                                    } else {
                                        encoded_image = "";
                                        Toast.makeText(ComplaintActivity.this, "Invalid Image", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                final Uri imageUri = data.getData();
                                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                                img_attchment.setImageBitmap(selectedImage);
                                encoded_image = encodeTobase64(selectedImage);
                            }
                        }
                        break;
                    }
                    case PICK_FROM_CAMERA: {

                        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                        Log.e("file_path", "=" + file.getAbsolutePath());
                        if (file.exists()) {
                            String filePath = Environment.getExternalStorageDirectory() + File.separator + "img.jpg";
                            Bitmap thumbnail = BitmapFactory.decodeFile(filePath);
                            if (thumbnail != null) {
                                onCaptureImageResult(filePath);
                            } else {
                                Toast.makeText(this, "Invalid image", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Log.e("data", "=" + "data null");
                        }
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void onCaptureImageResult(String thumbnail) {


        String new_file = Environment.getExternalStorageDirectory() + File.separator + "img1.jpg";

        Bitmap b = BitmapFactory.decodeFile(thumbnail);
        Bitmap out = Bitmap.createScaledBitmap(b, b.getWidth() / 5, b.getHeight() / 5, false);

        File file = new File(new_file);
        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(file);
            out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            b.recycle();
            // out.recycle();


        } catch (Exception e) {
        }

        img_attchment.setImageBitmap(out);
        encoded_image = encodeTobase64(out);

    }

    public static String encodeTobase64(Bitmap immagex) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image*//*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, getString(R.string.str_select_file)), PICK_FROM_FILE);
    }

    private void cameraIntent() {

        if (getApplicationContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // Open default camera
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file.toString())));

            // start the image capture Intent
            startActivityForResult(intent, PICK_FROM_CAMERA);

        } else {
            Toast.makeText(getApplication(), "Camera not supported", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }*/


////////////////////////

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            img_attchment.setImageURI(Crop.getOutput(result));

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(result));
                encoded_image = encodeTobase64(bitmap);
                Log.e("imge_cropping",""+encoded_image);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ComplaintActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(ComplaintActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_CROP_PICTURE);
    }

    private void cameraIntent() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file.toString())));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File croppedImageFile = new File(ComplaintActivity.this.getFilesDir(), "test.jpg");
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CROP_PICTURE) {
                if (data != null) {
                    String url = data.getData().toString();
                    InputStream is = null;
                    if (url.startsWith("content://com.google.android.apps.photos.content")) {
                        try {
                            is = getContentResolver().openInputStream(Uri.parse(url));
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            onCaptureImageResult1(bitmap);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Uri croppedImage = Uri.fromFile(croppedImageFile);
                        CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
                        cropImage.setSourceImage(data.getData());
                        sp.edit().putBoolean("crop", true).apply();
                        startActivityForResult(cropImage.getIntent(ComplaintActivity.this), 11);
                    }
                }
            }else if(requestCode == 11){
                Log.e("Path===============", "" + croppedImageFile.getAbsolutePath());
                sp.edit().remove("crop").apply();
                sp.edit().apply();
                Bitmap selectedImage = BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath());
                //img_profile.setImageBitmap(selectedImage);
                sp.edit().remove("crop").apply();
                sp.edit().apply();
                if (selectedImage != null)
                    onCaptureImageResult1(selectedImage);
                else
                    Toast.makeText(this, "Invalid image", Toast.LENGTH_SHORT).show();
            } else if (requestCode == REQUEST_CAMERA) {
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                performCrop(Uri.fromFile(file));
            } else if (requestCode == CAMERA_CROP) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    //get the cropped bitmap from extras
                    Bitmap selectedImage = extras.getParcelable("data");
//                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    if (selectedImage != null)
                        onCaptureImageResult(data);
                    else
                        Toast.makeText(this, "Invalid image", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Operation cancelled", Toast.LENGTH_SHORT).show();
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                    if (file.exists())
                        file.delete();
                }

            }
//				onCaptureImageResult(data);
        }
    }
    private void onCaptureImageResult1(Bitmap thumbnail) {
//        Bitmap thumbnail = (Bitmap) data.getExtras().getParcelable("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        try {
            String PATH = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/";
            File folder = new File(PATH);
            if (!folder.exists()) {
                folder.mkdir();//If there is no folder it will be created.
                Log.e("folder====", "created");
            }
            File destination = new File(PATH + TEMP_PHOTO_FILE);

            destination.createNewFile();
            FileOutputStream fo;

            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            if (thumbnail != null){
                encoded_image = encodeTobase64(thumbnail);
                img_attchment.setImageBitmap(thumbnail);
            } else{
                encoded_image = "";
                img_attchment.setImageResource(R.mipmap.ic_launcher);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().getParcelable("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        //thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        try {
            String PATH = Environment.getExternalStorageDirectory() + "/Android/data/" + getPackageName() + "/";
            File folder = new File(PATH);
            if (!folder.exists()) {
                folder.mkdir();//If there is no folder it will be created.
                Log.e("folder====", "created");
            }
            File destination = new File(PATH + TEMP_PHOTO_FILE);

            destination.createNewFile();
            FileOutputStream fo;

            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            if (thumbnail != null){
                encoded_image = encodeTobase64(thumbnail);
                img_attchment.setImageBitmap(thumbnail);
            } else {
                img_attchment.setImageResource(R.mipmap.ic_launcher);
                encoded_image = "";
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void performCrop(Uri picUri) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 200);
            cropIntent.putExtra("outputY", 200);
            File file = new File(filePath);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(file.toString())));
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CAMERA_CROP);
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }
    ////////////////////////
    private class postVideo extends AsyncTask<String, Void, String> {

        String cr_date = "";
        final ProgressDialog dialog = new ProgressDialog(ComplaintActivity.this);


        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            Log.w("upload", "startd....");
            dialog.setMessage(getString(R.string.str_registering));
            dialog.setCancelable(false);
            dialog.show();
        }


        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.w("upload", "uploading.....");
            StringBuffer result = new StringBuffer();
            String upLoadServerUri = getString(R.string.web_path) + "complaint.php";
            Log.e("URL", "=" + upLoadServerUri.replaceAll(" ", "%20"));
            // Log.e("fcm_id", "=" +FirebaseInstanceId.getInstance().getToken());

            try {
                String charset = "UTF-8";
                MultipartUtility multipart = new MultipartUtility(upLoadServerUri, charset);
                multipart.addHeaderField("User-Agent", "SanjayWable");
                multipart.addHeaderField("Test-Header", "Header-Value");
                multipart.addFormField("name", params[0]);
                multipart.addFormField("mobile_number", "" + params[1]);
                multipart.addFormField("email", "" + params[2]);
                multipart.addFormField("subject", "" + params[3]);
                multipart.addFormField("message", "" + params[4]);
                multipart.addFormField("image", "" + encoded_image);
                //multipart.addFormField("device_token", "" + FirebaseInstanceId.getInstance().getToken());
                multipart.addFormField("repost", "false");

                Log.e("param",""+params[0]+ params[1]+params[2]+params[3]+params[4]);
                List<String> response = multipart.finish();
                Log.e("multipart", response.toString());

                System.out.println("SERVER_REPLIED:");
                for (String line : response) {
                    System.out.println("" + line);
                    result.append(line);
                }

            } catch (Exception e) {
                e.printStackTrace();
                dialog.dismiss();
                // something went wrong. connection with the server error
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String response) {
            // TODO Auto-generated method stub
            Log.w("onPostExecute", "onPostExecute==========" + response);
            JSONObject jObj;
            if (response != null) {
                Log.e("complaint_registration", response);
                if (!response.startsWith("null")) {
                    try {
                        jObj = new JSONObject(response);
                        if (jObj.length() > 0) {
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                                File file1 = new File(Environment.getExternalStorageDirectory() + File.separator + "img1.jpg");
                                if (file.exists())
                                    file.delete();
                                if (file1.exists())
                                    file1.delete();

                                Toast.makeText(ComplaintActivity.this, getResources().getString(R.string.str_complaint_accept), Toast.LENGTH_SHORT).show();
                                finish();
                            } else if (jObj.getString("result").equals("failed")) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (dialog.isShowing())
                            dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
            } else {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        }
    }

    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                ComplaintActivity.this);
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.str_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private boolean isValidEmail(String email) {

        if (Character.isDigit(email.charAt(0))) {
            return false;
        } else if (email.startsWith(".")) {
            return false;
        } else if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public Boolean Is_Valid_first_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().trim().length() <= 0) {
            edt.setError(getResources().getString(R.string.enter_name));
            edt.requestFocus();

        } else if (edt.getText().toString().trim().length() <= 1) {
            edt.setError(getResources().getString(R.string.enter_more_char));

            edt.requestFocus();

        }
//        else if (!edt.getText().toString().trim().matches("[a-zA-Z ]+")) {
//            edt.setError(getResources().getString(R.string.accept_alphabet_first));
//            edt.requestFocus();
//
//        }
        else {

            return true;
        }
        return false;
    }//Is_Valid_first_Name

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }
}

