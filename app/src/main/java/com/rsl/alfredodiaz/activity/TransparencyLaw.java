package com.rsl.alfredodiaz.activity;

import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransparencyLaw extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.list_phone_nos)
    ListView mPhoneCategoryList;

    @BindView(R.id.horizontal_line)
    View mLine;

    ArrayList<String> items;
    private LayoutInflater inflater;
    MyCustomAdapter adapter;
    String[] mEmerArray;

    private RequestQueue mQueue;
    private ConnectionDetector internet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transparency_law);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        ButterKnife.bind(this);
        initView();
        mLine.setVisibility(View.INVISIBLE);

        this.inflater = LayoutInflater.from(TransparencyLaw.this);
        mEmerArray = getResources().getStringArray(R.array.important_number_header);
        internet = new ConnectionDetector(this);
        items = new ArrayList<>();
        adapter = new MyCustomAdapter();
//        for (int i = 0; i < mEmerArray.length; i++) {
//            adapter.add(mEmerArray[i]);
//        }

        if (internet.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(TransparencyLaw.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
//                    getImages();
                    getTransparencyLaw();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            internet.showAlertDialog(TransparencyLaw.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
       // mPhoneCategoryList.setDividerHeight(0);
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.str_transparency));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private class MyCustomAdapter extends BaseAdapter {
        public void add(String data) {
            items.add(data);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.layout_group, null);
            }
            TextView txt_item = (TextView) row.findViewById(R.id.lblListHeader);
            txt_item.setText(items.get(position));

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(TransparencyLaw.this, TransparencyLawDetails.class);
                    i.putExtra("position", String.valueOf(position));
                    i.putExtra("title", items.get(position));
                    Log.e("position", "====" + position);
                    startActivity(i);
                }
            });
            return row;
        }
    }
    private void getTransparencyLaw(){

        String requestURL = getString(R.string.web_path) +"get_TransLaw.php" + "?language=english";
        Log.e("requestURL"," "+requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.d("get_TransLaw", "got response");
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    Log.e("result",""+result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        JSONArray dataArray = baseJSONObject.getJSONArray("data");
                                        if (dataArray.length() > 0){
                                            for (int i = 0; i < dataArray.length(); i++){
                                                JSONObject dataObject = dataArray.getJSONObject(i);
                                                String mainTitle = dataObject.getString("transLaw_type");
                                                JSONArray mImpNumbersArray = dataObject.getJSONArray("transLaw_data");
                                                for (int k = 0; k < mImpNumbersArray.length(); k++){
                                                    JSONObject mImpNumberObject =  mImpNumbersArray.getJSONObject(k);
                                                  //  String imageUrl = mImpNumberObject.getString("image");
                                                }
                                                adapter.add(mainTitle);
                                            }

                                            mPhoneCategoryList.setAdapter(adapter);
                                            mLine.setVisibility(View.VISIBLE);
                                        }
                                    }else {
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        String msg = baseJSONObject.getString("msg");
                                        Toast.makeText(TransparencyLaw.this, ""+ msg, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mProgressbarLayout.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
            }
        });

        mQueue.add(stringRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
