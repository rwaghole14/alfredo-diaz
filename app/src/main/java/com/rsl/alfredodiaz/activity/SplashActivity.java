package com.rsl.alfredodiaz.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.rsl.alfredodiaz.R;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity {
    //    ImageView image;
    LinearLayout lay_zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        image = (ImageView) findViewById(R.id.imageView_landing);
        lay_zoom = (LinearLayout) findViewById(R.id.lay_zoom);

        Animation animationrotate = AnimationUtils.loadAnimation(
                getApplicationContext(), R.anim.zoom);
        lay_zoom.startAnimation(animationrotate);

        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(3 * 1000);
                    // After 5 seconds redirect to another intent
                    Intent i;
                    i = new Intent(getBaseContext(), HomeActivity.class);
                    startActivity(i);
                    // Remove activity
                    finish();

                } catch (Exception e) {

                }
            }
        };
        // start thread
        background.start();

        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        Log.e("Language", "" + prefs.getString(langPref, "en"));
        if (prefs.getString(langPref, "en").equals("en")) {
            changeLang("en");
        } else {
            changeLang("mr");
        }
    }

    private Locale myLocale;

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

}
