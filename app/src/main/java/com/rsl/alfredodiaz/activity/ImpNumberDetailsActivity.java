package com.rsl.alfredodiaz.activity;

import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.adapter.ExpandableListAdapter;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.model.ImportantNumbers;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImpNumberDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;


    @BindView(R.id.expand_imp_numbers_list)
    ExpandableListView mExpandableServiceList;

    @BindView(R.id.image_view_slider)
    ImageView mNumberImageView;

    private String mMainTitle;
    private RequestQueue mQueue;
    private ConnectionDetector mConnection;
    private ArrayList<ImportantNumbers> mImportantNoList = new ArrayList<>();
    private ArrayList<ImportantNumbers> mNumberHeaderList = new ArrayList<>();

    ExpandableListAdapter mExpandableListAdapter;
    String[] mEmerArray;
    ImageView img_home_slider;
    ArrayList<GroupData> group_list = new ArrayList<GroupData>();
    public static int[] imageId = {R.drawable.bg_admin_zonal_office, R.drawable.bg_st_depo, R.drawable.bg_admin_zonal_office, R.drawable.bg_water_tank};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imp_number_details);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        ButterKnife.bind(this);

        if (getIntent() != null) {
            mMainTitle = getIntent().getStringExtra("title");
        }
        initView();

//        int pos = Integer.parseInt(getIntent().getStringExtra("position"));
//        img_home_slider.setImageResource(imageId[pos]);
//        Log.e("position", "====" + pos);
////        listAdapter = new ExpandableListAdapter(this, group_list);
//        prepareListData(pos);

        mExpandableListAdapter = new ExpandableListAdapter(this, mImportantNoList, 1);
        // setting list adapter
        mExpandableServiceList.setAdapter(mExpandableListAdapter);

        mConnection = new ConnectionDetector(this);
        if (mConnection.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(ImpNumberDetailsActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
                    getImpNoDetails();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            mConnection.showAlertDialog(ImpNumberDetailsActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mMainTitle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void prepareListData(int pos) {
//        group_list.clear();
//        switch (pos) {
//            case 0: {
//                mEmerArray = getResources().getStringArray(R.array.useful_no_word);
//            }
//            break;
//            case 1:
//                mEmerArray = getResources().getStringArray(R.array.st_depo_inquiry);
//                break;
//            case 2:
//                mEmerArray = getResources().getStringArray(R.array.admin_zonal_offices);
//                break;
//            case 3:
//                mEmerArray = getResources().getStringArray(R.array.water_tank);
//                break;
//            default:
//                break;
//        }
//        Arrays.sort(mEmerArray);
//        for (int i = 0; i < mEmerArray.length; i++) {
//            String[] a = mEmerArray[i].split("/");
//
//            GroupData grp_data = new GroupData();
//            grp_data.setGroup_name(a[0]);
//            ArrayList<HashMap<String, String>> data = new ArrayList<>();
//            HashMap<String, String> child = new HashMap<>();
//            child.put("phone", a[1]);
//            child.put("address", a[2]);
//            data.add(child);
//
//            grp_data.setChild_items(data);
//
//            group_list.add(grp_data);
//        }
//        listAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getImpNoDetails() {
        final String requestURL = getString(R.string.web_path) + "get_ImpNum.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        JSONArray mDataObjectArray = baseJSONObject.getJSONArray("data");
                                        if (mDataObjectArray.length() > 0){
                                            for (int i = 0; i < mDataObjectArray.length(); i++){
                                                JSONObject mDataJSONObject = mDataObjectArray.getJSONObject(i);
                                                String title = mDataJSONObject.getString("main_title");
                                                if (title.equals(mMainTitle)){
                                                    String imageUrl = null;
                                                    JSONArray mNumberDataArray = mDataJSONObject.getJSONArray("ImpNum_data");
                                                    for (int k = 0; k < mNumberDataArray.length(); k++){
                                                        JSONObject mNumberObject = mNumberDataArray.getJSONObject(k);

                                                        String mSubTitle = mNumberObject.getString("sub_title");
                                                        String mContactNo = mNumberObject.getString("phone");
                                                        String mAddress = mNumberObject.getString("address");
                                                        imageUrl = mNumberObject.getString("image");
                                                        mNumberHeaderList.add(new ImportantNumbers(mSubTitle, mAddress, mContactNo, imageUrl));
                                                    }
                                                    prepareList(mNumberHeaderList);
                                                }
                                            }
                                        }

                                    }else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                }

                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        mQueue.add(stringRequest);
    }

    private void prepareList(ArrayList<ImportantNumbers> mHeaderList){

        Glide.with(this)
                .load(mHeaderList.get(0).getmImpNoImage())
                .into(mNumberImageView);

        mImportantNoList.clear();

        for (int i = 0; i < mHeaderList.size(); i++) {
            ImportantNumbers mNumberObject = new ImportantNumbers();
            mNumberObject.setmImpNoSubTitle(mHeaderList.get(i).getmImpNoSubTitle());

            ArrayList<HashMap<String, String>> mChildData = new ArrayList<>();
            HashMap<String, String> mChild = new HashMap<>();
            mChild.put("phone", mHeaderList.get(i).getmImpNoContact());
            mChild.put("address", mHeaderList.get(i).getmImpNoAddress());
            mChildData.add(mChild);

            mNumberObject.setChildItems(mChildData);
            mImportantNoList.add(mNumberObject);
        }
        mExpandableListAdapter.notifyDataSetChanged();
    }
}
