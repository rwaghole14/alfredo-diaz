package com.rsl.alfredodiaz.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.MultipartUtility;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    ArrayList<String> pager_list_data;
    Button btn_home, btn_my_info, btn_psst_work, btn_future_work,
            btn_emerg_servies, btn_imp_numbers, btn_e_services,
            btn_news_update, btn_events, btn_voter_list,
            btn_complaints, btn_contactus, btn_imp_forms, btn_imp_webs, btn_pcmc_scheme, btn_jobs, btn_media;
    //ViewPager view_pager;

    private SliderLayout mDemoSlider;
    int currentPage = 0;
    Timer timer;
    int[] imageId = {R.drawable.slide6, R.drawable.slide4, R.drawable.slide5, R.drawable.slide2, R.drawable.slide3};
    private RadioGroup radioLangGroup;
    SharedPreferences sp;
    String fcm_id = "";
    ImageView img_share,btn_support;
    static final int send_data = 1;
    ConnectionDetector internet;
    ArrayList<HashMap<String, String>> list_data;
    String mobile_number;
    String month_name;
    ImageView past_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            insertDummyContactWrapper();
        }
        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        internet = new ConnectionDetector(HomeActivity.this);
        initViews();
        fcm_id = FirebaseInstanceId.getInstance().getToken();
        Log.e("device_token", "===" + fcm_id);

        mDemoSlider = (SliderLayout) findViewById(R.id.view_pager_home);

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);

        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        Log.e("Language", "" + prefs.getString(langPref, "en"));
        if (prefs.getString(langPref, "en").equals("en")) {
            radioLangGroup.check(R.id.radio_english);
//            changeLang("en");
        } else {
            radioLangGroup.check(R.id.radio_marathi);
//            changeLang("mr");
        }
        radioLangGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb = (RadioButton) findViewById(checkedId);
                String lang = "en";
                switch (checkedId) {
                    case R.id.radio_english:
                        lang = "en";
                        sp.edit().putString("my_lang", "english").apply();
                        // do operations specific to this selection
                        break;
                    case R.id.radio_marathi:
                        lang = "mr";
                        sp.edit().putString("my_lang", "marathi").apply();
                        // do operations specific to this selection
                        break;
                }
                changeLang(lang);
                // Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
            }
        });
        Log.e("sendDeviceId_flag", "====" + sp.getBoolean("sendDeviceId", false));
        if (!sp.getBoolean("sendDeviceId", false)) {
            sendDeviceId();
        }
        loadLocale();

        TelephonyManager tm = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        if (tm != null) {
            mobile_number = tm.getLine1Number();
        }
        Log.e("number","----"+mobile_number);
        Calendar cal=Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
        String month = month_date.format(cal.getTime());
        month_name=month.substring(0,3);
        Log.e("month_name","----"+month_name);
    }
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    private void insertDummyContactWrapper() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.READ_SMS))
            permissionsNeeded.add("Contact");

        if (!addPermission(permissionsList, android.Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Contact");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }

            return;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkPermission(permission)) {
            permissionsList.add(permission);
            return false;
        }
        return true;
    }

    private boolean checkPermission(String permission) {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
        if (result != PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(android.Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);

                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
                } else {
                    //Permission Denied
                    // Toast.makeText(RegistrationActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT);
//                            .show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initViews() {
        btn_home = (Button) findViewById(R.id.btn_home);
        btn_my_info = (Button) findViewById(R.id.btn_my_info);
        btn_psst_work = (Button) findViewById(R.id.btn_psst_work);
        btn_future_work = (Button) findViewById(R.id.btn_future_work);
        btn_emerg_servies = (Button) findViewById(R.id.btn_emerg_servies);
        btn_imp_numbers = (Button) findViewById(R.id.btn_imp_numbers);
        btn_e_services = (Button) findViewById(R.id.btn_e_services);
        btn_news_update = (Button) findViewById(R.id.btn_news_update);
        btn_events = (Button) findViewById(R.id.btn_events);
        btn_voter_list = (Button) findViewById(R.id.btn_voter_list);
        btn_complaints = (Button) findViewById(R.id.btn_complaints);
        btn_contactus = (Button) findViewById(R.id.btn_contactus);
       // btn_massage = (Button) findViewById(R.id.btn_massage);
        btn_media = (Button) findViewById(R.id.btn_media);

        btn_imp_forms = (Button) findViewById(R.id.btn_imp_forms);
        btn_imp_webs = (Button) findViewById(R.id.btn_imp_webs);
        btn_pcmc_scheme = (Button) findViewById(R.id.btn_pcmc_scheme);
        btn_jobs = (Button) findViewById(R.id.btn_jobs);

        img_share = (ImageView) findViewById(R.id.img_share);
        past_count = (ImageView) findViewById(R.id.past_count);


        btn_support = (ImageView) findViewById(R.id.btn_support);
        radioLangGroup = (RadioGroup) findViewById(R.id.LanguageRadioGroup);
        pager_list_data = new ArrayList<>();

        list_data=new ArrayList<HashMap<String, String>>();

        //setclick listener
        btn_home.setOnClickListener(this);
        btn_my_info.setOnClickListener(this);
        btn_psst_work.setOnClickListener(this);
        btn_future_work.setOnClickListener(this);
        btn_emerg_servies.setOnClickListener(this);
        btn_imp_numbers.setOnClickListener(this);
        btn_e_services.setOnClickListener(this);
        btn_news_update.setOnClickListener(this);
        btn_events.setOnClickListener(this);
        btn_voter_list.setOnClickListener(this);
        btn_complaints.setOnClickListener(this);
        btn_contactus.setOnClickListener(this);
        btn_imp_forms.setOnClickListener(this);
        btn_imp_webs.setOnClickListener(this);
        btn_pcmc_scheme.setOnClickListener(this);
        btn_jobs.setOnClickListener(this);
        btn_support.setOnClickListener(this);
        img_share.setOnClickListener(this);
       // btn_massage.setOnClickListener(this);
        btn_media.setOnClickListener(this);

        if (internet.isConnectingToInternet()) {
            getImages();
        } else {
            internet.showAlertDialog(HomeActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(past_count);
        Glide.with(this).load(R.raw.new_blink).into(imageViewTarget);*/

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_home: {
                Intent i = new Intent(HomeActivity.this, IntroductionActivity.class);
                i.putExtra("TAG", "Introduction");
                startActivity(i);
                break;
            }
            case R.id.btn_my_info: {
                Intent i = new Intent(HomeActivity.this, MyInfoActivity.class);
//                i.putExtra("TAG", "Promises");
                startActivity(i);
                break;
            }
            case R.id.btn_psst_work: {
                Intent i = new Intent(HomeActivity.this, PastWorkActivity.class);
                i.putExtra("type", "Past work");
                startActivity(i);
                break;
            }
            case R.id.btn_future_work: {
                Intent i = new Intent(HomeActivity.this, FutureWorkActivity.class);
                i.putExtra("type", "Future work");
                startActivity(i);
                break;
            }
            case R.id.btn_emerg_servies: {
                Intent i = new Intent(HomeActivity.this, EmergencyServicesActivity.class);
                startActivity(i);
                break;
            }
            case R.id.btn_imp_numbers: {
                Intent i = new Intent(HomeActivity.this, ImportantNumbersActivity.class);
                startActivity(i);
                break;
            }
            case R.id.btn_e_services: {
                Intent i = new Intent(HomeActivity.this, EServices.class);
                startActivity(i);
                break;
            }
            case R.id.btn_news_update: {
                Intent i = new Intent(HomeActivity.this, NewsAndUpdatesActivity.class);
                startActivity(i);
                break;
            }
            case R.id.btn_events: {
                Intent i = new Intent(HomeActivity.this, EventsActivity.class);
                startActivity(i);
                break;
            }
            case R.id.btn_voter_list: {
                Intent i = new Intent(HomeActivity.this, VoterListActivity.class);
                i.putExtra("link", "http://103.23.150.139/marathi/");
                i.putExtra("title", "" + getString(R.string.str_voter_list));
                startActivity(i);
                break;
            }
            case R.id.btn_complaints: {
                Intent i = new Intent(HomeActivity.this, ComplaintActivity.class);
                startActivity(i);
                break;
            }
            case R.id.btn_contactus: {
                Intent i = new Intent(HomeActivity.this, ContactUsActivity.class);
                startActivity(i);
                break;
            }

            case R.id.btn_imp_forms: {
                Intent i = new Intent(HomeActivity.this, UsefulFormsActivity.class);
                startActivity(i);
                break;
            }
            case R.id.btn_imp_webs: {
                Intent i = new Intent(HomeActivity.this, ImpLinksActivity.class);
                startActivity(i);
                break;
            }
            case R.id.btn_pcmc_scheme: {
                Intent i = new Intent(HomeActivity.this, PcmcSchemesActivity.class);
               /* i.putExtra("link", "http://rslinfotech.in/SanjayWabale/admin/info.html");
                i.putExtra("title", "" + getString(R.string.str_pcmc_scheme));*/
                startActivity(i);
                break;
            }
            case R.id.btn_jobs: {
                Intent i = new Intent(HomeActivity.this, JobOpprtunityActivity.class);
                startActivity(i);
                break;
            }

            case R.id.btn_support: {
               /* Intent i = new Intent(HomeActivity.this, SupportActivity.class);
                startActivity(i);*/
                new getSupport().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mobile_number,month_name,sp.getString("my_lang", "english"));

                break;
            }
          /*  case R.id.btn_massage: {
                Intent i = new Intent(HomeActivity.this, MassageActivity.class);
                startActivity(i);
                break;
            }*/
            case R.id.btn_media: {
                Intent i = new Intent(HomeActivity.this, MediaActivity.class);
                startActivity(i);
                break;
            }
            case R.id.img_share: {

                if (internet.isConnectingToInternet()) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    //add a subject
                    sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                            getResources().getString(R.string.app_name));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.rsl.sanjaywable&hl=en");
                    sendIntent.setType("text/plain");
                    startActivityForResult(sendIntent, send_data);
                } else {
                    internet.showAlertDialog(HomeActivity.this,
                            "No Internet Connection",
                            "You don't have internet connection.", false);
                }
                break;
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private Locale myLocale;

    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        updateTexts();
    }

    public void saveLocale(String lang) {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.apply();
    }

    public void loadLocale() {
        String langPref = "Language";
        SharedPreferences prefs = getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "en");
        changeLang(language);
    }

    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // myLocale = new Locale(LocaleHelper.getLanguage(getApplicationContext()));
        if (myLocale != null) {
            Log.e("onConfigurationChanged", "====onConfigurationChanged" + myLocale.getDisplayLanguage());
            newConfig.locale = myLocale;
            Locale.setDefault(myLocale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    private void sendDeviceId() {
        final RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);
        String requestURL = getString(R.string.web_path) + "get_device_token.php";
        Log.e("check web_path", requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                JSONObject jobj;
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            Log.d("response", "" + result);
                            jobj = new JSONObject(result);
                            if (jobj.getString("result").equals("success")) {
                                sp.edit().putBoolean("sendDeviceId", true).apply();
                            } else {
                                sp.edit().putBoolean("sendDeviceId", false).apply();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                fcm_id = FirebaseInstanceId.getInstance().getToken();
                params.put("device_token", fcm_id);
                Log.e("device_token_params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void updateTexts() {
        btn_home.setText(R.string.str_home);
        btn_my_info.setText(R.string.str_my_info);
        btn_psst_work.setText(R.string.str_past_work);
        btn_future_work.setText(R.string.str_future_work);
        btn_emerg_servies.setText(R.string.str_emergency_service);
        btn_imp_numbers.setText(R.string.str_imp_numbers);
        btn_e_services.setText(R.string.str_e_services);
        btn_news_update.setText(R.string.str_news_update);
        btn_events.setText(R.string.str_events);
        btn_voter_list.setText(R.string.str_voter_list);
       // btn_support.setText(R.string.str_support_wable);
        btn_complaints.setText(R.string.str_complaints);
        btn_imp_forms.setText(R.string.str_online_forms);
        btn_imp_webs.setText(R.string.str_imp_webs);
        btn_pcmc_scheme.setText(R.string.str_pcmc_scheme);
        btn_jobs.setText(R.string.str_jobs);
        btn_contactus.setText(R.string.str_contact_us);
       // btn_massage.setText(R.string.str_massage);
        btn_media.setText(R.string.str_media);
    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onResume() {
        mDemoSlider.startAutoCycle();
        super.onResume();
       /* if (internet.isConnectingToInternet()) {
            getImages();
        } else {
            internet.showAlertDialog(HomeActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }*/
    }

    private void getImages() {
        final ProgressDialog dialog = new ProgressDialog(HomeActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        //swipetorefresh.setRefreshing(true);
        final RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);
        String requestURL = getString(R.string.web_path) + "home.php";

        Log.e("requestURL","home"+ requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", "home"+response);
                        try {

                            dialog.dismiss();
                           // list_data.clear();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                if (jObj_result.getJSONArray("images").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "images").length(); i++) {

                                        JSONObject jobj_Data = jObj_result.getJSONArray("images").getJSONObject(i);

                                        HashMap<String, String> offers = new HashMap<String, String>();
                                        offers.put("image_url",jobj_Data.getString("image_path"));
                                        list_data.add(offers);
                                    }
                                }

                                for (int i = 0; i <list_data.size(); i++) {
                                    TextSliderView textSliderView = new TextSliderView(HomeActivity.this);
                                    // initialize a SliderLayout
                                    textSliderView
                                            .image(list_data.get(i).get("image_url"))
                                            .setScaleType(BaseSliderView.ScaleType.Fit);
                                    mDemoSlider.addSlider(textSliderView);
                                }

                            } else if(jObj_result.getString("result").equals(
                                    "failed")){
                                Toast.makeText(HomeActivity.this.getApplicationContext(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private class getSupport extends AsyncTask<String, Void, String> {

        String cr_date = "";
        final ProgressDialog dialog = new ProgressDialog(HomeActivity.this);

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            Log.w("upload", "startd....");
            //dialog.setMessage(getString(R.string.str_registering));
            dialog.setCancelable(false);
            dialog.show();
        }


        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            Log.w("upload", "uploading.....");
            StringBuffer result = new StringBuffer();
            String upLoadServerUri = getString(R.string.web_path) + "support.php";
            Log.e("URL", "=" + upLoadServerUri.replaceAll(" ", "%20"));
            try {
                String charset = "UTF-8";
                MultipartUtility multipart = new MultipartUtility(upLoadServerUri, charset);
                multipart.addHeaderField("User-Agent", "SanjayWable");
                multipart.addHeaderField("Test-Header", "Header-Value");
                multipart.addFormField("mobile_number",params[0]);
                multipart.addFormField("month",""+ params[1]);
                multipart.addFormField("language",""+params[2]);
                multipart.addFormField("repost", "false");
                Log.e("params[0]","--"+params[0]);
                Log.e("params[1]","--"+params[1]);
                Log.e("params[2]","--"+params[2]);
                //multipart.addFormField("device_token", "" + FirebaseInstanceId.getInstance().getToken());


                List<String> response = multipart.finish();
                Log.e("multipart", response.toString());

                System.out.println("SERVER_REPLIED:");
                for (String line : response) {
                    System.out.println("" + line);
                    result.append(line);
                }

            } catch (Exception e) {
                e.printStackTrace();
                dialog.dismiss();
                // something went wrong. connection with the server error
            }
            return result.toString();
        }

        @Override
        protected void onPostExecute(String response) {
            // TODO Auto-generated method stub
            Log.w("onPostExecute", "onPostExecute==========" + response);
            JSONObject jObj;
            if (response != null) {
                Log.e("complaint_registration", response);
                if (!response.startsWith("null")) {
                    try {
                        jObj = new JSONObject(response);
                        if (jObj.length() > 0) {
                            if (jObj.getString("result").equals("success")) {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                Toast.makeText(HomeActivity.this,jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                    Log.e("msg","---"+jObj.getString("msg"));
                                    Intent i=new Intent(HomeActivity.this,SupportGraphActivity.class);
                                    i.putExtra("msg",jObj.getString("msg"));
                                    startActivity(i);

                               /// finish();
                            } else if (jObj.getString("result").equals("failed")) {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                                Toast.makeText(HomeActivity.this,jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                                Log.e("msg","+++"+jObj.getString("msg"));
                                Intent i=new Intent(HomeActivity.this,SupportGraphActivity.class);
                                i.putExtra("msg",jObj.getString("msg"));
                                startActivity(i);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (dialog.isShowing())
                            dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (dialog.isShowing())
                        dialog.dismiss();
                }
            } else {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        }
    }
}
