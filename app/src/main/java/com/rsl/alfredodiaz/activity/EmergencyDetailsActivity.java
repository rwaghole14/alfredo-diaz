package com.rsl.alfredodiaz.activity;

import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.adapter.ExpandableListAdapter;
import com.rsl.alfredodiaz.model.EmergencyServices;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmergencyDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    @BindView(R.id.expand_service_list)
    ExpandableListView mExpandableServiceList;

    @BindView(R.id.img_home_slider)
    ImageView mEmergencyImageView;

    List<String> mListDataHeader;
    HashMap<String, List<String>> listDataChild;
    ExpandableListAdapter mExpandableListAdapter;
    String[] mEmerArray;

//    ArrayList<GroupData> group_list = new ArrayList<GroupData>();
    ArrayList<EmergencyServices> mEmergencyServices = new ArrayList<>();

    private String mMainTitle;
    private ConnectionDetector mConnection;
    private RequestQueue mQueue;
    private ArrayList<EmergencyServices> mServicesHeaderList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_emergency_details);
        ButterKnife.bind(this);
        mConnection = new ConnectionDetector(this);
        if (getIntent() != null) {
            int pos = Integer.parseInt(getIntent().getStringExtra("position"));
            mMainTitle = getIntent().getStringExtra("title");
        }
        initView();
//        mEmergencyImageView.setImageResource(EmergencyServicesActivity.imageId[pos]);
//        Log.e("position", "====" + pos);
//        mExpandableListAdapter = new ExpandableListAdapter(this,mEmergencyServices);
//        prepareListData(pos);
        // setting list adapter


        mExpandableListAdapter = new ExpandableListAdapter(this, mEmergencyServices);
        mExpandableServiceList.setAdapter(mExpandableListAdapter);

        if (mConnection.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(EmergencyDetailsActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
                    getServiceDetails();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            mConnection.showAlertDialog(EmergencyDetailsActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mMainTitle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getServiceDetails() {

        final String requestURL = getString(R.string.web_path) + "get_EmergencyServices.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        JSONArray mDataObjectArray = baseJSONObject.getJSONArray("data");
                                        if (mDataObjectArray.length() > 0){
                                            for (int i = 0; i < mDataObjectArray.length(); i++){
                                                JSONObject mDataJSONObject = mDataObjectArray.getJSONObject(i);
                                                String title = mDataJSONObject.getString("main_title");
                                                if (title.equals(mMainTitle)){
                                                    String imageUrl = null;
                                                    JSONArray mEmergencyDataArray = mDataJSONObject.getJSONArray("EmergencySer_data");
                                                    for (int k = 0; k < mEmergencyDataArray.length(); k++){
                                                        JSONObject mEmergencyServiceObject = mEmergencyDataArray.getJSONObject(k);

                                                        String mSubTitle = mEmergencyServiceObject.getString("sub_title");
                                                        String mContactNo = mEmergencyServiceObject.getString("phone");
                                                        String mAddress = mEmergencyServiceObject.getString("address");
                                                        imageUrl = mEmergencyDataArray.getJSONObject(0).getString("image");
                                                        mServicesHeaderList.add(new EmergencyServices(mSubTitle, mAddress, mContactNo));
                                                    }
                                                    prepareList(mServicesHeaderList, imageUrl);
                                                }
                                            }
                                        }

                                    }else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                }

                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        mQueue.add(stringRequest);
    }

    private void prepareList(ArrayList<EmergencyServices> mHeaderList,
                             String mUrl){

        try {
            Glide.with(this)
                    .load(mUrl)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            mProgressbar2.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            mProgressbar2.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(mEmergencyImageView);
        }catch (Exception e){
            e.printStackTrace();
        }

        mEmergencyServices.clear();

        for (int i = 0; i < mHeaderList.size(); i++) {
            EmergencyServices mEmergencyObject = new EmergencyServices();
            mEmergencyObject.setmEmergencySubTitle(mHeaderList.get(i).getmEmergencySubTitle());

            ArrayList<HashMap<String, String>> mChildData = new ArrayList<>();
            HashMap<String, String> mChild = new HashMap<>();
            mChild.put("phone", mHeaderList.get(i).getmEmergencyServiceContact());
            mChild.put("address", mHeaderList.get(i).getmEmergencyServiceAddress());
            mChildData.add(mChild);

            mEmergencyObject.setChildItems(mChildData);
            mEmergencyServices.add(mEmergencyObject);
        }
        mExpandableListAdapter.notifyDataSetChanged();
    }

    private void prepareListData(int pos) {
//        group_list.clear();
        mListDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        Arrays.sort(mEmerArray);
        for (int i = 0; i < mEmerArray.length; i++) {
            String[] a = mEmerArray[i].split("/");

            GroupData grp_data = new GroupData();
            grp_data.setGroup_name(a[0]);
            ArrayList<HashMap<String, String>> data = new ArrayList<>();
            HashMap<String, String> child = new HashMap<>();
            child.put("phone", a[1]);
            child.put("address", a[2]);
            data.add(child);

            grp_data.setChild_items(data);

//            group_list.add(grp_data);
        }
        mExpandableListAdapter.notifyDataSetChanged();
    }
}
