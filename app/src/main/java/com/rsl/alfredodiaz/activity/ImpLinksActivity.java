package com.rsl.alfredodiaz.activity;

import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImpLinksActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.list_emer_services)
    ListView mImpLinksListView;

    @BindView(R.id.img_home_slider)
    ImageView mSliderImageView;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    @BindView(R.id.horizontal_line)
    View mLine;

    ArrayList<HashMap<String, String>> items;
    private LayoutInflater inflater;
    MyCustomAdapter adapter;
    String[] mEmerArray;

    private RequestQueue mQueue;
    private ConnectionDetector internet;
    private String mImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imp_links);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Constants.activity_impwed = true;
        ButterKnife.bind(this);
        initView();
        mProgressbar2.setVisibility(View.INVISIBLE);
        mLine.setVisibility(View.INVISIBLE);
        this.inflater = LayoutInflater.from(ImpLinksActivity.this);
        mEmerArray = getResources().getStringArray(R.array.important_links_header);
        items = new ArrayList<>();
        adapter = new MyCustomAdapter();
        internet = new ConnectionDetector(this);
//        for (int i = 0; i < mEmerArray.length; i++) {
//            HashMap<String, String> d = new HashMap<>();
//            String[] a = mEmerArray[i].split(",");
//            d.put("title", a[0]);
//            d.put("link", a[1]);
//            adapter.add(d);
//        }
//        mImpLinksListView.setAdapter(adapter);


        if (internet.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(ImpLinksActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
//                    getImages();
                    getImpLinks();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            internet.showAlertDialog(ImpLinksActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }


        mImpLinksListView.setDividerHeight(0);
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.str_imp_webs));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private class MyCustomAdapter extends BaseAdapter {
        public void add(HashMap<String, String> data) {
            items.add(data);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.row_political_data, null);
            }
            TextView txt_item = (TextView) row.findViewById(R.id.txt_item);
            txt_item.setText(items.get(position).get("title"));

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == 3) {
                        Intent i = new Intent(ImpLinksActivity.this, UsefulFormDetailsActivity.class);
                        i.putExtra("title", "" + items.get(position).get("title"));
                        i.putExtra("link", items.get(position).get("link"));
                        startActivity(i);
                    } else {
                        Intent i = new Intent(ImpLinksActivity.this, VoterListActivity.class);
                        i.putExtra("position", String.valueOf(position));
                        i.putExtra("title", "" + items.get(position).get("title"));
                        i.putExtra("link", items.get(position).get("link"));
                        startActivity(i);
                    }
                }
            });
            return row;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getImpLinks(){

        String requestURL = getString(R.string.web_path) + "get_ImpWeb.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.d("WorkDetails", "got response");
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        mImageUrl = baseJSONObject.getString("image");
                                        Log.d("WorkDetails", "got response" + mImageUrl);
                                        loadImage(mImageUrl);
                                        JSONArray dataArray = baseJSONObject.getJSONArray("data");
                                        if (dataArray.length() > 0){
                                            for (int i = 0; i < dataArray.length(); i++){
                                                JSONObject dataObject = dataArray.getJSONObject(i);
                                                String mainTitle = dataObject.getString("title");
                                                String mWebLink = dataObject.getString("impWeb_link");
                                                HashMap<String, String> mData = new HashMap<>();
                                                mData.put("title", mainTitle);
                                                mData.put("link", mWebLink);
                                                adapter.add(mData);
                                            }
                                            mImpLinksListView.setAdapter(adapter);
                                            mLine.setVisibility(View.VISIBLE);
                                        }
                                    }else {
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        String msg = baseJSONObject.getString("msg");
                                        Toast.makeText(ImpLinksActivity.this, ""+ msg, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mProgressbarLayout.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
            }
        });

        mQueue.add(stringRequest);
    }
    private void loadImage(String url){
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(ImpLinksActivity.this)
                .load(url)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mSliderImageView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_impwed = false;
    }

}
