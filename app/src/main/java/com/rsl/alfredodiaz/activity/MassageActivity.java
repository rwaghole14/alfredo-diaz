package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MassageActivity extends AppCompatActivity {
    //    WebView webView;
    TextView text_description;
    ConnectionDetector internet;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_massage);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Constants.activity_message = true;
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_massage));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        text_description = (TextView) findViewById(R.id.text_description);

//        webView = (WebView) findViewById(R.id.webView);
//        webView.getSettings().setBuiltInZoomControls(true);
        internet = new ConnectionDetector(MassageActivity.this);

//        if (internet.isConnectingToInternet()) {
//            if (URLUtil.isValidUrl(url)) {
//                webView.setWebViewClient(new MyWebViewClient());
//                webView.getSettings().setJavaScriptEnabled(true);
//                webView.loadUrl(url);
//            }
//        } else {
//            Toast.makeText(MassageActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
//            progress.setVisibility(View.GONE);
//            webView.setVisibility(View.GONE);
//        }

        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);

        if (internet.isConnectingToInternet())
            getDescription();
        else {
            internet.showAlertDialog(MassageActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }


        // text_description.setText(getResources().getString(R.string.intro));
    }

    private void getDescription() {
        final ProgressDialog dialog = new ProgressDialog(MassageActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(MassageActivity.this);
        String requestURL = getString(R.string.web_path) + "get_message.php";
        Log.e("requestURL", requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", response);
                        try {

                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);

                            if (jObj_result.getString("result").equals("success")) {
                                text_description.setText(Html.fromHtml(jObj_result.getString("message")));
//                                webView.loadData(htmlText, "text/html; charset=UTF-8", null);
                            } else if (jObj_result.getString("result").equals(
                                    "failed")) {
                                Toast.makeText(MassageActivity.this, "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_message = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
