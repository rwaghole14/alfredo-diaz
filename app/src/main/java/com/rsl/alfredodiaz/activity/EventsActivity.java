package com.rsl.alfredodiaz.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.Event;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;
import com.rsl.alfredodiaz.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@SuppressLint("SimpleDateFormat")
public class EventsActivity extends AppCompatActivity {
    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;
    ListArrayAdapter listArrayAdapter;
    ListView listView;
    ArrayList<Event> eventItems;
    int currentMonth, currentYear;
    SharedPreferences sp;
    ConnectionDetector internet;

//    private void setCustomResourceForDates() {
//        Calendar cal = Calendar.getInstance();
//
//        // Min date is last 7 days
//        cal.add(Calendar.DATE, -7);
//        Date blueDate = cal.getTime();
//
//        // Max date is next 7 days
//        cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, 7);
//        Date greenDate = cal.getTime();
//
//        if (caldroidFragment != null) {
//            ColorDrawable blue = new ColorDrawable(getResources().getColor(R.color.blue));
//            ColorDrawable green = new ColorDrawable(Color.GREEN);
//            caldroidFragment.setBackgroundResourceForDate(R.color.blue, blueDate);
//            caldroidFragment.setBackgroundResourceForDate(R.color.green, greenDate);
//            caldroidFragment.setTextColorForDate(R.color.white, blueDate);
//            caldroidFragment.setTextColorForDate(R.color.white, greenDate);
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Constants.activity_event = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_events));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        internet = new ConnectionDetector(EventsActivity.this);
        eventItems = new ArrayList<>();
        ArrayList<Event> items = new ArrayList<Event>();
        listArrayAdapter = new ListArrayAdapter(EventsActivity.this, R.layout.list_view_event_item, items);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(listArrayAdapter);
        final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        // Setup caldroid fragment
        // **** If you want normal CaldroidFragment, use below line ****
        caldroidFragment = new CaldroidFragment();

        // //////////////////////////////////////////////////////////////////////
        // **** This is to show customized fragment. If you want customized
        // version, uncomment below line ****
//		 caldroidFragment = new CaldroidSampleCustomFragment();

        // Setup arguments

        // If Activity is created after rotation
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        }
        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            caldroidFragment.setArguments(args);
        }

        //setCustomResourceForDates();
        // Attach to the activity
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                Log.e("month/year", month + "/" + year);
                Log.e("date", "==========" + date.toString());
                showEventsForDate(date);
            }

            @Override
            public void onChangeMonth(int month, int year) {
                currentMonth = month;
                currentYear = year;
                if (internet.isConnectingToInternet())
                    loadEventsData(month, year);
                else {
                    internet.showAlertDialog(EventsActivity.this,
                            "No Internet Connection",
                            "You don't have internet connection.", false);
                }
                if (listArrayAdapter != null) {
                    listArrayAdapter.clear();
                    listArrayAdapter.notifyDataSetChanged();
                }
            }
        };

        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);


    }

    private void loadEventsData(final int month, final int year) {
        final ProgressDialog dialog = new ProgressDialog(EventsActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(EventsActivity.this);

        String requestURL = getString(R.string.web_path) + "events.php";
        Log.e("events_path", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                JSONObject jobj;
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            Log.d("events_response", "" + result);
                            jobj = new JSONObject(result);
                            if (jobj.getString("result").equals("success")) {
                                eventItems.clear();
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jobj.getJSONArray("data").length() > 0) {
                                    try {
                                        JSONArray events = jobj.getJSONArray("data");

                                        if (events.length() > 0) {
                                            Integer i;
                                            for (i = 0; i < events.length(); i++) {
                                                JSONObject obj = events.getJSONObject(i);
                                                String dateStr = obj.getString("date").split(" ")[0];
                                                String timeStr = obj.getString("date").split(" ")[1];
                                                Log.e("date", dateStr);
//                                                String[] dateTime = dateStr.split(" ");
                                                String[] separated = dateStr.split("-");
                                                Log.e("date", "y=" + separated[0] + "\nm=" + separated[1] + "\nd=" + separated[2]);

                                                Event event = new Event();
                                                event.setEvent_id(obj.getString("event_id"));
                                                event.setDate(dateStr);
                                                event.setTime(timeStr);
                                                event.setDescription(obj.getString("description"));
                                                event.setTitle(obj.getString("title"));
                                                event.setYear(Integer.parseInt(separated[0]));
                                                event.setMonth(Integer.parseInt(separated[1]));
                                                event.setDay(Integer.parseInt(separated[2]));

                                                Object object = obj.get("images");
                                                if (object instanceof JSONObject) {
                                                    // It is json object
                                                } else if (object instanceof JSONArray) {
                                                    ArrayList<HashMap<String, String>> images_array = new ArrayList<HashMap<String, String>>();
                                                    if (obj.getJSONArray("images").length() > 0 && obj.getJSONArray("images") != null) {
                                                        for (int j = 0; j < obj.getJSONArray("images").length(); j++) {
                                                            HashMap<String, String> images = new HashMap<String, String>();
                                                            JSONObject jobj_menu_items = obj.getJSONArray("images").getJSONObject(j);
                                                            images.put("image", jobj_menu_items.getString("image"));
                                                            images_array.add(images);
                                                        }
                                                    }
                                                    event.setEvent_images(images_array);
                                                }
                                                eventItems.add(event);
                                            }
                                        }
                                        loadLocalData(currentMonth, currentYear);
                                        showEventsForDate(new Date());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            } else {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.d("params", "events" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);


    }

    private void showEventsForDate(Date date) {
        listArrayAdapter.clear();
        Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
        Integer i;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        for (i = 0; i < eventItems.size(); i++) {
            Event item = eventItems.get(i);

            Log.e(dateFormat.format(date), item.date);
            if (dateFormat.format(date).equalsIgnoreCase(item.date)) {
//                caldroidFragment.setTextColorForDate(R.color.white, date);
//                caldroidFragment.setBackgroundResourceForDate(R.drawable.green_circle_gradiant, date);
                listArrayAdapter.add(item);
            }
        }
        listArrayAdapter.notifyDataSetChanged();
    }


    public class ListArrayAdapter extends ArrayAdapter<Event> {

        Context mContext;
        int layoutResourceId;
        //EventItem data[] = null;
        ArrayList<Event> data = null;

        public ListArrayAdapter(Context mContext, int layoutResourceId, ArrayList<Event> data) {

            super(mContext, layoutResourceId, data);

            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.data = data;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
        /*
         * The convertView argument is essentially a "ScrapView" as described is Lucas post
         * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
         * It will have a non-null value when ListView is asking you recycle the row layout.
         * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
         */
            View row = convertView;
            LayoutInflater inflater = LayoutInflater.from(EventsActivity.this);
            if (row == null) {
                row = inflater.inflate(R.layout.row_political_data, null);
            }
            final Event item = data.get(position);

            TextView txt_item = (TextView) row.findViewById(R.id.txt_item);
            txt_item.setText(StringUtils.trimTrailingWhitespace(Html.fromHtml(item.getTitle())));

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(EventsActivity.this, NewsUpdateDetailsActivity.class);
                    i.putExtra("description", item.getDescription());
                    i.putExtra("type", "events");
                    i.putExtra("id", data.get(position).getEvent_id());
                    i.putExtra("title", item.getTitle());
                    Log.e("position", "====" + position);
                    Constants.setImage_data(item.getEvent_images());
                    startActivity(i);
                }
            });
            return row;
        }
    }

    private void loadLocalData(int month, int year) {
        List<Event> eventList = getEventsForMonth(eventItems, year, month);

        Log.d("EventsFragments", eventList.toString());

        Calendar currentCalender = Calendar.getInstance(Locale.getDefault());

        eventItems = new ArrayList<>();
        for (Event event : eventList) {
            String dateTime = event.getDate();
            String[] separated = dateTime.split("-");
            currentCalender.set(Integer.parseInt(separated[0]), Integer.parseInt(separated[1]) - 1, Integer.parseInt(separated[2]));

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            Date today = calendar.getTime();

            String todayAsString = dateFormat.format(today);
            if (!(dateTime.equals(todayAsString)))
                caldroidFragment.setBackgroundResourceForDate(R.drawable.date_highlight_holiday, currentCalender.getTime());

            // -- FOR LIST VIEW --
            String[] dateSplit = event.date.split(" 00:00:00");
//            Event item = new Event(event.getEvent_id(), event.getTitle(), event.getDescription(), event.getDate(), event.getEvent_images());
//            item.se
            eventItems.add(event);
            //listArrayAdapter.add(item);
        }
        caldroidFragment.refreshView();
    }

    private List<Event> getEventsForMonth(List<Event> models, int year, int month) {
        final List<Event> filteredModelList = new ArrayList<>();
        for (Event model : models) {
            final int m = model.getMonth();
            final int y = model.getYear();
            if (m == month && y == year) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;

    }

    /**
     * Save current states of the Caldroid here
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

        if (caldroidFragment != null) {
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }

        if (dialogCaldroidFragment != null) {
            dialogCaldroidFragment.saveStatesToKey(outState,
                    "DIALOG_CALDROID_SAVED_STATE");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_event = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int year = cal.get(Calendar.YEAR);
                Log.e("month/year", month + "/" + year);
                Log.e("date", "==========" + date.toString());
                showEventsForDate(date);
            }

            @Override
            public void onChangeMonth(int month, int year) {
                currentMonth = month;
                currentYear = year;
                if (internet.isConnectingToInternet())
                    loadEventsData(month, year);
                else {
                    internet.showAlertDialog(EventsActivity.this,
                            "No Internet Connection",
                            "You don't have internet connection.", false);
                }
                if (listArrayAdapter != null) {
                    listArrayAdapter.clear();
                    listArrayAdapter.notifyDataSetChanged();
                }
            }
        };
        caldroidFragment.setCaldroidListener(listener);
    }
}
