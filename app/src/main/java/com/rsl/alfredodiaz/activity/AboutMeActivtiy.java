package com.rsl.alfredodiaz.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.fragments.AboutMeFragment;
import com.rsl.alfredodiaz.fragments.InspirationFragment;
import com.rsl.alfredodiaz.fragments.PoliticalDetailsFragment;

import de.hdodenhof.circleimageview.CircleImageView;

public class AboutMeActivtiy extends AppCompatActivity implements View.OnClickListener{

    CircleImageView btn_aboutme,btn_political,btn_inspiration;
    TextView txt_aboutme,txt_political,txt_inspiration;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_me_activtiy);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        initview();
    }

    private void initview() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        mToolTitle.setText(getString(R.string.app_name));
        setSupportActionBar(mToolbar);
         getSupportActionBar().setDisplayShowHomeEnabled(true);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         btn_aboutme=(CircleImageView) findViewById(R.id.btn_aboutme);
         btn_political=(CircleImageView)findViewById(R.id.btn_political);
         btn_inspiration=(CircleImageView)findViewById(R.id.btn_inspiration);
        txt_aboutme=(TextView)findViewById(R.id.txt_aboutme);
        txt_political=(TextView)findViewById(R.id.txt_political);
        txt_inspiration=(TextView)findViewById(R.id.txt_inspiration);
         btn_aboutme.setOnClickListener(this);
         btn_political.setOnClickListener(this);
         btn_inspiration.setOnClickListener(this);

        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/OpenSans-Light.ttf");
        txt_aboutme.setTypeface(face);
        txt_political.setTypeface(face);
        txt_inspiration.setTypeface(face);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_aboutme: {
                Intent i=new Intent(AboutMeActivtiy.this, AboutMeFragment.class);
                startActivity(i);
                break;
            }
            case R.id.btn_political: {
                Intent i=new Intent(AboutMeActivtiy.this, PoliticalDetailsFragment.class);
                startActivity(i);
                break;
            }
            case R.id.btn_inspiration: {
                Intent i=new Intent(AboutMeActivtiy.this, InspirationFragment.class);
                startActivity(i);
                break;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
