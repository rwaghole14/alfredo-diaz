package com.rsl.alfredodiaz.activity;

import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.adapter.ExpandJobsAdapter;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.model.JobModel;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobOpprtunityActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.expand_useful_links)
    ExpandableListView mJobExpandableListView;

    @BindView(R.id.img_home_slider)
    ImageView img_home_slider;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    private RequestQueue mQueue;
    private ConnectionDetector internet;


    ExpandJobsAdapter listAdapter;
    String[] mEmerArray;
    ArrayList<GroupData> group_list = new ArrayList<GroupData>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_opprtunity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Constants.activity_job = true;
        ButterKnife.bind(this);
        initView();
        mProgressbar2.setVisibility(View.INVISIBLE);

        internet = new ConnectionDetector(this);
        if (internet.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(JobOpprtunityActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
//                    getImages();
                    getJobDetails();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            internet.showAlertDialog(JobOpprtunityActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }


        listAdapter = new ExpandJobsAdapter(this, group_list);
//        prepareListData();
        // setting list adapter
        mJobExpandableListView.setAdapter(listAdapter);
    }


    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.str_imp_forms));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_job = false;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getJobDetails() {


        final String requestURL = getString(R.string.web_path) + "get_all_jobs.php" + "?language=english";
        Log.e("requestURL"," "+requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    Log.e("result", "" + result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        String mImageURL = baseJSONObject.getString("image");
                                        getImage(mImageURL);
                                        JSONArray mDataObjectArray = baseJSONObject.getJSONArray("data");
                                        if (mDataObjectArray.length() > 0){
                                            ArrayList<String> mJobTypeArray = new ArrayList<>();
                                            ArrayList<JobModel> mJobDataList = new ArrayList<>();
                                            for (int i = 0; i < mDataObjectArray.length(); i++){
                                                JSONObject mDataJSONObject = mDataObjectArray.getJSONObject(i);
                                                String mJobType = mDataJSONObject.getString("job_type");
                                                JSONArray mJobDataJSONArray = mDataJSONObject.getJSONArray("job_data");
                                                for (int k = 0; k < mJobDataJSONArray.length(); k++){
                                                    JSONObject mJobDataJSONObject = mJobDataJSONArray.getJSONObject(k);
                                                    String mTitle = mJobDataJSONObject.getString("title");
                                                    String mLink = mJobDataJSONObject.getString("job_link");
                                                    mJobDataList.add(new JobModel(mJobType, mTitle, mLink));
                                                }
                                                mJobTypeArray.add(mJobType);
                                            }
                                            prepareList(mJobTypeArray, mJobDataList);
                                        }

                                    }else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                }

                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        mQueue.add(stringRequest);
    }

    private void getImage(String mImageURL) {
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(mImageURL)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(img_home_slider);
    }

    private void prepareList(ArrayList<String> mJobTypeArray, ArrayList<JobModel> mJobDataList){
        group_list.clear();

        for (int i = 0; i < mJobTypeArray.size(); i++){
//            JobModel mJobModel = new JobModel();
            GroupData mJobModel = new GroupData();
//            mJobModel.setmJobType(mJobTypeArray.get(i));
            mJobModel.setGroup_name(mJobTypeArray.get(i));
            ArrayList<HashMap<String, String>> mJobData = new ArrayList<>();
            for (int k = 0; k < mJobDataList.size(); k++){
                String jobType = mJobDataList.get(k).getmJobType();
                if (jobType.equals(mJobTypeArray.get(i))){
                    HashMap<String, String> child = new HashMap<>();
                    child.put("title", mJobDataList.get(k).getmJobTitle());
                    child.put("link", mJobDataList.get(k).getmJobLink());
                    mJobData.add(child);
                }
            }
            mJobModel.setChild_items(mJobData);
            group_list.add(mJobModel);
        }
        listAdapter.notifyDataSetChanged();
    }

    private void prepareListData() {
        group_list.clear();
        mEmerArray = getResources().getStringArray(R.array.st_job_headers);


//        Arrays.sort(mEmerArray);
        for (int i = 0; i < mEmerArray.length; i++) {
            GroupData grp_data = new GroupData();
            grp_data.setGroup_name(mEmerArray[i]);
            ArrayList<HashMap<String, String>> data = new ArrayList<>();
            switch (i) {
                case 0: {
                    String[] a1 = getResources().getStringArray(R.array.st_job_headers1);
                    for (int j = 0; j < a1.length; j++) {
                        String[] a = a1[j].split(",");
                        HashMap<String, String> child = new HashMap<>();
                        child.put("title", a[0]);
                        child.put("link", a[1]);
                        data.add(child);
                    }
                }
                break;
                case 1: {
                    String[] a2 = getResources().getStringArray(R.array.st_job_headers2);
                    for (int j = 0; j < a2.length; j++) {
                        String[] a = a2[j].split(",");
                        HashMap<String, String> child = new HashMap<>();
                        child.put("title", a[0]);
                        child.put("link", a[1]);
                        data.add(child);
                    }
                }
                break;
            }
            grp_data.setChild_items(data);
            group_list.add(grp_data);
        }
        listAdapter.notifyDataSetChanged();
    }
}
