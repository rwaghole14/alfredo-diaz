package com.rsl.alfredodiaz.activity;

import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.adapter.ExpandUsefulLinksAdapter;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.model.OnlineForms;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UsefulFormsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.expand_useful_links)
    ExpandableListView mUsefullFormExpandableListView;

    @BindView(R.id.img_home_slider)
    ImageView mSliderImage;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    @BindView(R.id.horizontal_line)
    View mLine;

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    ExpandUsefulLinksAdapter listAdapter;
    String[] mEmerArray;
    ImageView img_home_slider;
    ArrayList<GroupData> group_list = new ArrayList<GroupData>();
    ArrayList<String> headers;

    private RequestQueue mQueue;
    private ConnectionDetector internet;
    private HashMap<String, List<String>> childs = new HashMap<>();
    private ExpandUsefulLinksAdapter mExpandableListAdapter;
    private ArrayList<OnlineForms> mFormHeaderList = new ArrayList<>();

    public static int[] imageId = {R.drawable.bg_govt_hospital, R.drawable.bg_st_depo, R.drawable.bg_ambulance, R.drawable.bg_water_tank};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_useful_forms);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Constants.activity_onlinefrom = true;
        ButterKnife.bind(this);
        initView();
        mProgressbar2.setVisibility(View.INVISIBLE);
        mLine.setVisibility(View.INVISIBLE);
        internet = new ConnectionDetector(this);

        if (internet.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(UsefulFormsActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
//                    getImages();
                    getUsefulForms();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            internet.showAlertDialog(UsefulFormsActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }

//        listAdapter = new ExpandUsefulLinksAdapter(this, mFormHeaderList);
//        prepareListData();
        // setting list adapter
        mExpandableListAdapter = new ExpandUsefulLinksAdapter(this, mFormHeaderList);
        mUsefullFormExpandableListView.setAdapter(mExpandableListAdapter);
    }


    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.str_online_forms));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getUsefulForms() {
        String requestURL = getString(R.string.web_path) + "get_OnlineForms.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.d("Online Forms", "got response");
                        JSONObject baseJSONObject;
                        if (result != null) {
                            if (!result.startsWith("null")) {
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")) {
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        String ImageUrl = baseJSONObject.getString("image");
                                        getImage(ImageUrl);
                                        JSONArray dataArray = baseJSONObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            String mainTitle = null;
                                            ArrayList<String> mTitleArray = new ArrayList<>();
                                            ArrayList<OnlineForms> mList = new ArrayList<>();
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject dataObject = dataArray.getJSONObject(i);
                                                mainTitle = dataObject.getString("main_title");
                                                mTitleArray.add(mainTitle);
                                                JSONArray mOnlineFormJSONArray = dataObject.getJSONArray("OnlineForms_data");
                                                for (int k = 0; k < mOnlineFormJSONArray.length(); k++) {
                                                    JSONObject onlineFormObject = mOnlineFormJSONArray.getJSONObject(k);
                                                    String mSubTitle = onlineFormObject.getString("sub_title");
                                                    String mFormLink = onlineFormObject.getString("form_link");
                                                    mList.add(new OnlineForms(mainTitle, mSubTitle, mFormLink));
                                                    Log.d("Online Forms", "SubTitle: " + mSubTitle);
                                                }

                                            }
                                            prepareList(mList, mTitleArray);
                                            mLine.setVisibility(View.VISIBLE);
//                                            mPhoneCategoryList.setAdapter(adapter);
                                        }
                                    }else {
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        String msg = baseJSONObject.getString("msg");
                                        Toast.makeText(UsefulFormsActivity.this, ""+ msg, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mProgressbarLayout.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
            }
        });

        mQueue.add(stringRequest);
    }

    private void getImage(String imageUrl) {
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mSliderImage);
    }

    private void prepareList(ArrayList<OnlineForms> mChildList, ArrayList<String> mTitleList) {
        mFormHeaderList.clear();

        for (int i = 0; i < mTitleList.size(); i++) {
            OnlineForms mOnlineForms = new OnlineForms();
            mOnlineForms.setmMainTitle(mTitleList.get(i));

            ArrayList<HashMap<String, String>> mChildData = new ArrayList<>();

            for (int k = 0; k < mChildList.size(); k++) {
                String mainTitle = mChildList.get(k).getmMainTitle();
                if (mainTitle.equals(mTitleList.get(i))){
                    HashMap<String, String> mChild = new HashMap<>();
                    mChild.put("title", mChildList.get(k).getmSubTitle());
                    mChild.put("link", mChildList.get(k).getmFormLink());
                    Log.d("Online Forms", "SubTitle1: " + mChildList.get(k).getmSubTitle());
                    mChildData.add(mChild);
                }
            }
            mOnlineForms.setmChildItems(mChildData);
            mFormHeaderList.add(mOnlineForms);
        }
        mExpandableListAdapter.notifyDataSetChanged();


    }

    private void prepareListData() {
        group_list.clear();
        mEmerArray = getResources().getStringArray(R.array.st_useful_forms_headers);


//        Arrays.sort(mEmerArray);
        for (int i = 0; i < mEmerArray.length; i++) {
            GroupData grp_data = new GroupData();
            grp_data.setGroup_name(mEmerArray[i]);
            ArrayList<HashMap<String, String>> data = new ArrayList<>();
            switch (i) {
                case 0: {
                    String[] a1 = getResources().getStringArray(R.array.st_useful_forms1);
                    for (int j = 0; j < a1.length; j++) {
                        String[] a = a1[j].split(",");
                        HashMap<String, String> child = new HashMap<>();
                        child.put("title", a[0]);
                        child.put("link", a[1]);
                        data.add(child);
                    }
                }
                break;
                case 1: {
                    String[] a2 = getResources().getStringArray(R.array.st_useful_forms2);
                    for (int j = 0; j < a2.length; j++) {
                        String[] a = a2[j].split(",");
                        HashMap<String, String> child = new HashMap<>();
                        child.put("title", a[0]);
                        child.put("link", a[1]);
                        data.add(child);
                    }
                }
                break;
                case 2: {
                    String[] a3 = getResources().getStringArray(R.array.st_useful_forms3);
                    for (int j = 0; j < a3.length; j++) {
                        String[] a = a3[j].split(",");
                        HashMap<String, String> child = new HashMap<>();
                        child.put("title", a[0]);
                        child.put("link", a[1]);
                        data.add(child);
                    }
                }
                break;
            }
            grp_data.setChild_items(data);
            group_list.add(grp_data);
        }
        listAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_onlinefrom = false;
    }
}
