package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WorkDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.txt_descr)
    TextView mDescriptionTextView;

    @BindView(R.id.text_view_title)
    TextView mTitleTextView;

    @BindView(R.id.text_view_estimated_cost)
    TextView mEstimateCost;

    @BindView(R.id.text_view_goverment_cost)
    TextView mGovermentCost;

    @BindView(R.id.text_view_actual_cost)
    TextView mActualCost;

    @BindView(R.id.text_view_actual_cost_label)
    TextView mActualCostLabel;

    @BindView(R.id.constraint_layout)
    ConstraintLayout mConstraintLayout;

    @BindView(R.id.horizontal_line)
    View mHorizontalLine;

    @BindView(R.id.view_pager_work)
    SliderLayout mDemoSlider;

    @BindView(R.id.img_slider)
    ImageView img_slider;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    private String mGroupId;
    private String mTitle;
    private String mType;
    private ArrayList<GroupData> mGroupDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_details);
        ButterKnife.bind(this);
        mProgressbar2.setVisibility(View.INVISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        if (getIntent() != null) {
            mTitle = getIntent().getStringExtra("title");
            mGroupId = getIntent().getStringExtra("group_id");
            mType = getIntent().getStringExtra("type");
            Log.d("WorkDetails", "Title is: " + mTitle);
        }
        initView();

        getWorkDetails(mGroupId);
    }//onCreate

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mTitle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

//    private void displayImages(){
//        TextSliderView textSliderView = new TextSliderView(this);
//        if (Constants.getImage_data().size() <= 0) {
//            textSliderView = new TextSliderView(this);
//            textSliderView.image(R.drawable.uttam4).setScaleType(BaseSliderView.ScaleType.Fit);
//            mDemoSlider.addSlider(textSliderView);
//        } else {
//            for (int i = 0; i < Constants.getImage_data().size(); i++) {
//                textSliderView = new TextSliderView(this);
//                // initialize a SliderLayout
//                String url = getString(R.string.image_path) + Constants.getImage_data().get(i).get("image");
//                textSliderView.image(url).setScaleType(BaseSliderView.ScaleType.Fit);
//                mDemoSlider.addSlider(textSliderView);
//            }
//        }
//        if (Constants.getImage_data().size() == 1) {
//            String url = getString(R.string.image_path) + Constants.getImage_data().get(0).get("image");
//            img_slider.setVisibility(View.VISIBLE);
//            Glide.with(WorkDetailsActivity.this.getApplicationContext())
//                    .load(url)
//                    .listener(new RequestListener<String, GlideDrawable>() {
//                        @Override
//                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            mProgressbar2.setVisibility(View.GONE);
//                            Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            mProgressbar2.setVisibility(View.GONE);
//                            return false;
//                        }
//                    })
//                    .placeholder(R.drawable.image_placeholder)
//                    .into(img_slider);
//
//            mDemoSlider.setVisibility(View.GONE);
//            mDemoSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
//            mDemoSlider.stopAutoCycle();
//            mDemoSlider.setEnabled(false);
//        }
//    }

    private void getWorkDetails(final String id) {
        final ProgressDialog dialog = new ProgressDialog(WorkDetailsActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();

        RequestQueue mQueue = Volley.newRequestQueue(WorkDetailsActivity.this);
        String requestURL = null;
        if (mType.equals("Past work")) {
            requestURL = getString(R.string.web_path) + "works.php" + "?language=english";
        }else if (mType.equals("Future work")){
            requestURL = getString(R.string.web_path) + "future_work.php" + "?language=english";
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.d("WorkDetails", "got response");
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                        JSONArray dataArray = baseJSONObject.getJSONArray("data");
                                        if (dataArray.length() > 0){
                                            for (int i = 0; i < dataArray.length(); i++){
                                                JSONObject dataObject = dataArray.getJSONObject(i);
                                                GroupData mGroupDataObject = new GroupData();

//                                                if (dataObject.getString("work_type").equals("Past work")){
                                                String mID = dataObject.getString("work_id");
//                                                    Log.d("WorkDetails", "work id: " + mID);
                                                if (mID.equals(id)){
                                                    mGroupDataObject.setGroup_name(dataObject.getString("title"));
                                                    mGroupDataObject.setGroup_id(dataObject.getString("work_id"));
                                                    mGroupDataObject.setGroup_description(dataObject.getString("description"));
                                                    mGroupDataObject.setType(dataObject.getString("work_type"));
                                                    mGroupDataObject.setmEstimateCost(dataObject.getString("estimated_cost"));
                                                    mGroupDataObject.setmGovermentCost(dataObject.getString("government_cost"));
                                                    if (dataObject.has("actual_cost")) {
                                                        mGroupDataObject.setmActualCost(dataObject.getString("actual_cost"));
                                                    }
                                                    ArrayList<String> mImageArray = new ArrayList<>();

//                                                        JSONObject imageObject = dataObject.get("images");
                                                    JSONArray mImageObjectArray = dataObject.getJSONArray("images");
                                                    if (mImageObjectArray.length() > 0){
                                                        for (int j = 0; j < mImageObjectArray.length(); j++){
                                                            JSONObject imageObject = mImageObjectArray.getJSONObject(j);
                                                            String mImageUrl = imageObject.getString("image");
                                                            mImageArray.add(mImageUrl);

                                                            mGroupDataObject
                                                                    .setGroup_image(mImageObjectArray.getJSONObject(0).getString("image"));
                                                        }

                                                    }
                                                    displayData(mGroupDataObject);
                                                    displayImages(mImageArray);
                                                }

//                                                }else if (dataObject.getString("work_type").equals("Future work")){
//
//                                                }
                                            }
                                        }
                                    }else {
                                        if (dialog.isShowing()) {
                                            dialog.dismiss();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    if (dialog.isShowing())
                                        dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                dialog.dismiss();
                            }
                        }else {
                            dialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(stringRequest);
    }

    private void displayData(GroupData mGroupDataObject) {
        mTitleTextView.setText(mGroupDataObject.getGroup_name());
        mDescriptionTextView.setText(mGroupDataObject.getGroup_description());
        mEstimateCost.setText(mGroupDataObject.getmEstimateCost());
        mGovermentCost.setText(mGroupDataObject.getmGovermentCost());
        if (!TextUtils.isEmpty(mGroupDataObject.getmActualCost())) {
            mActualCost.setText(mGroupDataObject.getmActualCost());
        }else {
            mActualCostLabel.setVisibility(View.GONE);

            ConstraintSet mConstraintSet = new ConstraintSet();
            mConstraintSet.clone(mConstraintLayout);
            mConstraintSet.connect(mHorizontalLine.getId(),
                    ConstraintSet.TOP, mGovermentCost.getId(), ConstraintSet.BOTTOM, 8);
            mConstraintSet.applyTo(mConstraintLayout);

        }
    }

    @Override
    protected void onResume() {
        mDemoSlider.startAutoCycle();

        super.onResume();
    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void displayImages(ArrayList<String> mImages) {
        Log.e("newsUpdate", "Imges: "+ mImages.size());
        if (mImages.size() == 1){
            img_slider.setVisibility(View.VISIBLE);
            Glide.with(this).load(mImages.get(0)).into(img_slider);
        }else {
            for (int i = 0; i < mImages.size(); i++) {
                TextSliderView textSliderView = new TextSliderView(WorkDetailsActivity.this);
                // initialize a SliderLayout
                textSliderView
                        .image(mImages.get(i))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
                mDemoSlider.addSlider(textSliderView);
            }
        }
    }
}
