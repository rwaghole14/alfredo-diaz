package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by admin on 12/27/2016.
 */

public class FutureWorkActivity extends AppCompatActivity {

    @BindView(R.id.img_home_slider)
    SliderLayout mDemoSlider;

//    @BindView(R.id.circular_progress2)
//    ProgressBar mProgressbar2;

    @BindView(R.id.img_slider)
    ImageView img_slider;

    private String mImageUrl;

    ListView listView;
    ArrayList<GroupData> group;
    //String tag;
    ConnectionDetector internet;
    SharedPreferences sp;
    MyCustomAdapter adapter;
    TextView txt_message;
    TextView mToolTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
//        mProgressbar2.setVisibility(View.INVISIBLE);
        Constants.activity_future = true;
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        txt_message = (TextView) findViewById(R.id.txt_message);
        mToolTitle.setText(getString(R.string.str_future_work));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        internet = new ConnectionDetector(FutureWorkActivity.this);
        //  Constants.isWorkActivityOpen = true;
        listView = (ListView) findViewById(R.id.recycler_view_work);
        group = new ArrayList<>();
        adapter = new FutureWorkActivity.MyCustomAdapter();
        listView.setAdapter(adapter);
        listView.setDividerHeight(0);
        if (internet.isConnectingToInternet()) {
            getWorkDone();
        } else {
            internet.showAlertDialog(FutureWorkActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    private void getWorkDone() {
        final ProgressDialog dialog = new ProgressDialog(FutureWorkActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(FutureWorkActivity.this);
        String requestURL = getString(R.string.web_path) + "future_work.php" + "?language=english";
        Log.e("check web_path", requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                JSONObject jobj;
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            Log.e("response", "" + result);
                            jobj = new JSONObject(result);
                            if (jobj.getString("result").equals("success")) {
                                group.clear();
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jobj.getJSONArray("data").length() > 0) {
                                    ArrayList<String> mImges = new ArrayList<>();
                                    for (int a = 0; a < jobj.getJSONArray("data").length(); a++) {
                                        JSONObject jobj_Data = jobj.getJSONArray("data").getJSONObject(a);
                                        GroupData grp_data = new GroupData();
                                        if (jobj_Data.getString("work_type").equals("Future work")) {
                                            grp_data.setGroup_name(jobj_Data.getString("title"));
                                            grp_data.setGroup_id(jobj_Data.getString("work_id"));
                                            grp_data.setGroup_description(jobj_Data.getString("description"));
                                            grp_data.setType(jobj_Data.getString("work_type"));
                                            JSONArray mJsonArray = jobj_Data.getJSONArray("images");
                                            for (int i = 0; i < mJsonArray.length(); i++) {
                                                JSONObject mImageObject = mJsonArray.getJSONObject(i);
                                                String imageUrl = mImageObject.getString("image");
                                                mImges.add(imageUrl);
                                                grp_data.setGroup_image(imageUrl);
                                            }

                                            adapter.add(grp_data);
                                            adapter.notifyDataSetChanged();
                                        }
                                        if (group.size() <= 0) {
                                            listView.setVisibility(View.GONE);
                                            txt_message.setVisibility(View.VISIBLE);
                                        } else {
                                            listView.setVisibility(View.VISIBLE);
                                            txt_message.setVisibility(View.GONE);
                                        }

                                    }
                                    displayImage(mImges);
                                }
                            } else {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
//                Log.d("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_future = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class MyCustomAdapter extends BaseAdapter {
        public void add(GroupData data) {
            group.add(data);
            Log.e("addddd", "" + data.getGroup_name());
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return group.size();
        }

        @Override
        public Object getItem(int position) {
            return group.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
//            View row = convertView;
            LayoutInflater inflater = LayoutInflater.from(FutureWorkActivity.this);
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.row_work_data, null);
            }
            TextView txt_data = (TextView) convertView.findViewById(R.id.txt_data);
            CircleImageView icon = (CircleImageView) convertView.findViewById(R.id.img_work);
//            if (group.get(position).getGroup_image().equals("") || group.get(position).getGroup_image().length() > 0) {
            String url = group.get(position).getGroup_image();
            Glide.with(getApplicationContext()).load(url).placeholder(R.drawable.ic_image_placeholder).dontAnimate().into(icon);
//            }
            txt_data.setText(group.get(position).getGroup_name());
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(FutureWorkActivity.this, WorkDetailsActivity.class);
                    i.putExtra("type", "Future work");
                    i.putExtra("title", group.get(position).getGroup_name());
                    i.putExtra("group_id", group.get(position).getGroup_id());
                    i.putExtra("description", group.get(position).getGroup_description());
                    Constants.setImage_data(group.get(position).getChild_items());
                    startActivity(i);
                }
            });
            return convertView;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (internet.isConnectingToInternet()) {
            getWorkDone();
            Log.e("onNewIntent", "onNewIntent");
        } else {
            internet.showAlertDialog(FutureWorkActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    private void displayImage(ArrayList<String> mImages) {
//        mProgressbar2.setVisibility(View.VISIBLE);
        if (mImages.size() > 5) {
            for (int i = 0; i < 5; i++) {
                TextSliderView textSliderView = new TextSliderView(FutureWorkActivity.this);
                // initialize a SliderLayout
                textSliderView
                        .image(mImages.get(i))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
                mDemoSlider.addSlider(textSliderView);
            }
        }else if (mImages.size() == 1){
            img_slider.setVisibility(View.VISIBLE);
            Glide.with(this).load(mImages.get(0)).into(img_slider);
        }
        else {
            for (int i = 0; i < mImages.size(); i++) {
                TextSliderView textSliderView = new TextSliderView(FutureWorkActivity.this);
                // initialize a SliderLayout
                textSliderView
                        .image(mImages.get(i))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
                mDemoSlider.addSlider(textSliderView);
            }
        }
    }

}