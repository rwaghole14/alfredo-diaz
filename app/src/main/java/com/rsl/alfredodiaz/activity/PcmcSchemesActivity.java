package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PcmcSchemesActivity extends AppCompatActivity {

    ConnectionDetector cd;
    MyCustomAdapter adapter;
    SharedPreferences sp;
    ConnectionDetector internet;
    ArrayList<HashMap<String, String>> list_data = null;

    ListView list_schemes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pcmc_schemes);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        );
        Constants.activity_pcmc = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_pcmc_scheme));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cd = new ConnectionDetector(getApplicationContext());
        sp = getSharedPreferences("sanjay_wable",MODE_PRIVATE);
        list_schemes = (ListView)findViewById(R.id.list_schemes);
        if (cd.isConnectingToInternet()) {
            getPcmcSchemes();
        } else {
            cd.showAlertDialog(PcmcSchemesActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        adapter = new MyCustomAdapter(this);
        list_data=new ArrayList<HashMap<String, String>>();
        list_schemes.setDividerHeight(0);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_pcmc = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Constants.activity_pcmc = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class MyCustomAdapter extends BaseAdapter {
        /*  public void add(String data) {
              items.add(data);
          }
  */
        private LayoutInflater inflater = null;
        Context context;

        public MyCustomAdapter(Context context) {
            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public  void add(HashMap<String, String> hash){
            list_data.add(hash);
            notifyDataSetChanged();
        }
        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return list_data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // View row = convertView;
           /* if (row == null) {
                row = inflater.inflate(R.layout.row_political_data, null);
            }
            TextView txt_item = (TextView) row.findViewById(R.id.txt_item);
            txt_item.setText(items.get(position));*/

            convertView = inflater.inflate(R.layout.row_political_data, parent, false);
            TextView txt_item = (TextView) convertView.findViewById(R.id.txt_item);
            txt_item.setText(list_data.get(position).get("title"));
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(context,SchemesDetailsActivity.class);
                    i.putExtra("description",list_data.get(position).get("description"));
                    startActivity(i);
                }
            });

            return convertView;
        }
    }

    private void getPcmcSchemes() {
        final ProgressDialog dialog = new ProgressDialog(PcmcSchemesActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(PcmcSchemesActivity.this);
        String requestURL = getString(R.string.web_path) + "schemes.php";
        Log.e("requestURL","schemes"+ requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", "schemes"+response);
                        try {

                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                if (jObj_result.getJSONArray("data").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "data").length(); i++) {
                                        JSONObject jobj_Data = jObj_result.getJSONArray("data").getJSONObject(i);
                                        HashMap<String, String> offers = new HashMap<String, String>();

                                        offers.put("scheme_id",jobj_Data.getString("scheme_id"));
                                        offers.put("title",jobj_Data.getString("title"));
                                        offers.put("description",jobj_Data.getString("description"));
                                        adapter.add(offers);

                                    }
                                    list_schemes.setAdapter(adapter);
                                }

                            } else if(jObj_result.getString("result").equals(
                                    "failed")){
                                Toast.makeText(PcmcSchemesActivity.this.getApplicationContext(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.e("params", "" + params);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }
}
