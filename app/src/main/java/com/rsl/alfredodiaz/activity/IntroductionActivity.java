package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IntroductionActivity extends AppCompatActivity {
    @BindView(R.id.img_home_slider)
    ImageView mSliderImage;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    ConnectionDetector internet;
    TextView text_description;
    ImageView img_home_slider;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
        mProgressbar2.setVisibility(View.INVISIBLE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_home));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        internet = new ConnectionDetector(IntroductionActivity.this);
        text_description = (TextView) findViewById(R.id.text_description);
        img_home_slider = (ImageView) findViewById(R.id.img_home_slider);
        //  text_description.setText(getResources().getString(R.string.intro));
        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        if (internet.isConnectingToInternet()) {
            getMystery();
        } else {
            internet.showAlertDialog(IntroductionActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }//onCreate

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getMystery() {
        final ProgressDialog dialog = new ProgressDialog(IntroductionActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(IntroductionActivity.this);
        String requestURL = getString(R.string.web_path) + "mystry.php";
        Log.e("requestURL","mystry"+ requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", "mystry"+response);
                        try {
                            String imageUrl = null;
                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                if (jObj_result.getJSONArray("mystry").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "mystry").length(); i++) {

                                        JSONObject jobj_Data = jObj_result.getJSONArray("mystry").getJSONObject(i);
                                        text_description.setText(jobj_Data.getString("information"));
                                        imageUrl= jobj_Data.getString("image");
                                    }
                                    displayImage(imageUrl);
                                }

                            } else if(jObj_result.getString("result").equals(
                                    "failed")){
                                Toast.makeText(IntroductionActivity.this.getApplicationContext(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.e("params", "" + params);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void displayImage(String imageUrl) {
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .placeholder(R.drawable.image_placeholder)
                .into(mSliderImage);
    }
}
