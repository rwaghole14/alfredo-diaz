package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAndUpdatesActivity extends AppCompatActivity {
    ListView listView;
    ArrayList<GroupData> list_data;
    MyCustomAdapter adapter;
    ConnectionDetector internet;
    SharedPreferences sp;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressBar2;

    @BindView(R.id.img_home_slider)
    SliderLayout mDemoSlider;

    @BindView(R.id.horizontal_line)
    View mLine;

    private String mImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constants.activity_news = true;
        setContentView(R.layout.activity_news_and_update);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
        mProgressBar2.setVisibility(View.INVISIBLE);
        mLine.setVisibility(View.INVISIBLE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_news_update));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        internet = new ConnectionDetector(NewsAndUpdatesActivity.this);
        listView = (ListView) findViewById(R.id.list_news);
        list_data = new ArrayList<>();
        adapter = new MyCustomAdapter();
        listView.setAdapter(adapter);
        listView.setDividerHeight(0);
        if (internet.isConnectingToInternet()) {
            getAllNews();
        } else {
            internet.showAlertDialog(NewsAndUpdatesActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    private void getAllNews() {
        final ProgressDialog dialog = new ProgressDialog(NewsAndUpdatesActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(NewsAndUpdatesActivity.this);

        String requestURL = getString(R.string.web_path) + "newsupdate.php";
        Log.e("check web_path", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                JSONObject jobj;
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            Log.v("response", "" + result);
                            jobj = new JSONObject(result);
                            if (jobj.getString("result").equals("success")) {
                                list_data.clear();
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                if (jobj.getJSONArray("data").length() > 0) {
                                    list_data.clear();
                                    ArrayList<String> mImages = new ArrayList<>();
                                    for (int a = 0; a < jobj.getJSONArray("data").length(); a++) {
                                        JSONObject jobj_Data = jobj.getJSONArray("data").getJSONObject(a);
                                        GroupData grp_data = new GroupData();

                                        grp_data.setGroup_name(jobj_Data.getString("title"));
                                        grp_data.setGroup_id(jobj_Data.getString("news_id"));
                                        grp_data.setGroup_description(jobj_Data.getString("description"));
                                        JSONArray mImageArray = jobj_Data.getJSONArray("images");
                                        for (int i = 0; i < mImageArray.length(); i++) {
                                            JSONObject mImageObject = mImageArray.getJSONObject(i);
                                            mImageUrl = mImageObject.getString("image");
                                            mImages.add(mImageUrl);
                                        }
                                        adapter.add(grp_data);
                                    }
                                    displayImage(mImages);
                                    mLine.setVisibility(View.VISIBLE);
                                }

                            } else {
                                if (dialog.isShowing()) {
                                    dialog.dismiss();
                                }
                                String msg = jobj.getString("msg");
                                Toast.makeText(NewsAndUpdatesActivity.this, ""+ msg, Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing())
                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        dialog.dismiss();
                    }
                } else {
                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.d("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void displayImage(ArrayList<String> mImages) {
        if (mImages.size() > 5) {
            for (int i = 0; i < 5; i++) {
                TextSliderView textSliderView = new TextSliderView(NewsAndUpdatesActivity.this);
                // initialize a SliderLayout
                textSliderView
                        .image(mImages.get(i))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
                mDemoSlider.addSlider(textSliderView);
            }
        }else {
            for (int i = 0; i < mImages.size(); i++) {
                TextSliderView textSliderView = new TextSliderView(NewsAndUpdatesActivity.this);
                // initialize a SliderLayout
                textSliderView
                        .image(mImages.get(i))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
                mDemoSlider.addSlider(textSliderView);
            }
        }
    }

    private class MyCustomAdapter extends BaseAdapter {


        public void add(GroupData data) {
            list_data.add(data);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return list_data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            LayoutInflater inflater = LayoutInflater.from(NewsAndUpdatesActivity.this);
            if (row == null) {
                row = inflater.inflate(R.layout.row_political_data, null);
            }
            TextView txt_item = (TextView) row.findViewById(R.id.txt_item);
            txt_item.setText(list_data.get(position).getGroup_name().trim());

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(NewsAndUpdatesActivity.this, NewsUpdateDetailsActivity.class);
                    i.putExtra("description", list_data.get(position).getGroup_description());
                    i.putExtra("type", "news");
                    Log.e("position", "====" + position);
                    i.putExtra("id", list_data.get(position).getGroup_id());
                    i.putExtra("title", list_data.get(position).getGroup_name());
                    Constants.setImage_data(list_data.get(position).getChild_items());
                    startActivity(i);
                }
            });
            return row;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_news = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Constants.activity_news = false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (internet.isConnectingToInternet()) {
            getAllNews();
            Log.e("onNewIntent", "onNewIntent");
        } else {
            internet.showAlertDialog(NewsAndUpdatesActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }
}
