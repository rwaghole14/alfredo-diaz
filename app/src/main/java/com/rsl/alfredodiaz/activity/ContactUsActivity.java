package com.rsl.alfredodiaz.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactUsActivity extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.linear_layout_call)
    LinearLayout mCallLayout;

    @BindView(R.id.linear_layout_FB)
    LinearLayout mFBLayout;

    @BindView(R.id.linear_layout_whatsApp)
    LinearLayout mWhatsAppLayout;

    @BindView(R.id.linear_layout_mail)
    LinearLayout mMailLayout;

    @BindView(R.id.juliet_text_view)
    TextView mNameTextView;

    @BindView(R.id.txt_fb_link)
    TextView mFBLink;

    @BindView(R.id.txt_email)
    TextView mEMailID;

    @BindView(R.id.str_tollfree_number)
    TextView mTollfreeNumber;

    @BindView(R.id.txt_num1)
    TextView mContactNum1;

    @BindView(R.id.txt_num2)
    TextView mContactNum2;

    @BindView(R.id.txt_company_address)
    TextView mCompanyAddress;

    private RequestQueue mQueue;
    private ConnectionDetector internet;
    private String mLatitude;
    private String mLongitude;
    private String mAddress;

    GoogleMap googleMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
        initView();
        hideViews();

        internet = new ConnectionDetector(this);

        if (internet.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(ContactUsActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
                    getContactUsDetails();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            internet.showAlertDialog(ContactUsActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }

        if (!checkLocationPermission()) {
            ActivityCompat.requestPermissions(ContactUsActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
        }

    }

    private void hideViews() {
        mNameTextView.setVisibility(View.INVISIBLE);
        mCallLayout.setVisibility(View.INVISIBLE);
        mFBLayout.setVisibility(View.INVISIBLE);
        mWhatsAppLayout.setVisibility(View.INVISIBLE);
        mMailLayout.setVisibility(View.INVISIBLE);
    }

    private void showView(){
        mNameTextView.setVisibility(View.VISIBLE);
        mCallLayout.setVisibility(View.VISIBLE);
        mFBLayout.setVisibility(View.VISIBLE);
        mWhatsAppLayout.setVisibility(View.VISIBLE);
        mMailLayout.setVisibility(View.VISIBLE);
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.str_contact_us));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        zoomCamera();
    }

    private void zoomCamera() {
        LatLng office = new LatLng(Double.parseDouble(mLatitude), Double.parseDouble(mLongitude));
//        if (checkLocationPermission()) {
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
//            googleMap.setMyLocationEnabled(true);
//            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(office, 12));
//            googleMap.addMarker(new MarkerOptions()
//                    .title(getString(R.string.str_office_address))
//                    .snippet(mAddress)
//                    .position(office)).showInfoWindow();
        try {
            MapsInitializer.initialize(ContactUsActivity.this);
            initilizeMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        }
    }

    private void initilizeMap() {
        Double latitude = Double.parseDouble(mLatitude);
        Double longitude =Double.parseDouble(mLongitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15.0f);
        googleMap.animateCamera(cameraUpdate);
        MarkerOptions marker = new MarkerOptions()
                .title(getResources().getString(R.string.map_title))
                .snippet(mAddress)
                .position(new LatLng(latitude, longitude));
        googleMap.addMarker(marker).showInfoWindow();
    }

    final private int REQUEST_CODE_LOCATION = 2;

    private boolean checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        } else
            return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    zoomCamera();
//                    onMapReady(googleMap);
                    setMap();
                } else {
                    Toast.makeText(ContactUsActivity.this, "Please allow Location permission from app ->settings", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getContactUsDetails(){
        String requestURL = getString(R.string.web_path) + "get_contact.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.d("WorkDetails", "got response");
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        JSONArray dataArray = baseJSONObject.getJSONArray("contact_details");
                                        if (dataArray.length() > 0){
                                            for (int i = 0; i < dataArray.length(); i++){
                                                JSONObject dataObject = dataArray.getJSONObject(i);
                                                String name = dataObject.getString("name");
                                                String phoneNumber = dataObject.getString("phone_number");
                                                String fbLink = dataObject.getString("fb_link");
                                                String whatsappNo = dataObject.getString("whatsapp_no");
                                                String mailId = dataObject.getString("gmail");
                                                String address = dataObject.getString("address");
                                                mAddress = address;
                                                mLatitude = dataObject.getString("latitude");
                                                mLongitude  = dataObject.getString("longitude");
                                                setData(name, phoneNumber, fbLink, whatsappNo, mailId, address);
                                                if (checkLocationPermission()) {
                                                    setMap();
                                                }
                                            }

                                        }
                                    }else {
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mProgressbarLayout.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
            }
        });

        mQueue.add(stringRequest);
    }

    private void setData(String name, final String phoneNumber,
                         String fbLink, final String whatsappNo, String gmail, String address){
        showView();
        mFBLink.setText(fbLink);

        mEMailID.setText(gmail);
        mTollfreeNumber.setText(phoneNumber);
        mContactNum1.setText(whatsappNo);

        mFBLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = getString(R.string.str_fb_link);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        mTollfreeNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });

        mContactNum1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + whatsappNo));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });


        mCompanyAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = getString(R.string.str_company_url);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        mEMailID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("text/plain");
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{getString(R.string.str_email_id)});
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.str_send_mail)), 1);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(ContactUsActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void setMap(){
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
}
