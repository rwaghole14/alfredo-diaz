package com.rsl.alfredodiaz.activity;

import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmergencyServicesActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    ListView list_emer_services;
    //    ViewPager view_pager;
    private SliderLayout mDemoSlider;
    int currentPage = 0;
    Timer timer;
    public static int[] imageId = {R.drawable.bg_govt_hospital, R.drawable.bg_private_hosp, R.drawable.bg_ambulance, R.drawable.bg_blood_bank, R.drawable.bg_police, R.drawable.bg_electricity, R.drawable.bg_fire_brigade};
    String[] mEmerArray;
    ArrayList<String> items;
    private LayoutInflater inflater;
    MyCustomAdapter adapter;
    public static final int NUM_PAGES = imageId.length;
    ConnectionDetector internet;
    ArrayList<HashMap<String, String>> list_data;
    private RequestQueue mQueue;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.horizontal_line)
    View mLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_services);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
      //  Constants.activity_emergency = true;
        ButterKnife.bind(this);
        mLine.setVisibility(View.INVISIBLE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_emergency_service));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.inflater = LayoutInflater.from(EmergencyServicesActivity.this);
        mEmerArray = getResources().getStringArray(R.array.emergency_header);
        mDemoSlider = (SliderLayout) findViewById(R.id.view_pager_e_services);
        list_emer_services = (ListView) findViewById(R.id.list_emer_services);
        internet = new ConnectionDetector(EmergencyServicesActivity.this);
        list_data=new ArrayList<HashMap<String, String>>();

        if (internet.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(EmergencyServicesActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
                    getServices();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);

                }
            }.start();

        } else {
            internet.showAlertDialog(EmergencyServicesActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
      /*  for (int i = 0; i < imageId.length; i++) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .image(imageId[i])
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            mDemoSlider.addSlider(textSliderView);
        }*/
        items = new ArrayList<>();
        adapter = new MyCustomAdapter();
        list_emer_services.setDividerHeight(0);
    }

    @Override
    protected void onResume() {
        mDemoSlider.startAutoCycle();
        super.onResume();
    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }


    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class MyCustomAdapter extends BaseAdapter {
        public void add(String data) {
            items.add(data);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.row_political_data, null);
            }
            TextView txt_item = (TextView) row.findViewById(R.id.txt_item);
            txt_item.setText(items.get(position));

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(EmergencyServicesActivity.this, EmergencyDetailsActivity.class);
                    i.putExtra("position", String.valueOf(position));
                    i.putExtra("title", items.get(position));
                    //Log.e("position", "===="+position);
                    startActivity(i);
                }
            });
            return row;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getImages(ArrayList<String> mImages) {
        for (int i = 0; i <mImages.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(EmergencyServicesActivity.this);
            // initialize a SliderLayout
            textSliderView
                    .image(mImages.get(i))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            mDemoSlider.addSlider(textSliderView);
        }
    }

    private void getServices(){

        String requestURL = getString(R.string.web_path) + "get_EmergencyServices.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.d("WorkDetails", "got response");
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        ArrayList<String> mImages = new ArrayList<>();
                                        JSONArray dataArray = baseJSONObject.getJSONArray("data");
                                        if (dataArray.length() > 0){
                                            for (int i = 0; i < dataArray.length(); i++){
                                                JSONObject dataObject = dataArray.getJSONObject(i);
                                                String mainTitle = dataObject.getString("main_title");
                                                JSONArray mEmergencyArray = dataObject.getJSONArray("EmergencySer_data");
                                                for (int k = 0; k < mEmergencyArray.length(); k++) {
                                                    JSONObject mEmergencyObject = mEmergencyArray.getJSONObject(k);
                                                    String mImage = mEmergencyObject.getString("image");
                                                    mImages.add(mImage);
                                                }
                                                adapter.add(mainTitle);
                                            }
                                            list_emer_services.setAdapter(adapter);
                                            getImages(mImages);
                                            mLine.setVisibility(View.VISIBLE);
                                        }
                                    }else {
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        String msg = baseJSONObject.getString("msg");
                                        Toast.makeText(EmergencyServicesActivity.this, ""+ msg, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mProgressbarLayout.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
            }
        });

        mQueue.add(stringRequest);
    }

}
