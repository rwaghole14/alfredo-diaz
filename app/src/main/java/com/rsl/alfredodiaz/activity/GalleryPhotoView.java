package com.rsl.alfredodiaz.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.fragments.PhotosFragment;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.HashMap;

public class GalleryPhotoView extends AppCompatActivity {

    GridView gridview;
    ConnectionDetector internet;
    ImageAdapter imageAdapter;
    String name;
    ArrayList<HashMap<String,String>> list_data;
    private int mPhotoSize, mPhotoSpacing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_photo_view);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_Photos));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        internet = new ConnectionDetector(getApplicationContext());

        gridview = (GridView) findViewById(R.id.gridview);
        list_data = new ArrayList<>();
        int pos = Integer.parseInt(getIntent().getExtras().getString("position"));
        name = getIntent().getStringExtra("name");
        list_data= PhotosFragment.list_data.get(pos).getGallery_images();
        //   list_data = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("arraylist");
        Log.e("list_data",""+list_data.toString());
        Log.e("list_datasize",""+list_data.size());
        mPhotoSize = getResources().getDimensionPixelSize(R.dimen.photo_size);
        mPhotoSpacing = getResources().getDimensionPixelSize(R.dimen.photo_spacing);
        imageAdapter = new ImageAdapter(this);
        gridview.setAdapter(imageAdapter);
        imageAdapter.notifyDataSetChanged();

        gridview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (imageAdapter.getNumColumns() == 0) {
                    final int numColumns = (int) Math.floor(gridview.getWidth() / (mPhotoSize + mPhotoSpacing));
                    if (numColumns > 0) {
                        final int columnWidth = (gridview.getWidth() / numColumns) - mPhotoSpacing;
                        imageAdapter.setNumColumns(numColumns);
                        imageAdapter.setItemHeight(columnWidth);
                    }
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public class ImageAdapter extends BaseAdapter {
        private LayoutInflater inflater = null;
        Context context;
        private RelativeLayout.LayoutParams mImageViewLayoutParams;
        private Animation anim_fade_in, anim_fade_out;
        private int mItemHeight = 0;
        private int mNumColumns = 0;


        public ImageAdapter(Context context) {
            this.context = context;

            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT,
                    GridLayout.LayoutParams.MATCH_PARENT);
            anim_fade_in = AnimationUtils.loadAnimation(
                    context, R.anim.animation_fade_in);
            anim_fade_out = AnimationUtils.loadAnimation(
                    context, R.anim.animation_fade_out);
        }

        public void add(HashMap<String,String> data) {
            list_data.add(data);
            Log.e("add","add");
            notifyDataSetChanged();
        }

        public void remove(int pos) {
//        image_data.remove(pos);
            list_data.get(pos);
            notifyDataSetChanged();
        }

        public void clear() {
            list_data.clear();
        }

        public int getCount() {
            Log.e("getCount",""+list_data.size());
            return list_data.size();
        }

        // set numcols
        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }

        public int getNumColumns() {
            return mNumColumns;
        }

        // set photo item height
        public void setItemHeight(int height) {
            if (height == mItemHeight) {
                return;
            }
            mItemHeight = height;
            mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, mItemHeight);
            notifyDataSetChanged();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View view, ViewGroup parent) {
            Log.e("getView","getView");
            /*if (view == null)
                view = mInflater.inflate(R.layout.photo_item, null);
*/

            view = inflater.inflate(R.layout.photo_item, parent, false);
            ImageView imagemain = (ImageView) view.findViewById(R.id.cover);
            TextView text = (TextView) view.findViewById(R.id.txt_imagetitle);
            final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

            imagemain.setLayoutParams(mImageViewLayoutParams);

            if (imagemain.getLayoutParams().height != mItemHeight) {
                imagemain.setLayoutParams(mImageViewLayoutParams);
            }
            final String img_url = context.getString(R.string.image_path)+list_data.get(position).get("gallary_image");
            Log.e("img_url",""+img_url);

           // final ImageView img_background = (ImageView) view.findViewById(R.id.cover);
//            TextView txt_menu = (TextView) view.findViewById(R.id.group_name);

           // Glide.with(context.getApplicationContext()).load(img_url).into(img_background);

            Glide.with(context.getApplicationContext()).load(img_url).dontAnimate().placeholder(R.drawable.image_placeholder)
                    .into(new GlideDrawableImageViewTarget(imagemain) {
                        @Override
                        public void onResourceReady(GlideDrawable arg0,
                                                    GlideAnimation<? super GlideDrawable> arg1) {
                            // TODO Auto-generated method stub
                            super.onResourceReady(arg0, arg1);
                            progressBar.setVisibility(View.GONE);
                        }
                    });
            imagemain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    openDilog(img_url);
                }
            });
           /* img_background.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    final Dialog view = new Dialog(GalleryPhotoView.this);
                    view.getWindow().requestFeature(1);
                    view.setContentView(R.layout.view_gallery_images);
                    view.setCancelable(false);
                    view.setCanceledOnTouchOutside(true);
                    ImageView profilepictureview = (ImageView) view
                            .findViewById(R.id.member_picture);
                    try {
                        //  profilepictureview.setImageResource(imagepath);
                        Glide.with(context.getApplicationContext()).load(img_url).into(profilepictureview);
                        //Glide.with(context.getApplicationContext()).load(img_url).into(img_background);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    Button b = (Button) view.findViewById(R.id.button1);
                    b.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            view.dismiss();
                        }
                    });
                    view.show();
                }
            });*/

            return view;
        }

        private void openDilog(String imagepath) {
            final Dialog dialogView = new Dialog(context, R.style.DialogTheme);
            dialogView.getWindow().requestFeature(1);
            dialogView.setContentView(R.layout.view_gallery_images);
            dialogView.setCancelable(false);
            dialogView.setCanceledOnTouchOutside(true);
            ImageView profilepictureview = (ImageView) dialogView.findViewById(R.id.member_picture);
            final ProgressBar progressBar = (ProgressBar) dialogView.findViewById(R.id.progressbar);
            final TextView txt_description = (TextView) dialogView.findViewById(R.id.txt_description);
            final ImageView img_back = (ImageView) dialogView.findViewById(R.id.img_back);

            img_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogView.dismiss();
                }
            });
         /*   txt_description.setText(description);*/
            try {
                //  profilepictureview.setImageResource(imagepath);
                Glide.with(context.getApplicationContext()).load(imagepath).dontAnimate().placeholder(R.drawable.image_placeholder)
                        .into(new GlideDrawableImageViewTarget(profilepictureview) {
                            @Override
                            public void onResourceReady(GlideDrawable arg0,
                                                        GlideAnimation<? super GlideDrawable> arg1) {
                                // TODO Auto-generated method stub
                                super.onResourceReady(arg0, arg1);
                                progressBar.setVisibility(View.GONE);
                            }
                        });


            } catch (Exception exception) {
                exception.printStackTrace();
            }
            profilepictureview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (txt_description.getVisibility() == View.VISIBLE) {
//                    txt_description.setAnimation(anim_fade_out);
                        txt_description.setVisibility(View.GONE);
                    } else {
//                    txt_description.setAnimation(anim_fade_in);
                        txt_description.setVisibility(View.VISIBLE);
                    }
                }
            });



            dialogView.show();
        }
    }


}
