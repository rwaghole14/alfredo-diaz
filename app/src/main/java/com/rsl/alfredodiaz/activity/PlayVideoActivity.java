package com.rsl.alfredodiaz.activity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.rsl.alfredodiaz.R;

public class PlayVideoActivity extends AppCompatActivity {

    VideoView videoView;
    String videopath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        videoView = (VideoView) findViewById(R.id.videoView);
        videopath = getIntent().getStringExtra("video");
        PlayVideo();

    }

    private void PlayVideo() {
        Uri uri = Uri.parse(videopath);
        videoView.setVideoURI(uri);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                MediaController mediaController = new
                        MediaController(PlayVideoActivity.this);
                videoView.requestFocus();
                videoView.setMediaController(null);
                mp.start();
                if (mp.isPlaying()) {
                    //mediaController.show();
                    progressBar.setVisibility(View.GONE);
                    mediaController.setAnchorView(videoView);
                    videoView.setMediaController(mediaController);
                }
            }
        });
    }

}
