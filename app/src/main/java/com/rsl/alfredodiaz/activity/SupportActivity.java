package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SupportActivity extends AppCompatActivity {

    ConnectionDetector internet;
    EditText edt_name, edt_mobile;
    Button btn_submit;
    TextView txt_company_address;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
        );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        sp = getSharedPreferences("sanjay_wable", MODE_PRIVATE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_support));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edt_name = (EditText) findViewById(R.id.edt_name);
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);
        btn_submit = (Button) findViewById(R.id.btn_submit);

        txt_company_address = (TextView) findViewById(R.id.txt_company_address);
        txt_company_address.setText(Html.fromHtml(getString(R.string.nice_html)));
        txt_company_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = getString(R.string.str_company_url);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        internet = new ConnectionDetector(SupportActivity.this);
        edt_name.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_name.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        edt_mobile.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                String str = s.toString();
                if (str.length() > 0 && str.startsWith(" ")) {
                    edt_mobile.setText("");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                String phoneText = edt_mobile.getText().toString().trim();
                if (!internet.isConnectingToInternet()) {
                    internet.showAlertDialog(SupportActivity.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                } else if (Is_Valid_first_Name(edt_name)) {
                    if (!phoneText.equals("")) {
                        if ((phoneText.length() == 10)) {
                            if (edt_mobile.getText().toString().trim().equals("0000000000") || edt_mobile.getText().toString().trim().equals("00000000000") || edt_mobile.getText().toString().trim().equals("000000000000") || edt_mobile.getText().toString().trim().equals("0000000000000") || edt_mobile.getText().toString().trim().equals("00000000000000") || edt_mobile.getText().toString().trim().equals("000000000000000")) {
                                edt_mobile.setError(getResources().getString(R.string.enter_valid_mobile));
                                edt_mobile.requestFocus();
                            } else {
                                support(edt_name.getText().toString().trim(), edt_mobile.getText().toString().trim());
                            }
                        } else {
                            edt_mobile.setError(getResources().getString(R.string.enter_digits));
                            edt_mobile.requestFocus();
                        }
                    } else {
                        edt_mobile.setError(getResources().getString(R.string.enter_mobile));
                        edt_mobile.requestFocus();
                    }
                }
            }//onClick
        });
    }//onCreate

    private void support(final String name, final String mobile) {
        final ProgressDialog dialog = new ProgressDialog(SupportActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_registering));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(SupportActivity.this);
        String requestURL = getString(R.string.web_path) + "support.php";
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject jObj;
                        if (response != null) {
                            Log.e("complaint_registration", response);
                            if (!response.startsWith("null")) {
                                try {
                                    jObj = new JSONObject(response);
                                    if (jObj.length() > 0) {
                                        if (jObj.getString("result").equals("success")) {
                                            if (dialog.isShowing()) {
                                                dialog.dismiss();
                                            }
                                            Toast.makeText(SupportActivity.this, getResources().getString(R.string.str_support_message), Toast.LENGTH_SHORT).show();
                                            finish();
                                        } else if (jObj.getString("result").equals("failed")) {
                                            if (dialog.isShowing())
                                                dialog.dismiss();
                                            alertDialog(jObj.getString("msg"));
//                                            Toast.makeText(SupportActivity.this, getResources().getString(R.string.str_support_failed), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (dialog.isShowing())
                                        dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (dialog.isShowing())
                                    dialog.dismiss();
                            }
                        } else {
                            if (dialog.isShowing())
                                dialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("language", sp.getString("my_lang", "english"));
                params.put("mobile_number", mobile);
                params.put("device_token", "" + FirebaseInstanceId.getInstance().getToken());
                Log.e("params_complaint", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void alertDialog(String errer) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                SupportActivity.this);
        alertDialog.setMessage(errer);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(getString(R.string.str_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                });
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public Boolean Is_Valid_first_Name(EditText edt)
            throws NumberFormatException {
        if (edt.getText().toString().trim().length() <= 0) {
            edt.setError(getResources().getString(R.string.enter_name));
            edt.requestFocus();
        } else if (edt.getText().toString().trim().length() <= 1) {
            edt.setError(getResources().getString(R.string.enter_more_char));
            edt.requestFocus();

        }
//        else if (!edt.getText().toString().trim().matches("[a-zA-Z ]+")) {
//            edt.setError(getResources().getString(R.string.accept_alphabet_first));
//            edt.requestFocus();
//
//        }

        else {

            return true;
        }
        return false;
    }//Is_Valid_first_Name

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
