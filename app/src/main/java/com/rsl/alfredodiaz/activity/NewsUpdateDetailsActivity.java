package com.rsl.alfredodiaz.activity;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsUpdateDetailsActivity extends AppCompatActivity {
    @BindView(R.id.txt_Title)
    TextView mTitleTextView;

    @BindView(R.id.horizontal_line)
    View mLine;

    //    ViewPager view_pager;
    private SliderLayout mDemoSlider;
    ImageView img_slider;
    //    HomeViewPagerAdapter viewpager_adapter;
    ArrayList<HashMap<String, String>> list_data = null;
    TextView txt_descr;

    private String mTitle;
    private String mNewsID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_update_details);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
        mLine.setVisibility(View.INVISIBLE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        String tag = getIntent().getStringExtra("type");
        if (tag.toLowerCase().contains("events")) {
            mToolTitle.setText(getIntent().getStringExtra("title"));
        } else {
            mToolTitle.setText(getString(R.string.str_news_update));
        }
        if (getIntent() != null) {
            mTitle = getIntent().getStringExtra("title");
            mNewsID = getIntent().getStringExtra("id");
        }
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDemoSlider = (SliderLayout) findViewById(R.id.view_pager_news_update);
        txt_descr = (TextView) findViewById(R.id.txt_descr);
        img_slider = (ImageView) findViewById(R.id.img_slider);

        list_data = new ArrayList<HashMap<String, String>>();
        mTitleTextView.setText(mTitle);
        txt_descr.setText(getIntent().getStringExtra("description"));
        if (getIntent().getStringExtra("type").equals("events")) {
            getEvent();
        } else {
            getNews();
        }
//        if (Constants.getImage_data().size() <= 0) {
//            textSliderView = new TextSliderView(this);
//            textSliderView
//                    .image(R.drawable.uttam4)
//                    .setScaleType(BaseSliderView.ScaleType.Fit);
//            mDemoSlider.addSlider(textSliderView);
//        } else {
//            for (int i = 0; i < Constants.getImage_data().size(); i++) {
//                // initialize a SliderLayout
//                textSliderView = new TextSliderView(this);
//                String url = getString(R.string.image_path) + Constants.getImage_data().get(i).get("image");
//                textSliderView
//                        .image(url)
//                        .setScaleType(BaseSliderView.ScaleType.Fit);
//                mDemoSlider.addSlider(textSliderView);
//            }
//        }
//        if (Constants.getImage_data().size() == 1) {
//            String url = getString(R.string.image_path) + Constants.getImage_data().get(0).get("image");
//            img_slider.setVisibility(View.VISIBLE);
//            Glide.with(NewsUpdateDetailsActivity.this.getApplicationContext()).load(url).placeholder(R.drawable.image_placeholder).into(img_slider);
//            mDemoSlider.setVisibility(View.GONE);
//            mDemoSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
//            mDemoSlider.stopAutoCycle();
//            mDemoSlider.setEnabled(false);
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    private void getEvent() {
//        final ProgressDialog dialog = new ProgressDialog(NewsUpdateDetailsActivity.this);
//        dialog.setCancelable(false);
//        dialog.setMessage(getString(R.string.str_loading));
//        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(NewsUpdateDetailsActivity.this);

        String requestURL = getString(R.string.web_path) + "events.php" + "?language=english";
        Log.e("check web_path_news", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                JSONObject jobj;
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            Log.v("response", "" + result);
                            jobj = new JSONObject(result);
                            if (jobj.getString("result").equals("success")) {
                                list_data.clear();
//                                if (dialog.isShowing()) {
//                                    dialog.dismiss();
//                                }
                                if (jobj.getJSONArray("data").length() > 0) {
                                    list_data.clear();
                                    ArrayList<String> mImages = new ArrayList<>();
                                    for (int a = 0; a < jobj.getJSONArray("data").length(); a++) {

                                        JSONObject jobj_Data = jobj.getJSONArray("data").getJSONObject(a);
                                        String id = jobj_Data.getString("event_id");
                                        Log.e("event", "eventId: "+ id);
                                        if (id.equals(mNewsID)) {
                                            Log.e("event", "eventId: "+ "inside it!");
                                            JSONArray mImageArray = jobj_Data.getJSONArray("images");
                                            for (int i = 0; i < mImageArray.length(); i++) {
                                                JSONObject mImageObject = mImageArray.getJSONObject(i);
                                                String mImageUrl = mImageObject.getString("image");
                                                Log.e("mImageUrl","+++"+mImageUrl);
                                                mImages.add(mImageUrl);
                                            }
                                        }
                                    }
                                    displayImage(mImages);
                                    mLine.setVisibility(View.VISIBLE);
                                }

                            } else {
//                                if (dialog.isShowing()) {
//                                    dialog.dismiss();
//                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            if (dialog.isShowing())
//                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
//                        dialog.dismiss();
                    }
                } else {
//                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("language", sp.getString("my_lang", "english"));
                Log.d("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void getNews() {
//        final ProgressDialog dialog = new ProgressDialog(NewsUpdateDetailsActivity.this);
//        dialog.setCancelable(false);
//        dialog.setMessage(getString(R.string.str_loading));
//        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(NewsUpdateDetailsActivity.this);

        String requestURL = getString(R.string.web_path) + "newsupdate.php" + "?language=english";
        Log.e("check web_path_news", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, requestURL.replaceAll(" ", "%20"), new Response.Listener<String>() {
            @Override
            public void onResponse(String result) {
                JSONObject jobj;
                if (result != null) {
                    if (!result.startsWith("null")) {
                        try {
                            Log.v("response", "" + result);
                            jobj = new JSONObject(result);
                            if (jobj.getString("result").equals("success")) {
                                list_data.clear();
//                                if (dialog.isShowing()) {
//                                    dialog.dismiss();
//                                }
                                if (jobj.getJSONArray("data").length() > 0) {
                                    list_data.clear();
                                    ArrayList<String> mImages = new ArrayList<>();
                                    for (int a = 0; a < jobj.getJSONArray("data").length(); a++) {

                                        JSONObject jobj_Data = jobj.getJSONArray("data").getJSONObject(a);
                                        String id = jobj_Data.getString("news_id");
                                        Log.e("newsUpdate", "newsId: "+ id);
                                        if (id.equals(mNewsID)) {
                                            Log.e("newsUpdate", "newsId: "+ "inside it!");
                                            JSONArray mImageArray = jobj_Data.getJSONArray("images");
                                            for (int i = 0; i < mImageArray.length(); i++) {
                                                JSONObject mImageObject = mImageArray.getJSONObject(i);
                                                String mImageUrl = mImageObject.getString("image");
                                                mImages.add(mImageUrl);
                                            }
                                        }
                                    }
                                    displayImage(mImages);
                                    mLine.setVisibility(View.VISIBLE);
                                }

                            } else {
//                                if (dialog.isShowing()) {
//                                    dialog.dismiss();
//                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
//                            if (dialog.isShowing())
//                                dialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
                        }

                    } else {
//                        dialog.dismiss();
                    }
                } else {
//                    dialog.dismiss();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                if (dialog.isShowing())
//                    dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Something went wrong plese try again!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
//                params.put("language", sp.getString("my_lang", "english"));
                Log.d("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(stringRequest);
    }

    private void displayImage(ArrayList<String> mImages) {
        Log.e("newsUpdate", "Imges: "+ mImages.size());
        if (mImages.size() == 1){
            img_slider.setVisibility(View.VISIBLE);
            Glide.with(this).load(mImages.get(0)).into(img_slider);
        }else {
            for (int i = 0; i < mImages.size(); i++) {
                TextSliderView textSliderView = new TextSliderView(NewsUpdateDetailsActivity.this);
                // initialize a SliderLayout
                textSliderView
                        .image(mImages.get(i))
                        .setScaleType(BaseSliderView.ScaleType.Fit);
                mDemoSlider.addSlider(textSliderView);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
