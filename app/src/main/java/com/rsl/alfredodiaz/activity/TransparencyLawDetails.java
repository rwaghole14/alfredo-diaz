package com.rsl.alfredodiaz.activity;

import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.adapter.ExpandableListAdapter;
import com.rsl.alfredodiaz.adapter.TraExpandableListAdapter;
import com.rsl.alfredodiaz.model.ImportantNumbers;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransparencyLawDetails extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;


    @BindView(R.id.expand_imp_numbers_list)
    ExpandableListView mExpandableServiceList;

    private String mMainTitle;
    private RequestQueue mQueue;
    private ConnectionDetector mConnection;
    private ArrayList<ImportantNumbers> mImportantNoList = new ArrayList<>();
    private ArrayList<ImportantNumbers> mNumberHeaderList = new ArrayList<>();

    TraExpandableListAdapter mExpandableListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transparency_law_details);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        ButterKnife.bind(this);

        if (getIntent() != null) {
            mMainTitle = getIntent().getStringExtra("title");
        }
        initView();
        mExpandableListAdapter = new TraExpandableListAdapter(this, mImportantNoList, 1);
        // setting list adapter
        mExpandableServiceList.setAdapter(mExpandableListAdapter);

        mConnection = new ConnectionDetector(this);
        if (mConnection.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(TransparencyLawDetails.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
                    getImpNoDetails();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();

        } else {
            mConnection.showAlertDialog(TransparencyLawDetails.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mMainTitle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getImpNoDetails() {
        final String requestURL = getString(R.string.web_path) + "get_TransLaw.php" + "?language=english";
        Log.e("requestURL"," "+requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){
                                        Log.e("result",""+result);
                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        JSONArray mDataObjectArray = baseJSONObject.getJSONArray("data");
                                        if (mDataObjectArray.length() > 0){
                                            for (int i = 0; i < mDataObjectArray.length(); i++){
                                                JSONObject mDataJSONObject = mDataObjectArray.getJSONObject(i);
                                                String title = mDataJSONObject.getString("transLaw_type");
                                                if (title.equals(mMainTitle)){
                                                    String imageUrl = null;
                                                    JSONArray mNumberDataArray = mDataJSONObject.getJSONArray("transLaw_data");
                                                    for (int k = 0; k < mNumberDataArray.length(); k++){
                                                        JSONObject mNumberObject = mNumberDataArray.getJSONObject(k);

                                                        String mSubTitle = mNumberObject.getString("administrative_unit");
                                                        String mContactNo = mNumberObject.getString("phone_no");
                                                        String mAddress = mNumberObject.getString("address");
                                                        imageUrl = mNumberObject.getString("director_name");
                                                        mNumberHeaderList.add(new ImportantNumbers(mSubTitle, mAddress, mContactNo,imageUrl));
                                                    }
                                                    prepareList(mNumberHeaderList);
                                                }
                                            }
                                        }

                                    }else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                }

                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        mQueue.add(stringRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void prepareList(ArrayList<ImportantNumbers> mHeaderList){


        mImportantNoList.clear();

        for (int i = 0; i < mHeaderList.size(); i++) {
            ImportantNumbers mNumberObject = new ImportantNumbers();
            mNumberObject.setmImpNoSubTitle(mHeaderList.get(i).getmImpNoSubTitle());

            ArrayList<HashMap<String, String>> mChildData = new ArrayList<>();
            HashMap<String, String> mChild = new HashMap<>();
            mChild.put("phone_no", mHeaderList.get(i).getmImpNoContact());
            mChild.put("address", mHeaderList.get(i).getmImpNoAddress());
            mChild.put("director_name", mHeaderList.get(i).getmImpNoImage());
            mChildData.add(mChild);

            mNumberObject.setChildItems(mChildData);
            mImportantNoList.add(mNumberObject);
        }
        mExpandableListAdapter.notifyDataSetChanged();
    }
}
