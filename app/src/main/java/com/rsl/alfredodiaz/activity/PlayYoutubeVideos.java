package com.rsl.alfredodiaz.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.Config;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlayYoutubeVideos extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{


    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youtube_view;
    String youtubelink="";
    ListView list_videos;
    VideoAdapter videoAdapter;

    ArrayList<HashMap<String, String>> list_data =new ArrayList<HashMap<String, String>>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_youtube_videos);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        youtube_view = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youtube_view.initialize(Config.YOUTUBE_API_KEY, this);

        String s= getIntent().getStringExtra("video_link");
        Log.e("playlink","---"+s);

        String[] parts = s.split("https://youtu.be/");
        String part1 = parts[0];
        String part2 = parts[1];
        Log.e("part2","---"+part2);
        youtubelink=part2;
        getVideos();
        list_videos= (ListView) findViewById(R.id.list_videos);
        // list_data=new ArrayList<HashMap<String, String>>();
        videoAdapter=new VideoAdapter(PlayYoutubeVideos.this);
        list_videos.setAdapter(videoAdapter);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo(youtubelink); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(Config.YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youtube_view;
    }

    public class VideoAdapter extends BaseAdapter {


        private LayoutInflater inflater = null;
        Context context;

        public VideoAdapter(Context context) {
            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public  void add(HashMap<String, String> hash){

            list_data.add(hash);
            notifyDataSetChanged();
        }
        @Override
        public int getCount() {
            // Log.e("size",""+list_data.size());
            return list_data.size();

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


            // LayoutInflater inflater = getLayoutInflater();
            convertView = inflater.inflate(R.layout.row_videos_list, parent, false);
            TextView txt_videos_name = (TextView) convertView.findViewById(R.id.txt_videos_name);

            txt_videos_name.setText(list_data.get(position).get("video_title"));
            txt_videos_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(PlayYoutubeVideos.this, PlayYoutubeVideos.class);
                    Log.e("video_title","="+list_data.get(position).get("video_title"));
                    Log.e("video_link","="+list_data.get(position).get("video"));
                    i.putExtra("video_name",list_data.get(position).get("video_title"));
                    i.putExtra("video_link",list_data.get(position).get("video"));
                    startActivity(i);
                    finish();
                }
            });

            return convertView;
        }
    }

    private void getVideos() {

        final ProgressDialog dialog = new ProgressDialog(PlayYoutubeVideos.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        //swipetorefresh.setRefreshing(true);
        final RequestQueue queue = Volley.newRequestQueue(PlayYoutubeVideos.this);
        String requestURL = getString(R.string.web_path) + "media.php";

        Log.e("requestURL","media"+ requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", "media"+response);
                        try {

                            dialog.dismiss();
                            list_data.clear();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                if (jObj_result.getJSONArray("videos").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "videos").length(); i++) {

                                        JSONObject jobj_Data = jObj_result.getJSONArray("videos").getJSONObject(i);

                                        HashMap<String, String> offers = new HashMap<String, String>();
                                        offers.put("video_title",jobj_Data.getString("video_title"));
                                        offers.put("video",jobj_Data.getString("video"));
                                        videoAdapter.add(offers);
                                    }

                                    //Constants.setImage_data(list_data);
                                   // Constants.setImage_data(list_data);

                                }
                                // swipetorefresh.setRefreshing(false);

                            } else if(jObj_result.getString("result").equals(
                                    "failed")){
                                Toast.makeText(PlayYoutubeVideos.this.getApplicationContext(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }




                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                                //swipetorefresh.setRefreshing(false);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("type", "video");
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }
}
