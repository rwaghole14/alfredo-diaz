package com.rsl.alfredodiaz.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.GameEntity;

import java.util.ArrayList;

public class ViewAllActivity extends AppCompatActivity {

    GridView list_viewAll;

    ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        initview();

        imageAdapter = new ImageAdapter(this);

        addListdata();
    }

    private void addListdata() {
        imageAdapter.add(new GameEntity(R.drawable.browser, R.string.str_home));
        imageAdapter.add(new GameEntity(R.drawable.emergency_service, R.string.str_emergency_service));
        imageAdapter.add(new GameEntity(R.drawable.important_number, R.string.str_imp_numbers));
        imageAdapter.add(new GameEntity(R.drawable.eservices, R.string.str_e_services));
        imageAdapter.add(new GameEntity(R.drawable.news, R.string.str_news_update));
        imageAdapter.add(new GameEntity(R.drawable.event, R.string.str_events));
        imageAdapter.add(new GameEntity(R.drawable.voterlist, R.string.str_voter_list));
        imageAdapter.add(new GameEntity(R.drawable.complaints, R.string.str_complaints));
        imageAdapter.add(new GameEntity(R.drawable.contactus, R.string.str_contact_us));
        imageAdapter.add(new GameEntity(R.drawable.online_froms, R.string.str_online_forms));
        imageAdapter.add(new GameEntity(R.drawable.imp_web, R.string.str_imp_webs));
        imageAdapter.add(new GameEntity(R.drawable.schemes, R.string.str_pcmc_scheme));
        imageAdapter.add(new GameEntity(R.drawable.job_opportunites, R.string.str_jobs));
        imageAdapter.add(new GameEntity(R.drawable.gallery, R.string.str_media));
        imageAdapter.add(new GameEntity(R.drawable.browser,R.string.string_tender_header));
        imageAdapter.add(new GameEntity(R.drawable.gavel,R.string.str_transparency));
        list_viewAll.setAdapter(imageAdapter);
    }

    private void initview() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.app_name));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        list_viewAll=(GridView) findViewById(R.id.list_viewAll);
    }

    public class ImageAdapter extends BaseAdapter {

        private ArrayList<GameEntity> mData = new ArrayList<>(0);
        private Context mContext;

        public ImageAdapter(Context context) {
            this.mContext = context;
        }

/*	public void setData(ArrayList<GameEntity> data) {
		this.mData = data;
	}*/

        public void add(GameEntity ge){
            mData.add(ge);
            //notifyDataSetChanged();
        }
        public void clear(){
            mData.clear();
            //notifyDataSetChanged();
        }
        @Override
        public int getCount() {

            return mData.size();
        }

        @Override
        public Object getItem(int pos) {
            return mData.get(pos);
        }

        @Override
        public long getItemId(int pos) {
            return pos;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder=null;
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.row_viewwork, null);
                viewHolder = new ViewHolder();
                viewHolder.text = (TextView) rowView.findViewById(R.id.txt_menuname);
                viewHolder.image = (ImageView) rowView.findViewById(R.id.img_allimage);
                //	viewHolder.img_circle = (CircleImageView) rowView.findViewById(R.id.img_circle);
                Typeface face = Typeface.createFromAsset(mContext.getAssets(),
                        "fonts/OpenSans-Light.ttf");
                viewHolder.text.setTypeface(face);

                rowView.setTag(viewHolder);


            }else {
                viewHolder = (ViewHolder) rowView.getTag();
            }



            viewHolder.image.setImageResource(mData.get(position).imageResId);
            String name= mContext.getResources().getString(mData.get(position).titleResId);
            Log.e("check","----"+name);

            viewHolder.text.setText(mContext.getResources().getString(mData.get(position).titleResId));
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (position == 0) {
                        Intent i = new Intent(mContext, IntroductionActivity.class);
                        i.putExtra("TAG", "Introduction");
                        startActivity(i);
                    } else if (position == 1) {
                        Intent i = new Intent(mContext, EmergencyServicesActivity.class);
                        startActivity(i);
                    } else if (position == 2) {
                        Intent i = new Intent(mContext, ImportantNumbersActivity.class);
                        startActivity(i);
                    } else if (position == 3) {
                        Intent i = new Intent(mContext, EServices.class);
                        startActivity(i);
                    } else if (position == 4) {
                        Intent i = new Intent(mContext, NewsAndUpdatesActivity.class);
                        startActivity(i);
                    } else if (position == 5) {
                        Intent i = new Intent(mContext, EventsActivity.class);
                        startActivity(i);
                    } else if (position == 6) {
                        Intent i = new Intent(mContext, VoterListActivity.class);
                        i.putExtra("link", "http://103.23.150.139/marathi/");
                        i.putExtra("title", "" + getString(R.string.str_voter_list));
                        startActivity(i);
                    } else if (position == 7) {
                        Intent i = new Intent(mContext, ComplaintActivity.class);
                        startActivity(i);
                    } else if (position == 8) {
                        Intent i = new Intent(mContext, ContactUsActivity.class);
                        startActivity(i);
                    } else if (position == 9) {
                        Intent i = new Intent(mContext, UsefulFormsActivity.class);
                        startActivity(i);
                    } else if (position == 10) {
                        Intent i = new Intent(mContext, ImpLinksActivity.class);
                        startActivity(i);
                    } else if (position == 11) {
                        Intent i = new Intent(mContext, PcmcSchemesActivity.class);
                        startActivity(i);
                    } else if (position == 12) {
                        Intent i = new Intent(mContext, JobOpprtunityActivity.class);
                        startActivity(i);
                    } else if (position == 13){
                        Intent i = new Intent(mContext, MediaActivity.class);
                        startActivity(i);
                    }else if (position == 14){
                        Intent i = new Intent(mContext, TenderActivity.class);
                        startActivity(i);
                    }else if (position == 15){
                        Intent i = new Intent(mContext, TransparencyLaw.class);
                        startActivity(i);
                    }
                }
            });
            return rowView;
        }

       class ViewHolder {
            public TextView text;
            public ImageView image;
            // public CircleImageView img_circle;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
