package com.rsl.alfredodiaz.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.rsl.alfredodiaz.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class AllWorkActivity extends AppCompatActivity implements View.OnClickListener{

    CircleImageView btn_pastwork,btn_futurework;
    //RippleBackground rippleBackground;
    TextView txt_pastwork,txt_futurework;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_work);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        initview();
    }

    private void initview() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        mToolTitle.setText(getString(R.string.app_name));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btn_pastwork=(CircleImageView) findViewById(R.id.btn_pastwork);
        btn_futurework=(CircleImageView)findViewById(R.id.btn_futurework);
        txt_pastwork=(TextView)findViewById(R.id.txt_pastwork);
        txt_futurework=(TextView)findViewById(R.id.txt_futurework);
       // rippleBackground=(RippleBackground)findViewById(R.id.content);
        btn_pastwork.setOnClickListener(this);
        btn_futurework.setOnClickListener(this);

        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/OpenSans-Light.ttf");
        txt_pastwork.setTypeface(face);
        txt_futurework.setTypeface(face);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_pastwork: {
               /* rippleBackground.startRippleAnimation();
                Thread background = new Thread() {
                    public void run() {
                        try {
                            // Thread will sleep for 5 seconds
                            sleep(1 * 1000);
                            // After 5 seconds redirect to another intent

                            Intent i=new Intent(AllWorkActivity.this,PastWorkActivity.class);
                            startActivity(i);

                            // Remove activity
                            rippleBackground.stopRippleAnimation();

                        } catch (Exception e) {

                        }
                    }
                };
                background.start();*/
                Intent i=new Intent(AllWorkActivity.this, PastWorkActivity.class);
                startActivity(i);

                break;
            }
            case R.id.btn_futurework: {
 /*               Thread background = new Thread() {
                    public void run() {
                        try {
                            // Thread will sleep for 5 seconds
                            sleep(1 * 1000);
                            // After 5 seconds redirect to another intent
                            Intent i=new Intent(AllWorkActivity.this,FutureWorkActivity.class);
                            startActivity(i);
                            // Remove activity

                        } catch (Exception e) {

                        }
                    }
                };
                background.start();*/
                Intent i=new Intent(AllWorkActivity.this, FutureWorkActivity.class);
                startActivity(i);

                break;
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
