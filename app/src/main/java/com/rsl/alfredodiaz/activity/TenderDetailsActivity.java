package com.rsl.alfredodiaz.activity;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TenderDetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.txt_descr)
    TextView mDescriptionTextView;

    @BindView(R.id.text_view_description_label)
    TextView mDescriptionLabelTextView;

    @BindView(R.id.text_view_title)
    TextView mTitleTextView;

    @BindView(R.id.text_view_title_label)
    TextView mTitleLabelTextView;

    @BindView(R.id.img_slider)
    ImageView mImageSlider;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    private String mTitle;
    private RequestQueue mQueue;
    private ConnectionDetector mConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tender_details);
        ButterKnife.bind(this);
        hideViews();
        if (getIntent() != null){
            mTitle = getIntent().getStringExtra("title");

        }
        initView();
        mProgressbar2.setVisibility(View.INVISIBLE);
        mConnection = new ConnectionDetector(TenderDetailsActivity.this);

        if (mConnection.isConnectingToInternet()){
            mQueue = Volley.newRequestQueue(this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
                    getTenderDetails();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();
        }else {
            mConnection.showAlertDialog(TenderDetailsActivity.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }

    }

    private void hideViews(){
        mTitleLabelTextView.setVisibility(View.INVISIBLE);
        mDescriptionLabelTextView.setVisibility(View.INVISIBLE);
    }

    private void showViews(){
        mTitleLabelTextView.setVisibility(View.VISIBLE);
        mDescriptionLabelTextView.setVisibility(View.VISIBLE);
    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(mTitle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void getTenderDetails() {

        final String requestURL = getString(R.string.web_path) + "get_tender.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){

                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        showViews();
                                        JSONArray mDataObjectArray = baseJSONObject.getJSONArray("data");
                                        if (mDataObjectArray.length() > 0){

                                            for (int i = 0; i < mDataObjectArray.length(); i++){
                                                JSONObject mDataJSONObject = mDataObjectArray.getJSONObject(i);
                                                String title = mDataJSONObject.getString("title");
                                                if (title.equals(mTitle)) {
                                                    String description = mDataJSONObject.getString("description");
                                                    String imageUrl = mDataJSONObject.getString("images");
                                                    displayData(title, description, imageUrl);
                                                }
                                            }
                                        }

                                    }else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                }

                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        mQueue.add(stringRequest);
    }

    private void displayData(String title, String description, String imageUrl) {

        mProgressbar2.setVisibility(View.VISIBLE);
        mTitleTextView.setText(title);
        mDescriptionTextView.setText(description);
        Glide.with(this)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mImageSlider);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
