package com.rsl.alfredodiaz.activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;
import com.rsl.alfredodiaz.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TenderActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.progressbar_layout)
    LinearLayout mProgressbarLayout;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    @BindView(R.id.list_view_tender)
    ListView mListViewForTenders;

    @BindView(R.id.img_home_slider)
    ImageView mImageViewForSlider;

    private RequestQueue mQueue;
    private ArrayList<String> mItems;
    private TenderAdapter mTenderAdapter;
    private ConnectionDetector mConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tender);
        Constants.activity_tender = true;
        ButterKnife.bind(this);
        initView();
        mProgressbar2.setVisibility(View.INVISIBLE);

        mItems = new ArrayList<>();
        mTenderAdapter = new TenderAdapter();
        mConnection = new ConnectionDetector(this);

        if (mConnection.isConnectingToInternet()){
            mQueue = Volley.newRequestQueue(TenderActivity.this);
            new CountDownTimer(1000, 1000) {
                public void onFinish() {
                    getTenderDetails();
                }

                public void onTick(long millisUntilFinished) {
                    mProgressbarLayout.setVisibility(View.VISIBLE);
                }
            }.start();
        }

    }

    private void initView() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(getString(R.string.string_tender_header));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.activity_tender= false;
    }
    private void getTenderDetails() {

        final String requestURL = getString(R.string.web_path) + "get_tender.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        JSONObject baseJSONObject;
                        if (result != null){
                            if (!result.startsWith("null")){
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")){

                                        mProgressbarLayout.setVisibility(View.INVISIBLE);
                                        JSONArray mDataObjectArray = baseJSONObject.getJSONArray("data");
                                        if (mDataObjectArray.length() > 0){

                                            for (int i = 0; i < mDataObjectArray.length(); i++){
                                                JSONObject mDataJSONObject = mDataObjectArray.getJSONObject(i);
                                                String title = mDataJSONObject.getString("title");

                                                mTenderAdapter.add(title);
                                            }
                                            mListViewForTenders.setAdapter(mTenderAdapter);

                                            //Only for placeholder purpose
                                            String imageUrl = mDataObjectArray.getJSONObject(0).getString("images");
                                            displayImage(imageUrl);
                                        }

                                    }else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    mProgressbarLayout.setVisibility(View.INVISIBLE);
                                }

                            }else {
                                mProgressbarLayout.setVisibility(View.INVISIBLE);
                            }
                        }else {
                            mProgressbarLayout.setVisibility(View.INVISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                });
        mQueue.add(stringRequest);
    }

    private void displayImage(String imageUrl) {
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mImageViewForSlider);
    }

    private class TenderAdapter extends BaseAdapter{

        public void add(String data) {
            mItems.add(data);
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            View mRow = convertView;
            if (mRow == null){
                mRow = LayoutInflater
                        .from(TenderActivity.this)
                        .inflate(R.layout.row_political_data, null);
            }

            TextView mTextViewItem = (TextView) mRow.findViewById(R.id.txt_item);
            mTextViewItem.setText(mItems.get(position));

            mRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mTenderIntent = new Intent(TenderActivity.this, TenderDetailsActivity.class);
                    mTenderIntent.putExtra("title", mItems.get(position));
                    startActivity(mTenderIntent);
                }
            });

            return mRow;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void prepareTenderList(){

    }
}
