package com.rsl.alfredodiaz.activity;

import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WebViewEservice extends AppCompatActivity {

//    @BindView(R.id.toolbar)
//    Toolbar mToolbar;

    public static String[] MenuNameList;
    WebView webView;
    String url;
    private ProgressBar progress;
    ConnectionDetector internet;

    private String mTitle;
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_eservice);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        if (getIntent() != null) {
            mTitle = getIntent().getStringExtra("title");
        }

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setBuiltInZoomControls(true);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setMax(100);
        mToolTitle.setText(mTitle);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


//        int pos = getIntent().getIntExtra("key", 0);

//        if (pos == 0) {
//            url = "http://203.129.227.16:8080/pcmc/pcmc?wicket:bookmarkablePage=wicket-0:com.common.property.reports.RptMilkatKaracheBillnew";
//        } else if (pos == 1) {
//            url = "http://203.129.227.16:8080/pcmc/pcmc?wicket:bookmarkablePage=wicket-1:com.common.property.reports.RptWaterBill";
//        } else if (pos == 2) {
//            url = "https://www.pcmcindia.gov.in/marathi/schemes.php";
//        } else if (pos == 3) {
//            url = "https://wss.mahadiscom.in/wss/wss?uiActionName=getViewPayBill";
//        } else if (pos == 4) {
//            url = "http://www.lilapoonawallafoundation.com";
//        }

//        MenuNameList = getResources().getStringArray(R.array.e_services_headers);
//        mToolTitle.setText(MenuNameList[pos]);

        internet = new ConnectionDetector(WebViewEservice.this);
        if (internet.isConnectingToInternet()) {
            mQueue = Volley.newRequestQueue(WebViewEservice.this);
            getEServiceDetails();

        } else {
            Toast.makeText(WebViewEservice.this, "No internet connection", Toast.LENGTH_LONG).show();
            progress.setVisibility(View.GONE);
            webView.setVisibility(View.GONE);
        }


    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progress.setVisibility(View.GONE);
            progress.setProgress(100);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progress.setVisibility(View.VISIBLE);
            progress.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getEServiceDetails() {

        String requestURL = getString(R.string.web_path) + "get_eServices.php" + "?language=english";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, requestURL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        Log.d("WorkDetails", "got response");
                        JSONObject baseJSONObject;
                        if (result != null) {
                            if (!result.startsWith("null")) {
                                try {
                                    baseJSONObject = new JSONObject(result);
                                    if (baseJSONObject.getString("result").equals("success")) {

                                        JSONArray dataArray = baseJSONObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject dataObject = dataArray.getJSONObject(i);
                                                String title = dataObject.getString("title");
                                                if (title.equals(mTitle)) {
                                                    url = dataObject.getString("eServices_link");
                                                    Log.d("WorkDetails", "url1: " + url);
                                                    displayWebView(url);
                                                }
                                            }
//                                            mEServiceListView.setAdapter(adapter);
                                        }
                                    } else {

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                            }
                        } else {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(), "Something went wrong please try again!", Toast.LENGTH_SHORT).show();
            }
        });

        mQueue.add(stringRequest);
    }

    private void displayWebView(String url) {
        if (URLUtil.isValidUrl(url)) {
            Log.d("WorkDetails", "url: " + url);
            webView.setWebViewClient(new MyWebViewClient());
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(url);
        }
    }

}

