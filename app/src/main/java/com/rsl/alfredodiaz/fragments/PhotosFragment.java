package com.rsl.alfredodiaz.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.activity.GalleryPhotoView;
import com.rsl.alfredodiaz.model.Gallery;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2/4/2017.
 */

public class PhotosFragment extends Fragment {

    GridView gridview;
  //  GallaryImageAdapter imageAdapter;
   ImageAdapter imageAdapter;
    private int mPhotoSize, mPhotoSpacing;
  //  ArrayList<HashMap<String, String>> list_data = null;
   public static ArrayList<Gallery> list_data = null;
    public static ArrayList<HashMap<String, String>> list_gallery = null;
    ConnectionDetector internet;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //  return super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.photos_fragment, container, false);

        gridview= (GridView)rootView. findViewById(R.id.gridview);
       // list_data=new ArrayList<HashMap<String, String>>();
        list_data = new ArrayList<>();
        list_gallery = new ArrayList<HashMap<String, String>>();
        mPhotoSize = getResources().getDimensionPixelSize(R.dimen.photo_size);
        mPhotoSpacing = getResources().getDimensionPixelSize(R.dimen.photo_spacing);
        internet = new ConnectionDetector(getActivity());
        imageAdapter = new ImageAdapter(getActivity());
        // gridview.setAdapter(new ImageAdapter(getActivity(), MenuImages));
       // imageAdapter=new GallaryImageAdapter(getActivity());
       // gridview.setAdapter(imageAdapter);

        if (internet.isConnectingToInternet())
            getphotos();
        else {
            internet.showAlertDialog(getActivity(),
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        gridview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (imageAdapter.getNumColumns() == 0) {
                    final int numColumns = (int) Math.floor(gridview.getWidth() / (mPhotoSize + mPhotoSpacing));
                    if (numColumns > 0) {
                        final int columnWidth = (gridview.getWidth() / numColumns) - mPhotoSpacing;
                        imageAdapter.setNumColumns(numColumns);
                        imageAdapter.setItemHeight(columnWidth);
                    }
                }
            }
        });

        return  rootView;
    }


    private void getphotos() {

        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = getString(R.string.web_path) + "media.php";

        Log.e("requestURL", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("getphotos", response);
                        try {
                            list_data.clear();
                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                if (jObj_result.getJSONArray("data").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "data").length(); i++) {
                                        JSONObject jobj_Data = jObj_result.getJSONArray("data").getJSONObject(i);
                                        Gallery gallery = new Gallery();
                                        gallery.setTitle(jobj_Data.getString("image_title"));
                                        gallery.setDescription(jobj_Data.getString("description"));
                                        gallery.setDate(jobj_Data.getString("date"));

                                        JSONArray jsonArray = jobj_Data.getJSONArray("image");
                                        ArrayList<HashMap<String, String>> gallerydetails = new ArrayList<>();
                                        if (jsonArray.length() > 0) {
                                            for (int j = 0; j < jsonArray.length(); j++) {
                                                JSONObject job = jsonArray.getJSONObject(j);
                                                HashMap<String, String> data = new HashMap<>();
                                                data.put("gallary_image", job.getString("gallary_image"));
                                                //  String img=job.getString("image");
                                                //Log.e("image","="+img);
                                                gallerydetails.add(data);
                                            }

                                        }

                                      //  imageAdapter.add(offers);

                                        gallery.setGallery_images(gallerydetails);
                                        list_data.add(gallery);
                                        gridview.setAdapter(imageAdapter);
                                    }
                                }

                            } else if(jObj_result.getString("result").equals(
                                    "failed")){
                                Toast.makeText(getActivity(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("type", "image");
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    public class ImageAdapter extends BaseAdapter {


        private LayoutInflater inflater = null;
        Context context;
        private RelativeLayout.LayoutParams mImageViewLayoutParams;
        private Animation anim_fade_in, anim_fade_out;
        private int mItemHeight = 0;
        private int mNumColumns = 0;

        public ImageAdapter(Context context) {

            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT,
                    GridLayout.LayoutParams.MATCH_PARENT);
            anim_fade_in = AnimationUtils.loadAnimation(
                    context, R.anim.animation_fade_in);
            anim_fade_out = AnimationUtils.loadAnimation(
                    context, R.anim.animation_fade_out);
        }
        public void add(Gallery hash) {

            list_data.add(hash);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        // set numcols
        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }

        public int getNumColumns() {
            return mNumColumns;
        }

        // set photo item height
        public void setItemHeight(int height) {
            if (height == mItemHeight) {
                return;
            }
            mItemHeight = height;
            mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, mItemHeight);
            notifyDataSetChanged();
        }
        @Override
        public View getView(final int position, View list, ViewGroup parent) {


           // LayoutInflater inflater = getLayoutInflater();
            list = inflater.inflate(R.layout.photo_item, parent, false);
            ImageView imagemain = (ImageView) list.findViewById(R.id.cover);
            TextView text = (TextView) list.findViewById(R.id.txt_imagetitle);
            final ProgressBar progressBar = (ProgressBar) list.findViewById(R.id.progressbar);

            imagemain.setLayoutParams(mImageViewLayoutParams);

            if (imagemain.getLayoutParams().height != mItemHeight) {
                imagemain.setLayoutParams(mImageViewLayoutParams);
            }
            list_gallery = list_data.get(position).getGallery_images();
            Log.e("list_gallery--", "" + list_gallery.toString());
            Log.e("list_gallerysize", "" + list_gallery.size());
            String imagepath;

            if (list_data.get(position).getGallery_images() != null) {
                imagepath = context.getString(R.string.image_path)+list_data.get(position).getGallery_images().get(0).get("gallary_image");
                Log.e("imagepath","--"+imagepath);
                //Glide.with(getActivity()).load(imagepath).placeholder(R.drawable.app_logo_main).into(imagemain);

                Glide.with(context.getApplicationContext()).load(imagepath).dontAnimate().placeholder(R.drawable.image_placeholder)
                        .into(new GlideDrawableImageViewTarget(imagemain) {
                            @Override
                            public void onResourceReady(GlideDrawable arg0,
                                                        GlideAnimation<? super GlideDrawable> arg1) {
                                // TODO Auto-generated method stub
                                super.onResourceReady(arg0, arg1);
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            } /*else
                Glide.with(GalleryActivity.this).load(R.drawable.app_logo_main).placeholder(R.drawable.app_logo_main);*/
            text.setText(list_data.get(position).getTitle());

         /*   list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), GalleryPhotoView.class);
                    list_gallery = list_data.get(position).getGallery_images();
                    intent.putExtra("name", list_data.get(position).getTitle());
                    intent.putExtra("position", String.valueOf(position));
                    Log.e("position", "" + position);
                    startActivity(intent);

                }
            });*/
            imagemain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), GalleryPhotoView.class);
                    list_gallery = list_data.get(position).getGallery_images();
                    intent.putExtra("name", list_data.get(position).getTitle());
                    intent.putExtra("position", String.valueOf(position));
                    Log.e("position", "" + position);
                    startActivity(intent);
                }
            });



            //   Log.e("image",""+imagepath);


            return list;
        }
    }//ImageAdapter
}
