package com.rsl.alfredodiaz.fragments;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.adapter.CustomAdapterForList;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspirationFragment extends AppCompatActivity {

    SharedPreferences sp;
    ConnectionDetector internet;
    String name;
    String name1;


    @BindView(R.id.img_home_slider)
    ImageView mSliderImage;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

    @BindView(R.id.list_view_inspiration)
    ListView mInspirationListView;

    private CustomAdapterForList mCustomAdapter;

/*
    public InspirationFragment() {
        // Required empty public constructor
    }*/

    /*   @Override
       public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState) {
           // Inflate the layout for this fragment
           View rootView = inflater.inflate(R.layout.fragment_inspiration, container, false);
           internet = new ConnectionDetector(getActivity());
           sp = getActivity().getSharedPreferences("sanjay_wable", getActivity().MODE_PRIVATE);
           if (internet.isConnectingToInternet()) {
               getInspiration();
           } else {
               internet.showAlertDialog(getActivity(),
                       "No Internet Connection",
                       "You don't have internet connection.", false);
           }
           txt_guide = (TextView) rootView.findViewById(R.id.txt_guide);
           txt_guild_name = (TextView) rootView.findViewById(R.id.txt_guild_name);
           txt_motivation = (TextView) rootView.findViewById(R.id.txt_motivation);
           txt_motivation_name = (TextView) rootView.findViewById(R.id.txt_motivation_name);

           return rootView;
       }
   */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_inspiration);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
        mProgressbar2.setVisibility(View.INVISIBLE);
        mCustomAdapter = new CustomAdapterForList(InspirationFragment.this);
        mInspirationListView.setAdapter(mCustomAdapter);
        Toolbar mToolbar =  findViewById(R.id.toolbar);
        TextView mToolTitle =  mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_inspiration));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        internet = new ConnectionDetector(InspirationFragment.this);
        sp = getSharedPreferences("sanjay_wable",InspirationFragment.this.MODE_PRIVATE);
        if (internet.isConnectingToInternet()) {
            getInspiration();
        } else {
            internet.showAlertDialog(InspirationFragment.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }


    }

    private void getInspiration() {
        final ProgressDialog dialog = new ProgressDialog(InspirationFragment.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(InspirationFragment.this);
        String requestURL = getString(R.string.web_path) + "myinfo.php";
        Log.e("requestURL","myinfo"+ requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", ""+response);
                        try {

                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                String imageUrl = jObj_result.getString("image");
                                displayImage(imageUrl);

                                JSONArray mInspirationJsonArray = jObj_result.getJSONArray("Inspiration");
                                for (int i = 0; i < mInspirationJsonArray.length(); i++){
                                    JSONObject mInspirationObject = mInspirationJsonArray.getJSONObject(i);
                                    String mType = mInspirationObject.getString("type");
                                    String mInspirationName = mInspirationObject.getString("inspiration_name");

                                    HashMap<String,String> mHash = new HashMap<>();
                                    mHash.put("type", mType);
                                    mHash.put("inspiration", mInspirationName);
                                    mCustomAdapter.add(mHash);
                                }
                            } else if(jObj_result.getString("result").equals(
                                    "failed")){
                                Toast.makeText(getApplicationContext(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.e("params", "" + params);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void displayImage(String imageUrl) {
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mSliderImage);
    }

}
