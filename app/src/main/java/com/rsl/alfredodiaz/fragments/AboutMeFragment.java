package com.rsl.alfredodiaz.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AboutMeFragment extends AppCompatActivity {
    // TODO: Rename parameter arguments, choose names that match
    TextView txt_num1, txt_name, txt_address, txt_dob,
            txt_num2, txt_qualification, txt_hobbies, txt_occupation;
    SharedPreferences sp;
    ConnectionDetector internet;

    @BindView(R.id.img_home_slider)
    ImageView mSliderImage;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

  /*  public AboutMeFragment() {
        // Required empty public constructor
    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_about_me);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
        mProgressbar2.setVisibility(View.INVISIBLE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_about_me));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



    /*    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about_me, container, false);
        internet = new ConnectionDetector(getActivity());
        sp = getActivity().getSharedPreferences("sanjay_wable", getActivity().MODE_PRIVATE);
        if (internet.isConnectingToInternet()) {
            getAoutMe();
        } else {
            internet.showAlertDialog(getActivity(),
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }*/

        //final String[] number = getString(R.string.str_whatsapp_number).split(",");
        internet = new ConnectionDetector(AboutMeFragment.this);
        sp = getSharedPreferences("sanjay_wable", AboutMeFragment.this.MODE_PRIVATE);
        if (internet.isConnectingToInternet()) {
            getAoutMe();
        } else {
            internet.showAlertDialog(AboutMeFragment.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        txt_num1 = (TextView) findViewById(R.id.txt_num1);
        txt_num2 = (TextView) findViewById(R.id.txt_num2);
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_address = (TextView) findViewById(R.id.txt_address);
        txt_dob = (TextView) findViewById(R.id.txt_dob);
        txt_qualification = (TextView) findViewById(R.id.txt_qualification);
        txt_hobbies = (TextView) findViewById(R.id.txt_hobbies);
        txt_occupation = (TextView) findViewById(R.id.txt_occupation);

        // txt_num1.setText(number[1]);
        txt_num1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(txt_num1.getText().toString().trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });

        txt_num2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(txt_num2.getText().toString().trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });
       /* txt_num2 = (TextView) rootView.findViewById(R.id.txt_num2);
        txt_num2.setText(number[0]);
        txt_num2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(number[1].trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);
            }
        });*/


        //  return rootView;
    }


    private void getAoutMe() {
        final ProgressDialog dialog = new ProgressDialog(AboutMeFragment.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(AboutMeFragment.this);
        String requestURL = getString(R.string.web_path) + "myinfo.php";
        Log.e("requestURL", "myinfo.php" + requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", "myinfo.php" + response);
                        try {

                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);

                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                String imageUrl = jObj_result.getString("image");
                                displayImage(imageUrl);
                                if (jObj_result.getJSONArray("About_me").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "About_me").length(); i++) {
                                        JSONObject jobj_Data = jObj_result.getJSONArray("About_me").getJSONObject(i);
                                        txt_name.setText(jobj_Data.getString("name"));
                                        txt_address.setText(jobj_Data.getString("address"));
                                        txt_dob.setText(jobj_Data.getString("date_of_birth"));
                                        txt_qualification.setText(jobj_Data.getString("qualification"));
                                        txt_occupation.setText(jobj_Data.getString("occupation"));
                                        txt_hobbies.setText(jobj_Data.getString("hobbies"));
                                        String numbers = jobj_Data.getString("mobile_no");
                                        String[] mNumbers = numbers.split("/");

                                        txt_num1.setText(mNumbers[0]);
                                        txt_num2.setText(mNumbers[1]);

                                    }
                                }

                            } else if (jObj_result.getString("result").equals(
                                    "failed")) {
                                Toast.makeText(getApplicationContext(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void displayImage(String imageUrl) {
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mSliderImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
