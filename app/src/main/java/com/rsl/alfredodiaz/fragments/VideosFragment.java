package com.rsl.alfredodiaz.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.activity.PlayYoutubeVideos;
import com.rsl.alfredodiaz.utils.ConnectionDetector;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 2/8/2017.
 */

public class VideosFragment extends Fragment {
   // GridView gridview;
    Bitmap bitmap;
    ImageAdapter imageAdapter;
    ArrayList<HashMap<String, String>> list_data = null;
    ConnectionDetector internet;
    ListView list_videos;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //  return super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.videos_fragment, container, false);

        //  gridview= (GridView)rootView. findViewById(R.id.gridview);
        //gridview.setAdapter(new PhotosFragment.ImageAdapter(getActivity(), MenuImages));

       /* DataModel dbModel = new DataModel(this);
        list = dbModel.selectAll();*/

        internet = new ConnectionDetector(getActivity());
        list_videos = (ListView) rootView.findViewById(R.id.list_videos);

        list_data = new ArrayList<HashMap<String, String>>();
        //gridview.setAdapter(new ImageAdapter(getActivity()));
        imageAdapter = new ImageAdapter(getActivity());

        if (internet.isConnectingToInternet()) {
            getvideos();
        } else {
            internet.showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection.", false);
        }

        // gridview.setAdapter(imageAdapter);

        list_videos.setAdapter(imageAdapter);

        return rootView;
    }


    public class ImageAdapter extends BaseAdapter {


        private LayoutInflater inflater = null;
        Context context;

        public ImageAdapter(Context context) {

            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void add(HashMap<String, String> hash) {
            list_data.add(hash);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View list, ViewGroup parent) {
          /*  LayoutInflater inflater = getActivity().getLayoutInflater();
            list = inflater.inflate(R.layout.row_videos, parent, false);
            final ImageView imagemain = (ImageView) list.findViewById(R.id.imageView);
            final ImageButton img_btn_play = (ImageButton) list.findViewById(R.id.img_btn_play);
            //   final VideoView videoView = (VideoView) list.findViewById(R.id.videoView);

            final String videopath = getString(R.string.image_path) + list_data.get(position).get("video");
            final String imagepath = getString(R.string.image_path) + list_data.get(position).get("image");

            Glide.with(getActivity()).load(imagepath).dontAnimate().into(imagemain);

            list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), PlayVideoActivity.class);
                    i.putExtra("video", videopath);
                    startActivity(i);
                }
            });
            img_btn_play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), PlayVideoActivity.class);
                    i.putExtra("video", videopath);
                    startActivity(i);
                }
            });

            return list;*/
            list = inflater.inflate(R.layout.row_videos_list, parent, false);
            TextView txt_videos_name = (TextView) list.findViewById(R.id.txt_videos_name);

            txt_videos_name.setText(list_data.get(position).get("video_title"));
            txt_videos_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(getActivity(), PlayYoutubeVideos.class);
                    Log.e("video_title","="+list_data.get(position).get("video_title"));
                    Log.e("video","="+list_data.get(position).get("video"));
                    i.putExtra("video_name",list_data.get(position).get("video_title"));
                    i.putExtra("video_link",list_data.get(position).get("video"));
                    startActivity(i);
                }
            });

            return list;
        }
    }

    private void getvideos() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(getActivity());
        String requestURL = getString(R.string.web_path) + "media.php";

        Log.e("requestURL", requestURL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", response);
                        try {

                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                if (jObj_result.getJSONArray("videos").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "videos").length(); i++) {
                                        JSONObject jobj_Data = jObj_result.getJSONArray("videos").getJSONObject(i);
                                        HashMap<String, String> offers = new HashMap<String, String>();

                                        offers.put("video_title", jobj_Data.getString("video_title"));
                                        offers.put("video", jobj_Data.getString("video"));

                                        imageAdapter.add(offers);
                                    }
                                }

                            } else if (jObj_result.getString("result").equals("failed")) {
                                Toast.makeText(getActivity(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }

        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("type", "video");
                Log.e("params", "" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

}
