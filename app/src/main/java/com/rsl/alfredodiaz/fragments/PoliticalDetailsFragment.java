package com.rsl.alfredodiaz.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.utils.ConnectionDetector;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PoliticalDetailsFragment extends AppCompatActivity {
    View rootView;
    ListView lst_poli_details;
    String poli_data = "";
    private LayoutInflater inflater;
    MyCustomAdapter adapter;
    SharedPreferences sp;
    ConnectionDetector internet;
    //ArrayList<String> items;
    ArrayList<HashMap<String, String>> list_data = null;

    @BindView(R.id.img_home_slider)
    ImageView mSliderImage;

    @BindView(R.id.circular_progress2)
    ProgressBar mProgressbar2;

   /* public PoliticalDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_political_details, container, false);
        this.inflater = LayoutInflater.from(getActivity());
        lst_poli_details = (ListView) rootView.findViewById(R.id.lst_poli_details);
        *//*poli_data = getString(R.string.str_political_data);
        String[] aa = poli_data.split(",");
        items = new ArrayList<>();

        for (int i = 0; i < aa.length; i++) {
            adapter.add(aa[i]);
        }*//*
        internet = new ConnectionDetector(getActivity());
        sp = getActivity().getSharedPreferences("sanjay_wable", getActivity().MODE_PRIVATE);

        if (internet.isConnectingToInternet()) {
            getPoliticalDatails();
        } else {
            internet.showAlertDialog(getActivity(),
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        adapter = new MyCustomAdapter(getActivity());
        list_data=new ArrayList<HashMap<String, String>>();
        lst_poli_details.setDividerHeight(0);
        return rootView;
    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_political_details);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        ButterKnife.bind(this);
        mProgressbar2.setVisibility(View.INVISIBLE);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolTitle = (TextView) mToolbar.findViewById(R.id.tool_title);
        setSupportActionBar(mToolbar);
        mToolTitle.setText(getString(R.string.str_political));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lst_poli_details = (ListView) findViewById(R.id.lst_poli_details);
        internet = new ConnectionDetector(PoliticalDetailsFragment.this);
        sp = getSharedPreferences("sanjay_wable", PoliticalDetailsFragment.this.MODE_PRIVATE);

        if (internet.isConnectingToInternet()) {
            getPoliticalDatails();
        } else {
            internet.showAlertDialog(PoliticalDetailsFragment.this,
                    "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        adapter = new MyCustomAdapter(PoliticalDetailsFragment.this);
        list_data=new ArrayList<HashMap<String, String>>();
        lst_poli_details.setDividerHeight(0);
    }
        private class MyCustomAdapter extends BaseAdapter {
      /*  public void add(String data) {
            items.add(data);
        }
*/
      private LayoutInflater inflater = null;
        Context context;

        public MyCustomAdapter(Context context) {
            this.context = context;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public  void add(HashMap<String, String> hash){
          list_data.add(hash);
          notifyDataSetChanged();
      }
        @Override
        public int getCount() {
            return list_data.size();
        }

        @Override
        public Object getItem(int position) {
            return list_data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
           // View row = convertView;
           /* if (row == null) {
                row = inflater.inflate(R.layout.row_political_data, null);
            }
            TextView txt_item = (TextView) row.findViewById(R.id.txt_item);
            txt_item.setText(items.get(position));*/

            convertView = inflater.inflate(R.layout.row_political_data, parent, false);
            TextView txt_item = (TextView) convertView.findViewById(R.id.txt_item);
            txt_item.setText(list_data.get(position).get("title")+" : "+list_data.get(position).get("description"));

            return convertView;
        }
    }

    private void getPoliticalDatails() {
        final ProgressDialog dialog = new ProgressDialog(PoliticalDetailsFragment.this);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.str_loading));
        dialog.show();
        final RequestQueue queue = Volley.newRequestQueue(PoliticalDetailsFragment.this);
        String requestURL = getString(R.string.web_path) + "myinfo.php";
        Log.e("requestURL","myinfo.php"+ requestURL);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                requestURL.replaceAll(" ", "%20"),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("result", "myinfo.php"+response);
                        try {

                            dialog.dismiss();
                            JSONObject jObj_result = new JSONObject(response);
                            if (jObj_result.getString("result").equals(
                                    "success")) {
                                String imageUrl = jObj_result.getString("image");
                                displayImage(imageUrl);
                                if (jObj_result.getJSONArray("Political").length() > 0) {
                                    for (int i = 0; i < jObj_result.getJSONArray(
                                            "Political").length(); i++) {
                                        JSONObject jobj_Data = jObj_result.getJSONArray("Political").getJSONObject(i);
                                        HashMap<String, String> offers = new HashMap<String, String>();

                                        offers.put("title",jobj_Data.getString("title"));
                                        offers.put("description",jobj_Data.getString("description"));
                                        adapter.add(offers);

                                    }
                                    lst_poli_details.setAdapter(adapter);
                                }

                            } else if(jObj_result.getString("result").equals(
                                    "failed")){
                                Toast.makeText(getApplicationContext(), "" + jObj_result.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("language", sp.getString("my_lang", "english"));
                Log.e("params", "" + params);
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void displayImage(String imageUrl) {
        mProgressbar2.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(imageUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), "Error while loading the image", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mProgressbar2.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(mSliderImage);
    }
}
