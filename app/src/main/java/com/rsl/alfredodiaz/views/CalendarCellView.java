package com.rsl.alfredodiaz.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by pratikvelani on 7/1/15.
 */
public class CalendarCellView extends LinearLayout {
    public CalendarCellView(Context context) {
        super(context);
    }

    public CalendarCellView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CalendarCellView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CalendarCellView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        //Log.d(">>", "" + widthMeasureSpec + "::" + heightMeasureSpec);

        int measure = (int) Math.round(widthMeasureSpec * 0.75);

        setMeasuredDimension(measure, measure);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        //Log.d("SIZE", "" + w +  "::" + h);

        int width = (int) Math.round(w*0.75);

        super.onSizeChanged(width, width, oldw, oldh);
    }
}