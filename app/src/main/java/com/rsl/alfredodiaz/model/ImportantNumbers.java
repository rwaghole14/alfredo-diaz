package com.rsl.alfredodiaz.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rsl on 7/3/18.
 */

public class ImportantNumbers {

    private String mImpNoMainTitle;
    private String mImpNoMainId;
    private String mImpNoSubId;
    private String mImpNoSubTitle;
    private String mImpNoAddress;
    private String mImpNoContact;
    private String mImpNoImage;
    private ArrayList<HashMap<String, String>> mChildItems;

    public ImportantNumbers() {
    }

    public ImportantNumbers(String mImpNoSubTitle, String mImpNoAddress, String mImpNoContact, String mImpNoImage) {
        this.mImpNoSubTitle = mImpNoSubTitle;
        this.mImpNoAddress = mImpNoAddress;
        this.mImpNoContact = mImpNoContact;
        this.mImpNoImage = mImpNoImage;
    }

    public void setmImpNoSubTitle(String mImpNoSubTitle) {
        this.mImpNoSubTitle = mImpNoSubTitle;
    }

    public String getmImpNoMainTitle() {
        return mImpNoMainTitle;
    }

    public String getmImpNoMainId() {
        return mImpNoMainId;
    }

    public String getmImpNoSubId() {
        return mImpNoSubId;
    }

    public String getmImpNoSubTitle() {
        return mImpNoSubTitle;
    }

    public String getmImpNoAddress() {
        return mImpNoAddress;
    }

    public String getmImpNoContact() {
        return mImpNoContact;
    }

    public String getmImpNoImage() {
        return mImpNoImage;
    }

    public ArrayList<HashMap<String, String>> getChildItems() {
        return mChildItems;
    }

    public void setChildItems(ArrayList<HashMap<String, String>> mChildItems) {
        this.mChildItems = mChildItems;
    }
}
