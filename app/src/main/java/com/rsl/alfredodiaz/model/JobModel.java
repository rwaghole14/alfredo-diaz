package com.rsl.alfredodiaz.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rsl on 9/3/18.
 */

public class JobModel {

    private String mJobType;
    private String mJobID;
    private String mJobTitle;
    private String mJobLink;
    private ArrayList<HashMap<String, String>> mChildItems;

    public JobModel() {
    }

    public ArrayList<HashMap<String, String>> getmChildItems() {
        return mChildItems;
    }

    public void setmChildItems(ArrayList<HashMap<String, String>> mChildItems) {
        this.mChildItems = mChildItems;
    }

    public JobModel(String mJobType, String mJobTitle, String mJobLink) {
        this.mJobType = mJobType;
        this.mJobTitle = mJobTitle;
        this.mJobLink = mJobLink;
    }

    public String getmJobType() {
        return mJobType;
    }

    public String getmJobID() {
        return mJobID;
    }

    public String getmJobTitle() {
        return mJobTitle;
    }

    public String getmJobLink() {
        return mJobLink;
    }

    public void setmJobType(String mJobType) {
        this.mJobType = mJobType;
    }

    public void setmJobTitle(String mJobTitle) {
        this.mJobTitle = mJobTitle;
    }

    public void setmJobLink(String mJobLink) {
        this.mJobLink = mJobLink;
    }
}
