package com.rsl.alfredodiaz.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rsl on 6/3/18.
 */

public class EmergencyServices {

    private String mEmergencyMainTitle;
    private String mEmergencyServiceId;
    private String mEmergencySubServiceId;
    private String mEmergencySubTitle;
    private String mEmergencyServiceAddress;
    private String mEmergencyServiceContact;
    private String mEmergencyServiceImage;
    private ArrayList<HashMap<String, String>> mChildItems;

    public EmergencyServices() {
    }

    public EmergencyServices(String mEmergencySubTitle) {
        this.mEmergencySubTitle = mEmergencySubTitle;
    }

    public EmergencyServices(String mEmergencyServiceAddress, String mEmergencyServiceContact) {
        this.mEmergencyServiceAddress = mEmergencyServiceAddress;
        this.mEmergencyServiceContact = mEmergencyServiceContact;
    }

    public EmergencyServices(String mEmergencySubTitle, String mEmergencyServiceAddress, String mEmergencyServiceContact) {
        this.mEmergencySubTitle = mEmergencySubTitle;
        this.mEmergencyServiceAddress = mEmergencyServiceAddress;
        this.mEmergencyServiceContact = mEmergencyServiceContact;
    }

    //Setter methods
    public void setmEmergencyMainTitle(String mEmergencyMainTitle) {
        this.mEmergencyMainTitle = mEmergencyMainTitle;
    }

    public void setmEmergencyServiceId(String mEmergencyServiceId) {
        this.mEmergencyServiceId = mEmergencyServiceId;
    }

    public void setmEmergencySubServiceId(String mEmergencySubServiceId) {
        this.mEmergencySubServiceId = mEmergencySubServiceId;
    }

    public void setmEmergencySubTitle(String mEmergencySubTitle) {
        this.mEmergencySubTitle = mEmergencySubTitle;
    }

    public void setmEmergencyServiceAddress(String mEmergencyServiceAddress) {
        this.mEmergencyServiceAddress = mEmergencyServiceAddress;
    }

    public void setmEmergencyServiceContact(String mEmergencyServiceContact) {
        this.mEmergencyServiceContact = mEmergencyServiceContact;
    }

    public void setmEmergencyServiceImage(String mEmergencyServiceImage) {
        this.mEmergencyServiceImage = mEmergencyServiceImage;
    }

    public ArrayList<HashMap<String, String>> getChildItems() {
        return mChildItems;
    }

    public void setChildItems(ArrayList<HashMap<String, String>> child_items) {
        this.mChildItems = child_items;
    }


    //Getter methods
    public String getmEmergencyMainTitle() {
        return mEmergencyMainTitle;
    }

    public String getmEmergencyServiceId() {
        return mEmergencyServiceId;
    }

    public String getmEmergencySubServiceId() {
        return mEmergencySubServiceId;
    }

    public String getmEmergencySubTitle() {
        return mEmergencySubTitle;
    }

    public String getmEmergencyServiceAddress() {
        return mEmergencyServiceAddress;
    }

    public String getmEmergencyServiceContact() {
        return mEmergencyServiceContact;
    }

    public String getmEmergencyServiceImage() {
        return mEmergencyServiceImage;
    }

}
