package com.rsl.alfredodiaz.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by PC-9 on 27-09-2017.
 */

public class Gallery {
    String Title;
    String Description;
    String Date;
    ArrayList<HashMap<String, String>> Gallery_images = new ArrayList<>();

    public ArrayList<HashMap<String, String>> getGallery_images() {
        return Gallery_images;
    }

    public void setGallery_images(ArrayList<HashMap<String, String>> gallery_images) {
        Gallery_images = gallery_images;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
