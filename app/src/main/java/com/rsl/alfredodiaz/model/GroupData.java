package com.rsl.alfredodiaz.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vinay on 19-11-2016.
 */

public class GroupData {
    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    private String group_name;
    private String group_id;
    private String group_description;
    private String group_image;
    private String mEstimateCost;
    private String mGovermentCost;
    private String mActualCost;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    private ArrayList<HashMap<String, String>> child_items;

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }


    public String getGroup_description() {
        return group_description;
    }

    public void setGroup_description(String group_description) {
        this.group_description = group_description;
    }

    public void setmEstimateCost(String mEstimateCost) {
        this.mEstimateCost = mEstimateCost;
    }

    public void setmGovermentCost(String mGovermentCost) {
        this.mGovermentCost = mGovermentCost;
    }

    public void setmActualCost(String mActualCost) {
        this.mActualCost = mActualCost;
    }

    public String getmEstimateCost() {
        return mEstimateCost;
    }

    public String getmGovermentCost() {
        return mGovermentCost;
    }

    public String getmActualCost() {
        return mActualCost;
    }

    public String getGroup_image() {
        return group_image;
    }

    public void setGroup_image(String group_image) {
        this.group_image = group_image;
    }


//    public HashMap<String, String> getChild_items() {
//        return child_items;
//    }
//
//    public void setChild_items(HashMap<String, String> child_items) {
//        this.child_items = child_items;
//    }

    public ArrayList<HashMap<String, String>> getChild_items() {
        return child_items;
    }

    public void setChild_items(ArrayList<HashMap<String, String>> child_items) {
        this.child_items = child_items;
    }


}
