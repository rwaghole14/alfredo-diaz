package com.rsl.alfredodiaz.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rsl on 8/3/18.
 */

public class OnlineForms {

    private String mMainTitle;
    private int mMainSectionId;
    private int mFormId;
    private String mSubTitle;
    private String mFormLink;
    private ArrayList<HashMap<String, String>> mChildItems;

    public OnlineForms() {
    }

    public OnlineForms(String mMainTitle) {
        this.mMainTitle = mMainTitle;
    }

    public OnlineForms(String mSubTitle, String mFormLink) {
        this.mSubTitle = mSubTitle;
        this.mFormLink = mFormLink;
    }

    public OnlineForms(String mMainTitle, String mSubTitle, String mFormLink) {
        this.mMainTitle = mMainTitle;
        this.mSubTitle = mSubTitle;
        this.mFormLink = mFormLink;
    }

    public void setmMainTitle(String mMainTitle) {
        this.mMainTitle = mMainTitle;
    }

    public void setmChildItems(ArrayList<HashMap<String, String>> mChildItems) {
        this.mChildItems = mChildItems;
    }

    public String getmMainTitle() {
        return mMainTitle;
    }

    public int getmMainSectionId() {
        return mMainSectionId;
    }

    public int getmFormId() {
        return mFormId;
    }

    public String getmSubTitle() {
        return mSubTitle;
    }

    public String getmFormLink() {
        return mFormLink;
    }

    public ArrayList<HashMap<String, String>> getmChildItems() {
        return mChildItems;
    }
}
