package com.rsl.alfredodiaz.adapter;

/**
 * Created by vinay on 29-09-2015.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.activity.WorkDetailsActivity;
import com.rsl.alfredodiaz.model.GroupData;
import com.rsl.alfredodiaz.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Heaven on 24-09-2015.
 */
public class WorktAdapter extends RecyclerView.Adapter<WorktAdapter.PlaceViewHolder> {
    private static ArrayList<GroupData> mPlaces;
    private static ArrayList<GroupData> perm;
    private static Activity object;

    public WorktAdapter(ArrayList<GroupData> places, Activity object) {
        this.mPlaces = places;
        this.object = object;
        this.perm = places;

    }

    public void updateList(ArrayList<GroupData> data) {
        mPlaces = data;
        notifyDataSetChanged();

    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_work_data, parent, false);
        PlaceViewHolder pvh = new PlaceViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder holder, int position) {
        String name = mPlaces.get(position).getGroup_name();
        String url = object.getString(R.string.image_path) + mPlaces.get(position).getGroup_image();
        holder.txt_data.setText(name);
        Glide.with(object.getApplicationContext()).load(url).into(holder.icon);
    }

    @Override
    public int getItemCount() {

        return this.mPlaces.size();
    }

    public static class PlaceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_data;
        CircleImageView icon;


        PlaceViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            txt_data = (TextView) itemView.findViewById(R.id.txt_data);
            icon = (CircleImageView) itemView.findViewById(R.id.img_work);

        }

        @Override
        public void onClick(View v) {
            Context context = itemView.getContext();
            int pos = getAdapterPosition();
            Intent i = new Intent(object, WorkDetailsActivity.class);
            i.putExtra("title", mPlaces.get(pos).getGroup_name());
            i.putExtra("description", mPlaces.get(pos).getGroup_description());
            Constants.setImage_data(mPlaces.get(pos).getChild_items());
            Log.e("images", "====" + mPlaces.get(pos).getChild_items());
            object.startActivity(i);
            //object.finish();
        }
    }

    public void setFilter(List<GroupData> filteredList) {
        mPlaces = new ArrayList<>();
        mPlaces.addAll(filteredList);
        notifyDataSetChanged();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}

