package com.rsl.alfredodiaz.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.rsl.alfredodiaz.fragments.PhotosFragment;
import com.rsl.alfredodiaz.fragments.VideosFragment;

/**
 * Created by PC-9 on 17-03-2017.
 */

public class MediaPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public MediaPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                PhotosFragment tab1 = new PhotosFragment();
                return tab1;
            case 1:
                VideosFragment tab2 = new VideosFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
