package com.rsl.alfredodiaz.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by admin on 2/17/2016.
 */
public class EventsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public EventsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                /*AboutMeFragment tab1 = new AboutMeFragment();
                return tab1;*/
            case 1:
                /*PoliticalDetailsFragment tab2 = new PoliticalDetailsFragment();
                return tab2;*/
            case 2:
               /* InspirationFragment tab3 = new InspirationFragment();
                return tab3;*/

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

