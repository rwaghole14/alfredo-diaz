package com.rsl.alfredodiaz.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gtomato.android.ui.widget.CarouselView;
import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.GameEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rsl on 12/3/18.
 */

public class CarousalViewAdapter extends CarouselView.Adapter<CarousalViewAdapter.ViewHolder> {

    private ArrayList<GameEntity> mData = new ArrayList<>(0);
    private Context mContext;

    public CarousalViewAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void add(GameEntity ge){
        mData.add(ge);
        //notifyDataSetChanged();
    }

    public void clear(){
        mData.clear();
        //notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.row_allwork, null, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mMenuTextView.setText(mContext.getResources().getString(mData.get(position).titleResId));
        holder.mMenuImageView.setImageResource(mData.get(position).imageResId);

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_menuname)
        TextView mMenuTextView;

        @BindView(R.id.img_allimage)
        ImageView mMenuImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
