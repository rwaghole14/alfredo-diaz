package com.rsl.alfredodiaz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.GameEntity;

import java.util.ArrayList;

public class CoverFlowAdapter extends BaseAdapter {
	
	private ArrayList<GameEntity> mData = new ArrayList<>(0);
	private Context mContext;

	public CoverFlowAdapter(Context context) {
		this.mContext = context;
	}
	
/*	public void setData(ArrayList<GameEntity> data) {
		this.mData = data;
	}*/

	public void add(GameEntity ge){
		mData.add(ge);
		//notifyDataSetChanged();
	}
	public void clear(){
		mData.clear();
		//notifyDataSetChanged();
	}
	@Override
	public int getCount() {

		return mData.size();
	}

	@Override
	public Object getItem(int pos) {
		return mData.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
		ViewHolder viewHolder=null;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_allwork, null);
			 viewHolder = new ViewHolder();
			viewHolder.text = (TextView) rowView.findViewById(R.id.txt_menuname);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.img_allimage);
		//	viewHolder.img_circle = (CircleImageView) rowView.findViewById(R.id.img_circle);
			Typeface face = Typeface.createFromAsset(mContext.getAssets(),
					"fonts/OpenSans-Light.ttf");
			viewHolder.text.setTypeface(face);
			rowView.setTag(viewHolder);


		}else {
			viewHolder = (ViewHolder) rowView.getTag();
		}

		viewHolder.image.setImageResource(mData.get(position).imageResId);
        String name= mContext.getResources().getString(mData.get(position).titleResId);
		Log.e("check","----"+name);
/*		if (name.length()>=10){
			viewHolder.text.setText( mContext.getResources().getString(mData.get(position).titleResId)+"\n");
		}else {*/
			viewHolder.text.setText(mContext.getResources().getString(mData.get(position).titleResId));
	//	}

		return rowView;
	}


    static class ViewHolder {
        public TextView text;
        public ImageView image;
       // public CircleImageView img_circle;
    }
}
