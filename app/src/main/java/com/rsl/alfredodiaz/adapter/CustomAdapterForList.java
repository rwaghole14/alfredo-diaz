package com.rsl.alfredodiaz.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.rsl.alfredodiaz.R;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rsl on 16/3/18.
 */

public class CustomAdapterForList extends BaseAdapter {

    @BindView(R.id.inspiration_name)
    TextView mInspirationName;

    @BindView(R.id.inspiration_type)
    TextView mInspirationType;

    private ArrayList<HashMap<String, String>> mArrayList = new ArrayList<>();
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    public CustomAdapterForList(Context mContext) {
        this.mContext = mContext;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void add(HashMap<String, String> mHash) {
        mArrayList.add(mHash);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrayList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(mContext).inflate(R.layout.list_with_2_items, viewGroup, false);
        Log.e("Inspiration", "type: " + mArrayList.get(position).get("type")
                + " inpiration: " + mArrayList.get(position).get("inspiration"));
        ButterKnife.bind(this, view);

        mInspirationType.setText(mArrayList.get(position).get("type"));
        mInspirationName.setText(mArrayList.get(position).get("inspiration"));

        return view;
    }
}
