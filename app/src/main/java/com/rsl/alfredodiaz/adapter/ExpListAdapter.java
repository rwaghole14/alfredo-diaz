package com.rsl.alfredodiaz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.rsl.alfredodiaz.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by rsl on 8/3/18.
 */

public class ExpListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> mHeaders;
    private HashMap<String, List<String>> mChilds;
    private ExpandableListView mExpandableList;

    public ExpListAdapter(Context mContext, List<String> mHeaders, HashMap<String, List<String>> mChilds) {
        this.mContext = mContext;
        this.mHeaders = mHeaders;
        this.mChilds = mChilds;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.mChilds.get(this.mHeaders.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosititon) {
        return childPosititon;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.mChilds.get(this.mHeaders.get(groupPosition)).size();
    }


    @Override
    public View getChildView(int groupPosition,
                             int childPosition,
                             boolean isLastChild,
                             View convertView, ViewGroup parent) {
        String childText = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_political_data, null);
        }
        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.txt_item);
        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public Object getGroup(int groupPosition ){
        return this.mHeaders.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mHeaders.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.layout_group, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        //lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        convertView.setTag(groupPosition);
//        ImageView imageView = convertView.findViewById(R.id.buttonImageView);
//        if (isExpanded) {
//            imageView.setImageResource(R.drawable.expandable_list_group_button_close);
//        } else {
//            imageView.setImageResource(R.drawable.expandable_list_group_button_open);
//        }
        //_listView.setDividerHeight(20);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
