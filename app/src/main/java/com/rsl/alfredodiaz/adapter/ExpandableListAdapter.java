package com.rsl.alfredodiaz.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.model.EmergencyServices;
import com.rsl.alfredodiaz.model.ImportantNumbers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vinay on 19-11-2016.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    ArrayList<EmergencyServices> services;
    ArrayList<HashMap<String, String>> child;
    ArrayList<ImportantNumbers> numbers;
    private int mNo = 0;

    public ExpandableListAdapter(Context _context, ArrayList<ImportantNumbers> numbers, int no) {
        this._context = _context;
        this.numbers = numbers;
        this.mNo = no;
    }

    public ExpandableListAdapter(Context context, ArrayList<EmergencyServices> cart_group) {
        this._context = context;
        this.services = cart_group;
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final HashMap<String, String> child = getChild(groupPosition, childPosition);
        final String child_phone = child.get("phone");
        final String child_address = child.get("address");
        Log.e("child_phone", "====" + child_phone);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.layout_child, null);
        }
//        LinearLayout lay_phone = (LinearLayout) convertView.findViewById(R.id.lay_phone);
//        for (int i = 0; i < child_phone.split(",").length; i++) {
//            LayoutInflater inflater = null;
//            inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View mLinearView = inflater.inflate(R.layout.row_phone_number, null);
//            TextView txt_ph = (TextView) mLinearView.findViewById(R.id.lblListItem);
//            txt_ph.setText(child_phone.split(",")[i].trim());
//            txt_ph.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
//                    callIntent.setData(Uri.parse("tel:" + Uri.encode(txt_ph.getText().toString().trim())));
//                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    _context.startActivity(callIntent);
//                }
//            });
//            lay_phone.addView(mLinearView);
//        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView txt_address = (TextView) convertView.findViewById(R.id.txt_address);

        txtListChild.setText(child_phone);
        txt_address.setText(child_address);
        txtListChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectNumber(child_phone);

//                Intent callIntent = new Intent(Intent.ACTION_DIAL);
//                callIntent.setData(Uri.parse("tel:" + Uri.encode(child_phone.split(",")[0].trim())));
//                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                _context.startActivity(callIntent);
            }
        });

        return convertView;
    }


    private void selectNumber(String str_numbers) {
        final CharSequence[] items = str_numbers.split(",");
        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle(_context.getString(R.string.str_call_to));
        final HashMap<String, String> p = new HashMap<String, String>();
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        p.put("pos", "" + 0);
                        break;
                    case 1:
                        p.put("pos", "" + 1);
                        break;
                    case 2:
                        p.put("pos", "" + 2);
                        break;
                    default:
                        break;
                }
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(items[Integer.parseInt(p.get("pos"))].toString().trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                _context.startActivity(callIntent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        if (mNo == 1){
            if (numbers.get(groupPosition).getChildItems() != null) {
                child = numbers.get(groupPosition).getChildItems();
                Log.e("child.size", "====" + child.size());
                return child.size();
            } else
                return 0;
        }else {
            if (services.get(groupPosition).getChildItems() != null) {
                child = services.get(groupPosition).getChildItems();
                Log.e("child.size", "====" + child.size());
                return child.size();
            } else
                return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        if (mNo == 1){
            return numbers.get(groupPosition);
        }else {
            return services.get(groupPosition);
        }
    }

    @Override
    public HashMap<String, String> getChild(int groupPosition, int childPosition) {
        if (mNo == 1){
            return numbers.get(groupPosition).getChildItems().get(childPosition);
        }else {
            return services.get(groupPosition).getChildItems().get(childPosition);
        }
    }

    @Override
    public int getGroupCount() {
        if (mNo == 1){
            return numbers.size();
        }else {
            return services.size();
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle;

        if (mNo == 1){
            ImportantNumbers importantNumbers = numbers.get(groupPosition);
            headerTitle = importantNumbers.getmImpNoSubTitle();
        }else {
            EmergencyServices emergencyServices = services.get(groupPosition);
            headerTitle = emergencyServices.getmEmergencySubTitle();
        }

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.layout_group, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setText(headerTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
