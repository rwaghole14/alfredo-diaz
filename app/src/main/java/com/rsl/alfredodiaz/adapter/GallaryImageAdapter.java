package com.rsl.alfredodiaz.adapter;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.rsl.alfredodiaz.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vinay on 18-02-2016.
 */
public class GallaryImageAdapter extends BaseAdapter {
    ArrayList<HashMap<String, String>> image_data;
    private LayoutInflater mInflater;
    private int mItemHeight = 0;
    private int mNumColumns = 0;
    Context context;
    private RelativeLayout.LayoutParams mImageViewLayoutParams;
    private Animation anim_fade_in, anim_fade_out;

    public GallaryImageAdapter(Context context) {
        this.context = context;
        image_data = new ArrayList<HashMap<String, String>>();
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT,
                GridLayout.LayoutParams.MATCH_PARENT);
        anim_fade_in = AnimationUtils.loadAnimation(
                context, R.anim.animation_fade_in);
        anim_fade_out = AnimationUtils.loadAnimation(
                context, R.anim.animation_fade_out);
    }

    public void add(HashMap<String, String> add) {
        image_data.add(add);
        notifyDataSetChanged();
    }

    public void remove(int pos) {
        image_data.remove(pos);
        notifyDataSetChanged();
    }

    public void clear() {
        image_data.clear();
    }

    public int getCount() {
        return image_data.size();
    }

    // set numcols
    public void setNumColumns(int numColumns) {
        mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return mNumColumns;
    }

    // set photo item height
    public void setItemHeight(int height) {
        if (height == mItemHeight) {
            return;
        }
        mItemHeight = height;
        mImageViewLayoutParams = new RelativeLayout.LayoutParams(GridLayout.LayoutParams.MATCH_PARENT, mItemHeight);
        notifyDataSetChanged();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null)
            view = mInflater.inflate(R.layout.photo_item, null);

        ImageView cover = (ImageView) view.findViewById(R.id.cover);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        cover.setLayoutParams(mImageViewLayoutParams);

        if (cover.getLayoutParams().height != mItemHeight) {
            cover.setLayoutParams(mImageViewLayoutParams);
        }
        final String imagepath = context.getString(R.string.image_path) + image_data.get(position).get("image");
        final String description = image_data.get(position).get("text").trim();
        Log.e("image", "" + imagepath);

        Glide.with(context.getApplicationContext()).load(imagepath).dontAnimate().placeholder(R.drawable.image_placeholder)
                .into(new GlideDrawableImageViewTarget(cover) {
                    @Override
                    public void onResourceReady(GlideDrawable arg0,
                                                GlideAnimation<? super GlideDrawable> arg1) {
                        // TODO Auto-generated method stub
                        super.onResourceReady(arg0, arg1);
                        progressBar.setVisibility(View.GONE);
                    }
                });
        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                openDilog(imagepath, description);
            }
        });
        return view;
    }

    private void openDilog(String imagepath, String description) {
        final Dialog dialogView = new Dialog(context, R.style.DialogTheme);
        dialogView.getWindow().requestFeature(1);
        dialogView.setContentView(R.layout.view_gallery_images);
        dialogView.setCancelable(false);
        dialogView.setCanceledOnTouchOutside(true);
        ImageView profilepictureview = (ImageView) dialogView.findViewById(R.id.member_picture);
        final ProgressBar progressBar = (ProgressBar) dialogView.findViewById(R.id.progressbar);
        final TextView txt_description = (TextView) dialogView.findViewById(R.id.txt_description);
        final ImageView img_back = (ImageView) dialogView.findViewById(R.id.img_back);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogView.dismiss();
            }
        });
        txt_description.setText(description);
        try {
            //  profilepictureview.setImageResource(imagepath);
            Glide.with(context.getApplicationContext()).load(imagepath).dontAnimate().placeholder(R.drawable.image_placeholder)
                    .into(new GlideDrawableImageViewTarget(profilepictureview) {
                        @Override
                        public void onResourceReady(GlideDrawable arg0,
                                                    GlideAnimation<? super GlideDrawable> arg1) {
                            // TODO Auto-generated method stub
                            super.onResourceReady(arg0, arg1);
                            progressBar.setVisibility(View.GONE);
                        }
                    });


        } catch (Exception exception) {
            exception.printStackTrace();
        }
        profilepictureview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txt_description.getVisibility() == View.VISIBLE) {
//                    txt_description.setAnimation(anim_fade_out);
                    txt_description.setVisibility(View.GONE);
                } else {
//                    txt_description.setAnimation(anim_fade_in);
                    txt_description.setVisibility(View.VISIBLE);
                }
            }
        });



        dialogView.show();
    }

}
