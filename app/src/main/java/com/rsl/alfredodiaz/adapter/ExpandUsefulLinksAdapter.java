package com.rsl.alfredodiaz.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.rsl.alfredodiaz.R;
import com.rsl.alfredodiaz.activity.UsefulFormDetailsActivity;
import com.rsl.alfredodiaz.model.OnlineForms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vinay on 16-12-2016.
 */

public class ExpandUsefulLinksAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private ArrayList<OnlineForms> cart_group;
    private ArrayList<HashMap<String, String>> child;

    public ExpandUsefulLinksAdapter(Context context, ArrayList<OnlineForms> cart_group) {
        this._context = context;
        this.cart_group = cart_group;
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final HashMap<String, String> child = getChild(groupPosition, childPosition);
        final String child_phone = child.get("title");
        Log.e("child_phone", "====" + child_phone);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_political_data, null);
        }
        TextView txt_item = (TextView) convertView.findViewById(R.id.txt_item);
        txt_item.setText(child_phone);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(_context, UsefulFormDetailsActivity.class);
                i.putExtra("title", "" + child.get("title"));
                i.putExtra("link", "" + child.get("link"));
                _context.startActivity(i);
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (cart_group.get(groupPosition).getmChildItems() != null) {
            child = cart_group.get(groupPosition).getmChildItems();
            Log.e("child.size", "====" + child.size());
            return child.size();
        } else
            return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return cart_group.get(groupPosition);
    }

    @Override
    public HashMap<String, String> getChild(int groupPosition, int childPosition) {
        return cart_group.get(groupPosition).getmChildItems().get(childPosition);
    }

    @Override
    public int getGroupCount() {
        return cart_group.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final OnlineForms grList = cart_group.get(groupPosition);
        String headerTitle = grList.getmMainTitle();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.layout_group, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}