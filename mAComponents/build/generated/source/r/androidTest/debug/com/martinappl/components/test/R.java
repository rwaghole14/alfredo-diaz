/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.martinappl.components.test;

public final class R {
  public static final class attr {
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int adjustPositionMultiplier=0x7f010000;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int adjustPositionThreshold=0x7f010001;
    /**
     * <p>May be an integer value, such as "<code>100</code>".
     */
    public static final int alignAnimationTime=0x7f010002;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int circlePathRadius=0x7f010003;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int circlePathRadiusInMatrixSpace=0x7f010004;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int coverHeight=0x7f010005;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int coverWidth=0x7f010006;
    /**
     * <p>May be an integer value, such as "<code>100</code>".
     */
    public static final int deviceSpecificPixelSize=0x7f010007;
    /**
     * <p>Must be one or more (separated by '|') of the following constant values.</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Constant</th><th>Value</th><th>Description</th></tr>
     * <tr><td>dynamic</td><td>1</td><td></td></tr>
     * <tr><td>fixed</td><td>0</td><td></td></tr>
     * </table>
     */
    public static final int gridMode=0x7f010008;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int maxRotationAngle=0x7f010009;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int maxScaleFactor=0x7f01000a;
    /**
     * <p>May be a color value, in the form of "<code>#<i>rgb</i></code>",
     * "<code>#<i>argb</i></code>", "<code>#<i>rrggbb</i></code>", or
     * "<code>#<i>aarrggbb</i></code>".
     */
    public static final int reflectionBackroundColor=0x7f01000b;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int reflectionGap=0x7f01000c;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int reflectionHeight=0x7f01000d;
    /**
     * <p>May be an integer value, such as "<code>100</code>".
     */
    public static final int reflectionOpacity=0x7f01000e;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int rotationThreshold=0x7f01000f;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int scalingThreshold=0x7f010010;
    /**
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     */
    public static final int shouldRepeat=0x7f010011;
    /**
     * <p>May be a floating point value, such as "<code>1.2</code>".
     */
    public static final int spacing=0x7f010012;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int tunningWidgetSize=0x7f010013;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int verticalPaddingBottom=0x7f010014;
    /**
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static final int verticalPaddingTop=0x7f010015;
  }
  public static final class drawable {
    public static final int ico_delete_asset=0x7f020000;
  }
  public static final class id {
    public static final int dynamic=0x7f030000;
    public static final int fixed=0x7f030001;
  }
  public static final class string {
    public static final int app_name=0x7f040000;
  }
  public static final class style {
    public static final int AppBaseTheme=0x7f050000;
    public static final int AppTheme=0x7f050001;
  }
  public static final class styleable {
    /**
     * Attributes that can be used with a BasicContentBand.
     * <p>Includes the following attributes:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Attribute</th><th>Description</th></tr>
     * <tr><td><code>{@link #BasicContentBand_deviceSpecificPixelSize com.martinappl.components.test:deviceSpecificPixelSize}</code></td><td></td></tr>
     * <tr><td><code>{@link #BasicContentBand_gridMode com.martinappl.components.test:gridMode}</code></td><td></td></tr>
     * </table>
     * @see #BasicContentBand_deviceSpecificPixelSize
     * @see #BasicContentBand_gridMode
     */
    public static final int[] BasicContentBand={
        0x7f010007, 0x7f010008
      };
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#deviceSpecificPixelSize}
     * attribute's value can be found in the {@link #BasicContentBand} array.
     *
     * <p>May be an integer value, such as "<code>100</code>".
     *
     * @attr name com.martinappl.components.test:deviceSpecificPixelSize
     */
    public static final int BasicContentBand_deviceSpecificPixelSize=0;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#gridMode}
     * attribute's value can be found in the {@link #BasicContentBand} array.
     *
     * <p>Must be one or more (separated by '|') of the following constant values.</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Constant</th><th>Value</th><th>Description</th></tr>
     * <tr><td>dynamic</td><td>1</td><td></td></tr>
     * <tr><td>fixed</td><td>0</td><td></td></tr>
     * </table>
     *
     * @attr name com.martinappl.components.test:gridMode
     */
    public static final int BasicContentBand_gridMode=1;
    /**
     * Attributes that can be used with a EndlessLoopAdapterContainer.
     * <p>Includes the following attributes:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Attribute</th><th>Description</th></tr>
     * <tr><td><code>{@link #EndlessLoopAdapterContainer_shouldRepeat com.martinappl.components.test:shouldRepeat}</code></td><td></td></tr>
     * </table>
     * @see #EndlessLoopAdapterContainer_shouldRepeat
     */
    public static final int[] EndlessLoopAdapterContainer={
        0x7f010011
      };
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#shouldRepeat}
     * attribute's value can be found in the {@link #EndlessLoopAdapterContainer} array.
     *
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     *
     * @attr name com.martinappl.components.test:shouldRepeat
     */
    public static final int EndlessLoopAdapterContainer_shouldRepeat=0;
    /**
     * Attributes that can be used with a FeatureCoverFlow.
     * <p>Includes the following attributes:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Attribute</th><th>Description</th></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_adjustPositionMultiplier com.martinappl.components.test:adjustPositionMultiplier}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_adjustPositionThreshold com.martinappl.components.test:adjustPositionThreshold}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_alignAnimationTime com.martinappl.components.test:alignAnimationTime}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_circlePathRadius com.martinappl.components.test:circlePathRadius}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_circlePathRadiusInMatrixSpace com.martinappl.components.test:circlePathRadiusInMatrixSpace}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_coverHeight com.martinappl.components.test:coverHeight}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_coverWidth com.martinappl.components.test:coverWidth}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_maxRotationAngle com.martinappl.components.test:maxRotationAngle}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_maxScaleFactor com.martinappl.components.test:maxScaleFactor}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_reflectionBackroundColor com.martinappl.components.test:reflectionBackroundColor}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_reflectionGap com.martinappl.components.test:reflectionGap}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_reflectionHeight com.martinappl.components.test:reflectionHeight}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_reflectionOpacity com.martinappl.components.test:reflectionOpacity}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_rotationThreshold com.martinappl.components.test:rotationThreshold}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_scalingThreshold com.martinappl.components.test:scalingThreshold}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_spacing com.martinappl.components.test:spacing}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_tunningWidgetSize com.martinappl.components.test:tunningWidgetSize}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_verticalPaddingBottom com.martinappl.components.test:verticalPaddingBottom}</code></td><td></td></tr>
     * <tr><td><code>{@link #FeatureCoverFlow_verticalPaddingTop com.martinappl.components.test:verticalPaddingTop}</code></td><td></td></tr>
     * </table>
     * @see #FeatureCoverFlow_adjustPositionMultiplier
     * @see #FeatureCoverFlow_adjustPositionThreshold
     * @see #FeatureCoverFlow_alignAnimationTime
     * @see #FeatureCoverFlow_circlePathRadius
     * @see #FeatureCoverFlow_circlePathRadiusInMatrixSpace
     * @see #FeatureCoverFlow_coverHeight
     * @see #FeatureCoverFlow_coverWidth
     * @see #FeatureCoverFlow_maxRotationAngle
     * @see #FeatureCoverFlow_maxScaleFactor
     * @see #FeatureCoverFlow_reflectionBackroundColor
     * @see #FeatureCoverFlow_reflectionGap
     * @see #FeatureCoverFlow_reflectionHeight
     * @see #FeatureCoverFlow_reflectionOpacity
     * @see #FeatureCoverFlow_rotationThreshold
     * @see #FeatureCoverFlow_scalingThreshold
     * @see #FeatureCoverFlow_spacing
     * @see #FeatureCoverFlow_tunningWidgetSize
     * @see #FeatureCoverFlow_verticalPaddingBottom
     * @see #FeatureCoverFlow_verticalPaddingTop
     */
    public static final int[] FeatureCoverFlow={
        0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003, 
        0x7f010004, 0x7f010005, 0x7f010006, 0x7f010009, 
        0x7f01000a, 0x7f01000b, 0x7f01000c, 0x7f01000d, 
        0x7f01000e, 0x7f01000f, 0x7f010010, 0x7f010012, 
        0x7f010013, 0x7f010014, 0x7f010015
      };
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#adjustPositionMultiplier}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:adjustPositionMultiplier
     */
    public static final int FeatureCoverFlow_adjustPositionMultiplier=0;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#adjustPositionThreshold}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:adjustPositionThreshold
     */
    public static final int FeatureCoverFlow_adjustPositionThreshold=1;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#alignAnimationTime}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be an integer value, such as "<code>100</code>".
     *
     * @attr name com.martinappl.components.test:alignAnimationTime
     */
    public static final int FeatureCoverFlow_alignAnimationTime=2;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#circlePathRadius}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:circlePathRadius
     */
    public static final int FeatureCoverFlow_circlePathRadius=3;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#circlePathRadiusInMatrixSpace}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:circlePathRadiusInMatrixSpace
     */
    public static final int FeatureCoverFlow_circlePathRadiusInMatrixSpace=4;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#coverHeight}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name com.martinappl.components.test:coverHeight
     */
    public static final int FeatureCoverFlow_coverHeight=5;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#coverWidth}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name com.martinappl.components.test:coverWidth
     */
    public static final int FeatureCoverFlow_coverWidth=6;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#maxRotationAngle}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:maxRotationAngle
     */
    public static final int FeatureCoverFlow_maxRotationAngle=7;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#maxScaleFactor}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:maxScaleFactor
     */
    public static final int FeatureCoverFlow_maxScaleFactor=8;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#reflectionBackroundColor}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a color value, in the form of "<code>#<i>rgb</i></code>",
     * "<code>#<i>argb</i></code>", "<code>#<i>rrggbb</i></code>", or
     * "<code>#<i>aarrggbb</i></code>".
     *
     * @attr name com.martinappl.components.test:reflectionBackroundColor
     */
    public static final int FeatureCoverFlow_reflectionBackroundColor=9;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#reflectionGap}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name com.martinappl.components.test:reflectionGap
     */
    public static final int FeatureCoverFlow_reflectionGap=10;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#reflectionHeight}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:reflectionHeight
     */
    public static final int FeatureCoverFlow_reflectionHeight=11;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#reflectionOpacity}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be an integer value, such as "<code>100</code>".
     *
     * @attr name com.martinappl.components.test:reflectionOpacity
     */
    public static final int FeatureCoverFlow_reflectionOpacity=12;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#rotationThreshold}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:rotationThreshold
     */
    public static final int FeatureCoverFlow_rotationThreshold=13;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#scalingThreshold}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:scalingThreshold
     */
    public static final int FeatureCoverFlow_scalingThreshold=14;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#spacing}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a floating point value, such as "<code>1.2</code>".
     *
     * @attr name com.martinappl.components.test:spacing
     */
    public static final int FeatureCoverFlow_spacing=15;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#tunningWidgetSize}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name com.martinappl.components.test:tunningWidgetSize
     */
    public static final int FeatureCoverFlow_tunningWidgetSize=16;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#verticalPaddingBottom}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name com.martinappl.components.test:verticalPaddingBottom
     */
    public static final int FeatureCoverFlow_verticalPaddingBottom=17;
    /**
     * <p>This symbol is the offset where the {@link com.martinappl.components.test.R.attr#verticalPaddingTop}
     * attribute's value can be found in the {@link #FeatureCoverFlow} array.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name com.martinappl.components.test:verticalPaddingTop
     */
    public static final int FeatureCoverFlow_verticalPaddingTop=18;
  }
}